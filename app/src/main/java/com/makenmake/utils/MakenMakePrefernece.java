package com.makenmake.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.AddToCartModel;

import java.lang.reflect.Type;
import java.util.List;

public class MakenMakePrefernece {

    private static SharedPreferences mPreferences;
    private static MakenMakePrefernece mInstance;
    private static Editor mEditor;

    private static String IS_LOGGED_IN = "is_logged_in";
    private static String USER_ID = "user_id";
    private static String USER_NAME = "user_name";
    private static String PHONE_NO = "phone";
    private static String EMAIL = "email";
    private static String CART = "cart";
    public static final String TYPE = "type";
    public static final String SERVICE_PLAN = "ServicePlan";
    public static final String SERVICE_NAME = "ServiceName";
    public static final String AGREEMENT_ID = "AgreementID";
    public static final String CATEGORY = "Category";
    public static final String USER_PLAN_INFO = "userPlan";

    private static String SHOW_DIALOG_OR_NOT = "show_dialog_or_not";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";


    private MakenMakePrefernece() {
    }

    public static MakenMakePrefernece getInstance() {
        if (mInstance == null) {
            Context context = BaseApplication.mContext;
            mInstance = new MakenMakePrefernece();
            mPreferences = context.getSharedPreferences(AppConstants.NIGHT_ADVISOR_PREFS, Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }
        return mInstance;
    }

    public void setShowDilogorNot(boolean value) {
        mEditor.putBoolean(SHOW_DIALOG_OR_NOT, value).apply();
    }

    public boolean getShowDilogorNot()
    {
        return mPreferences.getBoolean(SHOW_DIALOG_OR_NOT, false);
    }

    public void setLoggedIn(boolean value) {
        mEditor.putBoolean(IS_LOGGED_IN, value).apply();
    }

    public boolean getLoggedIn() {
        return mPreferences.getBoolean(IS_LOGGED_IN, false);
    }

    public boolean UserPaymentInfo(){
        return mPreferences.getBoolean(USER_PLAN_INFO, false);
    }

    public void setUserId(String value) {
        mEditor.putString(USER_ID, value).apply();
    }

    public String getUserId() {
        return mPreferences.getString(USER_ID, null);
    }


    public void setEmail(String value) {
        mEditor.putString(EMAIL, value).apply();
    }

    public String getEmail() {
        return mPreferences.getString(EMAIL, null);
    }

    public void setPhoneNo(String value) {
        mEditor.putString(PHONE_NO, value).apply();
    }

    public String getPhoneNo() {
        return mPreferences.getString(PHONE_NO, null);
    }

    public void createPlanDetails(String type, String service_plan, String service_name, String agreement_id, String category) {
        mEditor.putString(TYPE, type);
        mEditor.putString(SERVICE_PLAN, service_plan);
        mEditor.putString(SERVICE_NAME, service_name);
        mEditor.putString(AGREEMENT_ID, agreement_id);
        mEditor.putString(CATEGORY, category);
        mEditor.putBoolean(USER_PLAN_INFO,true);
        mEditor.commit();
    }

    public String getPlan() {
        return mPreferences.getString(SERVICE_PLAN, null);
    }

    public String getCategory() {
        return mPreferences.getString(CATEGORY, null);
    }

    public String getType() {
        return mPreferences.getString(TYPE, null);
    }

    public String getAgreementId() {
        return mPreferences.getString(AGREEMENT_ID, null);
    }

    public String getServicePlan() {return mPreferences.getString(SERVICE_PLAN, null);}

    public String getPlanName() {
        return mPreferences.getString(SERVICE_NAME, null);
    }

    public void setUserName(String value) {
        mEditor.putString(USER_NAME, value).apply();
    }

    public String getUserName() {
        return mPreferences.getString(USER_NAME, null);
    }

    public void setCart(List<AddToCartModel> value) {
        Gson gson = new Gson();
        mEditor.putString(CART, gson.toJson(value)).apply();
    }

    public static void logoutUser() {
        mEditor.clear();
        mEditor.commit();
    }

    public List<AddToCartModel> getCart() {
        String cart = mPreferences.getString(CART, null);
        Type listType = new TypeToken<List<AddToCartModel>>() {
        }.getType();
        return new Gson().fromJson(cart, listType);
    }



    public static void setFirstTimeLaunch(boolean isFirstTime) {
        mEditor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime).apply();
    }

    public static boolean isFirstTimeLaunch() {
        return mPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, false);
    }


}