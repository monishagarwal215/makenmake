package com.makenmake.constants;

public interface ApiConstants {


	interface PARAMS {
		String	USERNAME				= "username";
		String	PASSWORD				= "password";

        String	FULLNAME			    = "fullname";
        String	EMAILID	    			= "emalid";
        String	PASSWORD1    			= "password";
        String	MOBILE	        		= "mobile";
		String 	USERID					= "UserId";
		String 	KEY_CATEGORY 			= "Category";
		String 	KEY_PLAN				= "Plan";
		String 	USER_ID				    = "userId";
		String	OLDPASS				    = "oldPass";
		String 	NEWPASS		     		= "newPass";
		String  FEEDBACKUSERID			= "UserID";
		String	TICKETID				= "ticketID";
		String	RATING				    = "Rating";
		String	DESCRIPTION				= "Description";
		String  INVOICE_NUMBER 			= "invoiceNumber";
		String 	AMOUNT 					= "Amount";
		String 	CHANGE_PLAN 			= "changePlan";
		String 	BALANCE 				= "balance";
		String 	WITHDRAW 				= "withdraw";
		String 	STATUS 					= "status";
		String 	PLAN  					= "plan";
		String	OTP			        	= "otp";
		String	MOBILE_NUMBER			= "emailId";
		String	USER_PASSWORD			= "password";

		String	USERS_ID		        = "userID";
		String	CREATEDFOR   			= "createdFor";
		String	AGREEMENTID	     		= "agreementID";
		String	TYPE        			= "type";
		String	DESC             		= "desc";
		String	PREFERRED_DATE			= "PreferredDate";
		String	SLOT	         		= "slotID";
		String	SERVICES		     	= "services";

		String	ADDRESS_USERID		    		= "UserID";
		String	ADDRESS_NAME		     	    = "name";
		String	ADDRESS_DOB		     	   		= "DOB";
		String	ADDRESS_EMAILID		     	    = "Emailid";
		String	ADDRESS_UserAUSERADDRESS		= "UserAddress";
		String	ADDRESS_USERCITY		     	= "UserCity";
		String	ADDRESS_USERCOUNTRY		     	= "UserCountry";
		String	ADDRESS_USERSTATE		     	= "UserState";
		String	ADDRESS_GENDER		     	    = "Gender";
		String	ADDRESS_MNUMBER		     	    = "MNumber";
		String	ADDRESS_MODIFIED		     	= "Modified";
		String	ADDRESS_CREATED		     	    = "Created";
		String	ADDRESS_USERDISTRICT		    = "UserDistrict";
		String	ADDRESS_CITYNAME		     	= "cityname";
		String	ADDRESS_DISTRICTNAME		    = "districtname";
		String	ADDRESS_STATENAME		     	= "statename";
		String	ADDRESS_COUNTRYNAME		     	= "countryname";
		String	ADDRESS_ALTERNATENUMBERS		= "AlternateNumbers";
		String	COUPON_COUPON_CODE	     	    = "CouponCode";
		String	COUPON_USER_ID		   		    = "UserID";
		String	COUPON_SEVICE_ID		     	= "ServiceID";
		String	COUPON_PLAN_TYPE		     	= "PlanType";
		String	COUPON_QUANTITY		     		= "Quantity";
		String 	USER_IDS     				    = "userid";

		String	COD_USERID							="userID";
		String	COD_CREATEDBY						="createdBy";
		String	COD_SERVICES						="services";
		String	COD_PLAN							="plan";
		String	COD_CALLS							="calls";
		String	COD_DURATION						="duration";
		String	COD_AMOUNT							="amount";







	}


	final int		REQUEST_LOGIN					= 1;
	final int		REQUEST_NOTIFICATION_COUNT		= 2;
	final int		REQUEST_GET_MENU_BAR			= 3;
	final int		REQUEST_GET_MENU_LEVEL_2		= 4;
	final int		REQUEST_GET_MENU_LEVEL_3		= 5;
	final int		REQUEST_GET_MENU_LEVEL_4		= 6;
	final int		REQUEST_CHECK_NUMBER_REGISTER	= 7;
    final int		REQUEST_REGISTER				= 8;
    final int		REQUEST_OTP_VERFICATION			= 9;
	final int		REQUEST_ACCOUNT					=10;
	final int		ALTERNATE_MOBILE				=11;
	final int		REQUEST_REFER_EARN              =12;
	final int 		REQUEST_PLAN					=13;
	final int 		REQUEST_CHECK_TICKET			=14;
	final int 		REQUEST_SEND_FEEDBACK_DATA		=15;
	final int 		REQUEST_CHANGE_PASSWORD			=16;
	final int 		REQUEST_SERVICE_BALANCE			=17;
	final int 		REQUEST_FORGOT_PASSWORD			=18;
	final int 		REQUEST_TICKET_HISTORY			=19;
	final int 		REQUEST_DETAILS_TICKET_HISTORY	=20;
    final int 		REQUEST_SERVICE_SAVE        	=21;
	final int 		REQUEST_GET_SERVICES_CLIENT  	=22;
	final int 		REQUEST_GET_SLOT_ID         	=23;
	final int 		REQUEST_VERIFY_OTP           	=24;
	final int 		REQUEST_GET_SERVICE_PURCHASED 	=25;
	final int 		REQUEST_AGREEMENT_DETAILS    	=26;
	final int 		REQUEST_WALLET_AMOUNT    	    =27;
	final int 		REQUEST_WALLET_HISTORY   	    =28;
	final int 		REQUEST_TICKET_DETAILS   	    =29;
	final int		REQUEST_STATE_LIST				=30;
	final int		REQUEST_DISTRICT_LIST			=31;
	final int		REQUEST_CITY_LIST				=32;
	final int       REQUEST_USER_INFO				=33;
	final int 		REQUEST_UPDATE_USER_INFO		=34;
	final int 		REQUEST_TROUBLE_LOGIN			=35;
	final int 		REQUEST_DO_COD_PAYMENT			=36;
    final int 		REQUEST_GET_PAYMENT 			=37;
	final int 		REQUEST_CHECK_COUPON 			=38;
	final int       REQUEST_SERVICE_BUY_SAVE		=39;

	final String	URL_IMAGE_BASE				= "http://services.makenmake.in/mnmapiv4/";
	final String	URL_BASE					= "http://services.makenmake.in/mnmapiv4/api/";//dev
    //final String	URL_BASE					= "http://103.25.131.236/mnmapi/api/";
	final String	URL_LOGIN					= URL_BASE + "login";
	final String	URL_GET_MENU_BAR			= URL_BASE + "getMenuNew";
	final String	URL_GET_MENU_LEVEL_2		= URL_BASE + "ServiceApi/getLevel2?subCatID=";
	final String	URL_GET_MENU_LEVEL_3		= URL_BASE + "ServiceApi/getAllCategory3?servicecat2=";
	final String	URL_GET_MENU_LEVEL_4		= URL_BASE + "ServiceApi/getAllCategory4?servicecat1=%s&servicecat2=%s&servicecat3=%s";
    final String	URL_CHECK_NUMBER_REGISTER	= URL_BASE + "registerUser?mobileNumber=";
    final String	URL_REGISTER				= URL_BASE + "registerUser";
    final String	URL_OTP_VERIFICATION		= URL_BASE + "forgotPassword";
	final String    ACCOUNT_URL 				= URL_BASE + "getUserInfo?UserId=";
	final String    ALTERNATE_MOBILE_URL        = URL_BASE + "updateAlernateNumber/Get?alternatemobileno1=";
	final String	REFER_EARN					= URL_BASE + "getReferralCode?userId=";
	final String    URL_PLAN                    = URL_BASE + "serviceBuy/Post";
	final String    URL_CHECK_TICKET	        = URL_BASE + "getFeedbackTicket?UserID=";
	final String    URL_SEND_FEEDBACK_DATA      = URL_BASE + "setTicketFeedbackData?UserID=";
	final String    URL_CHANGE_PASSWORD	        = URL_BASE + "ResetPassword";
	final String    SERVICE_BALANCE   			= URL_BASE + "balance?UserId=";
	final String    URL_FORGOT_PASSWORD   		= URL_BASE + "forgotPassword/Get?email=";
	final String    URL_TICKET_HISTORY	   		= URL_BASE + "TicketHistory?userId=";
	final String    URL_DETAILS_TICKET_HISTORY  = URL_BASE + "TicketHistory?currentpage=0&TicketID=";
	final String    URL_SERVICE_SAVE			= URL_BASE + "serviceSave?userID=";
	final String    URL_SERVICE_BUY_SAVE		= URL_BASE + "serviceBuySave/Save";
	final String    URL_GET_SERVICES_CLIENT	    = URL_BASE + "GetServiceAccordingToUser?userID=";
	final String    URL_GET_SLOT_ID     	    = URL_BASE + "ServiceApi/GetSerivceTimeSlots?serviceId=";
	final String    URL_VERIFY_OTP      	    = URL_BASE + "verifyUnverifiedOverOtp?emailId=";
	final String    URL_GET_SERVICE_PURCHASED   = URL_BASE + "getServicePurchasedByClient?userId=";
	final String    URL_AGREEMENT_DETAILS       = URL_BASE + "AgreementDetail?agreementID=";
	final String    URL_WALLET_AMOUNT           = URL_BASE + "Wallet/totalWalletAmount?UserId=";
	final String    URL_WALLET_History          = URL_BASE + "Wallet/walletHistoryData?UserId=";
	final String    URL_TICKET_DETAILS          = URL_BASE + "TIcketDetails";
	final String    URL_STATE_LIST	            = URL_BASE + "getState?countryID=";
	final String    URL_DISTRICT_LIST           = URL_BASE + "getDistrict?stateID=";
	final String    URL_CITY_LIST               = URL_BASE + "getCity?districtID=";
	final String	URL_USER_INFO				= URL_BASE + "getUserInfo?UserId=";
	final String    URL_UPDATE_USER_INFO		= URL_BASE + "updateUserInfo";
	final String    URL_TROUBLE_LOGIN			= URL_BASE + "TroubleInLogin?emailId=";
    final String    URL_GET_PAYMENT			    = URL_BASE + "getPayment?UserId=";
    final String    URL_DO_COD_PAYMENT			= URL_BASE + "doCODPayment";
	final String    URL_CHECK_COUPON			= URL_BASE + "isValidCoupon";
}
