package com.makenmake.constants;

public interface AppConstants {

	public int					REQUEST_CODE_CAMERA					= 10001;
	public int					REQUEST_PICK_IMAGE					= 10002;

	public static final String	NIGHT_ADVISOR_PREFS					= "make_n_make";

	public String				EXTRA_LOOKS							= "extra_looks";
	public String				EXTRA_PHONE_NO						= "extra_phone_no";
	public String				EXTRA_USERNAME						= "extra_username";
	public String				EXTRA_PLAN				     		= "extra_plan";
	public String				EXTRA_SERVICE						= "extra_service";
	public String				EXTRA_CART							= "extra_cart";
	public String				EXTRA_URL							= "extra_url";
	public String				EXTRA_USER_INFO						= "extra_user_info";
	public String 				FROM_TROUBLE 						= "extra_trouble";
	public String 				EXTRA_PAYMENT_LIST 					= "extra_payment_list";
	public String 				EXTRA_INVOICE_LIST 					= "extra_invoice_list";

	public String				BUNDLE_USER							= "BUNDLE_USER";
	public String				BUNDLE_CATEGORY_ID					= "BUNDLE_CATEGORY_ID";

	public String				CATEGORY_DETAILS					= "categoryActivity";

	public String				LOCAL_BROADCAST_DELETE_EXPIRE_LOOK	= "local_broadcast_delete_expire_look";

	public String				BUNDLE_CATEGORY			     		= "BUNDLE_CATEGORY";
	public String				BUNDLE_SLOTID			     		= "BUNDLE_SLOTID";
	public String				BUNDLE_AGREEMENTID					= "BUNDLE_AGREEMENTID";
	public String				BUNDLE_SERVICE_NAME					= "BUNDLE_SERVICE_NAME";
	public String				BUNDLE_SERVICEID					= "BUNDLE_SERVICEID";

	public String               EXTRA_USERID					    = "extra_userid";
	public String               EXTRA_NAME							= "extra_name";
	public String               EXTRA_DOB							= "extra_DOB";
	public String               EXTRA_EMAILID						= "extra_emailid";
	public String               EXTRA_GENDER						= "extra_gender";
	public String               EXTRA_MNUMBER						= "extra_mnumber";
	public String               EXTRA_MODIFIED						= "extra_modified";
	public String               EXTRA_CREATED						= "extra_created";
	public String               EXTRA_ALTERNATENUMBERS				= "extra_alternatednumbers";
	public String				EXTRA_FNAME							= "extra_fname";


	public String				CITRUS_PAYMENT_URL					= "http://services.makenmake.in/mnm/Pages/ConfirmCitrusPaymentAction.aspx?ActionParameter=";


}
