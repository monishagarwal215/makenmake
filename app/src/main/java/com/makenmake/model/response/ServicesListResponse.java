package com.makenmake.model.response;

import java.util.List;

/**
 * Created by K.Agg on 17-09-2016.
 */
public class ServicesListResponse {

    private List<ServicesList> servicesList;


    public List<ServicesList> getServicesList() {
        return servicesList;
    }

    public void setServicesList(List<ServicesList> servicesList) {
        this.servicesList = servicesList;
    }
}
