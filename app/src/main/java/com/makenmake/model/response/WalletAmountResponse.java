package com.makenmake.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by saurabh vardani on 19/09/16.
 */
public class WalletAmountResponse {

    @SerializedName("TotalAmount")
    @Expose
    private String totalAmount;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     *
     * @return
     * The totalAmount
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     *
     * @param totalAmount
     * The TotalAmount
     */
    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

}