package com.makenmake.model.response;

/**
 * Created by monish on 11/09/16.
 */
public class TblServicePlan {
    private int ServicePlanID;
    private int ServiceID;
    private String PlanType;
    private int NofCalls;
    private int Duration;
    private int Amount;
    private int VisitRequired;
    private int FDiscount;
    private int CreatedBy;

    public int getServicePlanID() {
        return ServicePlanID;
    }

    public void setServicePlanID(int servicePlanID) {
        ServicePlanID = servicePlanID;
    }

    public int getServiceID() {
        return ServiceID;
    }

    public void setServiceID(int serviceID) {
        ServiceID = serviceID;
    }

    public String getPlanType() {
        return PlanType;
    }

    public void setPlanType(String planType) {
        PlanType = planType;
    }

    public int getNofCalls() {
        return NofCalls;
    }

    public void setNofCalls(int nofCalls) {
        NofCalls = nofCalls;
    }

    public int getDuration() {
        return Duration;
    }

    public void setDuration(int duration) {
        Duration = duration;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }

    public int getVisitRequired() {
        return VisitRequired;
    }

    public void setVisitRequired(int visitRequired) {
        VisitRequired = visitRequired;
    }

    public int getFDiscount() {
        return FDiscount;
    }

    public void setFDiscount(int FDiscount) {
        this.FDiscount = FDiscount;
    }

    public int getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(int createdBy) {
        CreatedBy = createdBy;
    }
}
