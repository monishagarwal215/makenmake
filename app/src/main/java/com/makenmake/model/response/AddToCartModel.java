package com.makenmake.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

/**
 * Created by monish on 10/09/16.
 */
public class AddToCartModel implements Parcelable {
    private String serviceName;
    private String serviceContent;
    private int amount;
    private int noOfService;
    private int serviceId;
    private String type;
    private int planId;
    private String sPlan;
    private String couponCode;
    private String discountedPrice = "0";
    private String couponId = "";
    private String slotId;
    private String prefDate;

    public AddToCartModel() {

    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(serviceName);
        dest.writeString(serviceContent);
        dest.writeInt(amount);
        dest.writeInt(noOfService);
        dest.writeInt(serviceId);
        dest.writeString(type);
        dest.writeInt(planId);
        dest.writeString(sPlan);
        dest.writeString(couponCode);
        dest.writeString(couponId);
        dest.writeString(discountedPrice);
        dest.writeString(slotId);
        dest.writeString(prefDate);
    }

    public AddToCartModel(Parcel in) {
        this.serviceName = in.readString();
        this.serviceContent = in.readString();
        this.amount = in.readInt();
        this.noOfService = in.readInt();
        this.serviceId = in.readInt();
        this.type = in.readString();
        this.planId = in.readInt();
        this.sPlan = in.readString();
        this.couponCode = in.readString();
        this.couponId = in.readString();
        this.discountedPrice = in.readString();
        this.slotId = in.readString();
        this.prefDate = in.readString();
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getNoOfService() {
        return noOfService;
    }

    public void setNoOfService(int noOfService) {
        this.noOfService = noOfService;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public String getsPlan() {
        return sPlan;
    }

    public void setsPlan(String sPlan) {
        this.sPlan = sPlan;
    }

    public String getServiceContent() {
        return serviceContent;
    }

    public void setServiceContent(String serviceContent) {
        this.serviceContent = serviceContent;
    }

    public String getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(String discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getPrefDate() {
        return prefDate;
    }

    public void setPrefDate(String prefDate) {
        this.prefDate = prefDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AddToCartModel that = (AddToCartModel) o;
        return serviceId == that.serviceId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(serviceId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<AddToCartModel> CREATOR = new Parcelable.Creator<AddToCartModel>() {

        public AddToCartModel createFromParcel(Parcel in) {
            return new AddToCartModel(in);
        }

        public AddToCartModel[] newArray(int size) {
            return new AddToCartModel[size];
        }
    };


}
