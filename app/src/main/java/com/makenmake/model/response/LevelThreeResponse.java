package com.makenmake.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 11/09/16.
 */
public class LevelThreeResponse extends CommonJsonResponse{
    private int addServiceForId;
    private int SubcategoryId;
    private String ServiceForName;

    public int getAddServiceForId() {
        return addServiceForId;
    }

    public void setAddServiceForId(int addServiceForId) {
        this.addServiceForId = addServiceForId;
    }

    public int getSubcategoryId() {
        return SubcategoryId;
    }

    public void setSubcategoryId(int subcategoryId) {
        SubcategoryId = subcategoryId;
    }

    public String getServiceForName() {
        return ServiceForName;
    }

    public void setServiceForName(String serviceForName) {
        ServiceForName = serviceForName;
    }
}
