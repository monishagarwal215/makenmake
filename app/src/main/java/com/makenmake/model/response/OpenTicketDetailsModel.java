package com.makenmake.model.response;

/**
 * Created by K.Agg on 21-09-2016.
 */
public class OpenTicketDetailsModel {
    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
