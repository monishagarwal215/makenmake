package com.makenmake.model.response;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class TimeSloteModel {

    private int SlotID;
    private int SessionID;
    private String SlotSession;
    private String SlotFrom;
    private String SlotTo;
    private String SlotStatus;

    public int getSlotID() {
        return SlotID;
    }

    public void setSlotID(int slotID) {
        SlotID = slotID;
    }

    public int getSessionID() {
        return SessionID;
    }

    public void setSessionID(int sessionID) {
        SessionID = sessionID;
    }

    public String getSlotSession() {
        return SlotSession;
    }

    public void setSlotSession(String slotSession) {
        SlotSession = slotSession;
    }

    public String getSlotStatus() {
        return SlotStatus;
    }

    public void setSlotStatus(String slotStatus) {
        SlotStatus = slotStatus;
    }

    public String getSlotTo() {
        return SlotTo;
    }

    public void setSlotTo(String slotTo) {
        SlotTo = slotTo;
    }

    public String getSlotFrom() {
        return SlotFrom;
    }

    public void setSlotFrom(String slotFrom) {
        SlotFrom = slotFrom;
    }
}
