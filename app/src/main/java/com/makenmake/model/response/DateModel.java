package com.makenmake.model.response;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by monish on 16/09/16.
 */
public class DateModel {
    private DateTime dateTime;
    private boolean isSelected;

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
