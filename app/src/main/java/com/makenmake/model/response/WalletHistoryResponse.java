package com.makenmake.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by saurabh vardani on 19/09/16.
 */
public class WalletHistoryResponse {

    private String WalletHistoryID;
    private String Description;
    private String Amount;
    private String PreviousAmount;
    private String createdDate;

    public String getWalletHistoryID() {
        return WalletHistoryID;
    }

    public void setWalletHistoryID(String walletHistoryID) {
        WalletHistoryID = walletHistoryID;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getPreviousAmount() {
        return PreviousAmount;
    }

    public void setPreviousAmount(String previousAmount) {
        PreviousAmount = previousAmount;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }
}