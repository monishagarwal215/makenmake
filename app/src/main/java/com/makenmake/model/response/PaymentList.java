package com.makenmake.model.response;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by monish on 24/09/16.
 */

public class PaymentList implements Parcelable {
    private String Category;
    private String serviceType;
    private String Plan;
    private String services;
    private String total;
    private String totalWithOverhead;
    private String saving;
    private String AlreadyActivePlan;
    private String walletMoney;


    public PaymentList() {

    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(Category);
        dest.writeString(serviceType);
        dest.writeString(Plan);
        dest.writeString(services);
        dest.writeString(total);
        dest.writeString(totalWithOverhead);
        dest.writeString(saving);
        dest.writeString(AlreadyActivePlan);
        dest.writeString(walletMoney);


    }

    public PaymentList(Parcel in) {
        this.Category = in.readString();
        this.serviceType = in.readString();
        this.Plan = in.readString();
        this.services = in.readString();
        this.total = in.readString();
        this.totalWithOverhead = in.readString();
        this.saving = in.readString();
        this.AlreadyActivePlan = in.readString();
        this.walletMoney = in.readString();

    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPlan() {
        return Plan;
    }

    public void setPlan(String plan) {
        Plan = plan;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalWithOverhead() {
        return totalWithOverhead;
    }

    public void setTotalWithOverhead(String totalWithOverhead) {
        this.totalWithOverhead = totalWithOverhead;
    }

    public String getSaving() {
        return saving;
    }

    public void setSaving(String saving) {
        this.saving = saving;
    }

    public String getAlreadyActivePlan() {
        return AlreadyActivePlan;
    }

    public void setAlreadyActivePlan(String alreadyActivePlan) {
        AlreadyActivePlan = alreadyActivePlan;
    }

    public String getWalletMoney() {
        return walletMoney;
    }

    public void setWalletMoney(String walletMoney) {
        this.walletMoney = walletMoney;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<PaymentList> CREATOR = new Parcelable.Creator<PaymentList>() {

        public PaymentList createFromParcel(Parcel in) {
            return new PaymentList(in);
        }

        public PaymentList[] newArray(int size) {
            return new PaymentList[size];
        }
    };

}
