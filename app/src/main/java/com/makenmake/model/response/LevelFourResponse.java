package com.makenmake.model.response;

/**
 * Created by monish on 11/09/16.
 */
public class LevelFourResponse {
    private TbladdServiceRate TbladdServiceRate;
    private TblServicePlan TblServicePlan;

    public com.makenmake.model.response.TbladdServiceRate getTbladdServiceRate() {
        return TbladdServiceRate;
    }

    public void setTbladdServiceRate(com.makenmake.model.response.TbladdServiceRate tbladdServiceRate) {
        TbladdServiceRate = tbladdServiceRate;
    }

    public com.makenmake.model.response.TblServicePlan getTblServicePlan() {
        return TblServicePlan;
    }

    public void setTblServicePlan(com.makenmake.model.response.TblServicePlan tblServicePlan) {
        TblServicePlan = tblServicePlan;
    }
}
