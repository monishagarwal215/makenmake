package com.makenmake.model.response;

import com.krapps.model.CommonJsonResponse;

import java.util.List;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class ServicePurchasedByClient extends CommonJsonResponse {
    private List<UserAgreement> userAgreement;
    private Boolean Success;
    private boolean isAnyAgreement;

    public List<UserAgreement> getUserAgreement() {
        return userAgreement;
    }

    public void setUserAgreement(List<UserAgreement> userAgreement) {
        this.userAgreement = userAgreement;
    }

    public Boolean getSuccess() {
        return Success;
    }

    public void setSuccess(Boolean success) {
        Success = success;
    }

    public boolean isAnyAgreement() {
        return isAnyAgreement;
    }

    public void setAnyAgreement(boolean anyAgreement) {
        isAnyAgreement = anyAgreement;
    }
}
