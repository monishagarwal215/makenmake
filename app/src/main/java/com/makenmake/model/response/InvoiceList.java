package com.makenmake.model.response;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by monish on 24/09/16.
 */

public class InvoiceList implements Parcelable {

    private String invoiceNumber;
    private String AgreementId;
    private String AgreementNumber;
    private String Expirationdate;
    private String Invoicedate;
    private String amount;
    private String amount1;
    private String TotalUsedAmount;
    private String previousPlan;

    public InvoiceList() {

    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(invoiceNumber);
        dest.writeString(AgreementId);
        dest.writeString(AgreementNumber);
        dest.writeString(Expirationdate);
        dest.writeString(Invoicedate);
        dest.writeString(amount);
        dest.writeString(amount1);
        dest.writeString(TotalUsedAmount);
        dest.writeString(previousPlan);
    }

    public InvoiceList(Parcel in) {
        this.invoiceNumber = in.readString();
        this.AgreementId = in.readString();
        this.AgreementNumber = in.readString();
        this.Expirationdate = in.readString();
        this.Invoicedate = in.readString();
        this.amount = in.readString();
        this.amount1 = in.readString();
        this.TotalUsedAmount = in.readString();
        this.previousPlan = in.readString();
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getAgreementId() {
        return AgreementId;
    }

    public void setAgreementId(String agreementId) {
        AgreementId = agreementId;
    }

    public String getAgreementNumber() {
        return AgreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        AgreementNumber = agreementNumber;
    }

    public String getExpirationdate() {
        return Expirationdate;
    }

    public void setExpirationdate(String expirationdate) {
        Expirationdate = expirationdate;
    }

    public String getInvoicedate() {
        return Invoicedate;
    }

    public void setInvoicedate(String invoicedate) {
        Invoicedate = invoicedate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmount1() {
        return amount1;
    }

    public void setAmount1(String amount1) {
        this.amount1 = amount1;
    }

    public String getTotalUsedAmount() {
        return TotalUsedAmount;
    }

    public void setTotalUsedAmount(String totalUsedAmount) {
        TotalUsedAmount = totalUsedAmount;
    }

    public String getPreviousPlan() {
        return previousPlan;
    }

    public void setPreviousPlan(String previousPlan) {
        this.previousPlan = previousPlan;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<InvoiceList> CREATOR = new Parcelable.Creator<InvoiceList>() {

        public InvoiceList createFromParcel(Parcel in) {
            return new InvoiceList(in);
        }

        public InvoiceList[] newArray(int size) {
            return new InvoiceList[size];
        }
    };
}
