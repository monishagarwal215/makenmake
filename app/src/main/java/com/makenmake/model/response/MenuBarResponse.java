package com.makenmake.model.response;

import com.krapps.model.CommonJsonResponse;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by monish on 11/09/16.
 */
public class MenuBarResponse extends CommonJsonResponse {
    private String categroy1;
    private int serviceID;
    private String Title;
    private String Content;
    private String imageIcon;
    private String ImagePath;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getCategroy1() {
        return categroy1;
    }

    public void setCategroy1(String categroy1) {
        this.categroy1 = categroy1;
    }

    public int getServiceID() {
        return serviceID;
    }

    public void setServiceID(int serviceID) {
        this.serviceID = serviceID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(String imageIcon) {
        this.imageIcon = imageIcon;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }
/*
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuBarResponse that = (MenuBarResponse) o;
        return serviceID == that.serviceID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(serviceID);
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuBarResponse that = (MenuBarResponse) o;
        return Objects.equals(categroy1, that.categroy1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(categroy1);
    }
}
