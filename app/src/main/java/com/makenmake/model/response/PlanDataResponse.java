package com.makenmake.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saurabh vardani on 14-09-2016.
 */
public class PlanDataResponse {
    @SerializedName("basicServicesList")
    @Expose
    private List<BasicServicesList> basicServicesList ;
    @SerializedName("addOnServicesList")
    @Expose
    private List<AddOnServicesList> addOnServicesList ;
    /**
     *
     * @return
     * The basicServicesList
     */
    public List<BasicServicesList> getBasicServicesList() {
        return basicServicesList;
    }

    /**
     *
     * @param basicServicesList
     * The basicServicesList
     */
    public void setBasicServicesList(List<BasicServicesList> basicServicesList) {
        this.basicServicesList = basicServicesList;
    }

    /**
     *
     * @return
     * The addOnServicesList
     */
    public List<AddOnServicesList> getAddOnServicesList() {
        return addOnServicesList;
    }

    /**
     *
     * @param addOnServicesList
     * The addOnServicesList
     */
    public void setAddOnServicesList(List<AddOnServicesList> addOnServicesList) {
        this.addOnServicesList = addOnServicesList;
    }

}
