package com.makenmake.model.response;

/**
 * Created by K.Agg on 16-09-2016.
 */
public class TicketListModel {

private int ticketID;

    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }
}
