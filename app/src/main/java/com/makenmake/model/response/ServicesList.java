package com.makenmake.model.response;

/**
 * Created by K.Agg on 17-09-2016.
 */
public class ServicesList {
    private String AgreementID;
    private String serviceId;
    private String serviceName;
    private String serviceType;
    private String Category;
    private String category1;
    private String Plane;
    private String duration;
    private String amount;
    private String NofCalls;
    private String ImageIcon;
    private String ImagePath;


    public String getAgreementID() {
        return AgreementID;
    }

    public void setAgreementID(String agreementID) {
        AgreementID = agreementID;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    public String getImageIcon() {
        return ImageIcon;
    }

    public void setImageIcon(String imageIcon) {
        ImageIcon = imageIcon;
    }

    public String getNofCalls() {
        return NofCalls;
    }

    public void setNofCalls(String nofCalls) {
        NofCalls = nofCalls;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getPlane() {
        return Plane;
    }

    public void setPlane(String plane) {
        Plane = plane;
    }

    public String getCategory1() {
        return category1;
    }

    public void setCategory1(String category1) {
        this.category1 = category1;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }
}
