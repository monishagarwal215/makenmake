package com.makenmake.model.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CouponResponse {

    @SerializedName("isValid")
    @Expose
    private boolean isValid;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("CouponID")
    @Expose
    private int couponID;
    @SerializedName("DiscountType")
    @Expose
    private String discountType;
    @SerializedName("TotalDisountAmount")
    @Expose
    private int totalDisountAmount;
    @SerializedName("TotalPayableAmount")
    @Expose
    private int totalPayableAmount;

    /**
     *
     * @return
     * The isValid
     */
    public boolean isIsValid() {
        return isValid;
    }

    /**
     *
     * @param isValid
     * The isValid
     */
    public void setIsValid(boolean isValid) {
        this.isValid = isValid;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The Message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The couponID
     */
    public int getCouponID() {
        return couponID;
    }

    /**
     *
     * @param couponID
     * The CouponID
     */
    public void setCouponID(int couponID) {
        this.couponID = couponID;
    }

    /**
     *
     * @return
     * The discountType
     */
    public String getDiscountType() {
        return discountType;
    }

    /**
     *
     * @param discountType
     * The DiscountType
     */
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    /**
     *
     * @return
     * The totalDisountAmount
     */
    public int getTotalDisountAmount() {
        return totalDisountAmount;
    }

    /**
     *
     * @param totalDisountAmount
     * The TotalDisountAmount
     */
    public void setTotalDisountAmount(int totalDisountAmount) {
        this.totalDisountAmount = totalDisountAmount;
    }

    /**
     *
     * @return
     * The totalPayableAmount
     */
    public int getTotalPayableAmount() {
        return totalPayableAmount;
    }

    /**
     *
     * @param totalPayableAmount
     * The TotalPayableAmount
     */
    public void setTotalPayableAmount(int totalPayableAmount) {
        this.totalPayableAmount = totalPayableAmount;
    }

}