package com.makenmake.model.response;

/**
 * Created by monish on 17/09/16.
 */
public class BasicServicePaymentList {
    private String Category;
    private String serviceType;
    private String Plan;
    private String services;
    private int total;
    private int totalWithOverhead;
    private int saving;
    private String AlreadyActivePlan;
    private int walletMoney;

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getPlan() {
        return Plan;
    }

    public void setPlan(String plan) {
        Plan = plan;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotalWithOverhead() {
        return totalWithOverhead;
    }

    public void setTotalWithOverhead(int totalWithOverhead) {
        this.totalWithOverhead = totalWithOverhead;
    }

    public int getSaving() {
        return saving;
    }

    public void setSaving(int saving) {
        this.saving = saving;
    }

    public String getAlreadyActivePlan() {
        return AlreadyActivePlan;
    }

    public void setAlreadyActivePlan(String alreadyActivePlan) {
        AlreadyActivePlan = alreadyActivePlan;
    }

    public int getWalletMoney() {
        return walletMoney;
    }

    public void setWalletMoney(int walletMoney) {
        this.walletMoney = walletMoney;
    }
}
