package com.makenmake.model.response;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class AgreementDetail {

    private String AgreementID;
    private String Category;
    private String Type;
    private String ServicePlan;
    private String ServiceName;
    private String Amount;
    private String NofCalls;
    private String Area;
    private String ServiceID;
    private String UCategory;

    public String getAgreementID() {
        return AgreementID;
    }

    public void setAgreementID(String agreementID) {
        AgreementID = agreementID;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getServicePlan() {
        return ServicePlan;
    }

    public void setServicePlan(String servicePlan) {
        ServicePlan = servicePlan;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getNofCalls() {
        return NofCalls;
    }

    public void setNofCalls(String nofCalls) {
        NofCalls = nofCalls;
    }

    public String getArea() {
        return Area;
    }

    public void setArea(String area) {
        Area = area;
    }

    public String getServiceID() {
        return ServiceID;
    }

    public void setServiceID(String serviceID) {
        ServiceID = serviceID;
    }

    public String getUCategory() {
        return UCategory;
    }

    public void setUCategory(String UCategory) {
        this.UCategory = UCategory;
    }
}
