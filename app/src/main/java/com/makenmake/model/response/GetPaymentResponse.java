package com.makenmake.model.response;

import java.util.List;

/**
 * Created by monish on 24/09/16.
 */

public class GetPaymentResponse {

    private List<PaymentList> basicServicesList;
    private List<InvoiceList> addOnServicesList;

    public List<PaymentList> getBasicServicesList() {
        return basicServicesList;
    }

    public void setBasicServicesList(List<PaymentList> basicServicesList) {
        this.basicServicesList = basicServicesList;
    }

    public List<InvoiceList> getAddOnServicesList() {
        return addOnServicesList;
    }

    public void setAddOnServicesList(List<InvoiceList> addOnServicesList) {
        this.addOnServicesList = addOnServicesList;
    }
}
