package com.makenmake.model.response;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class UserAgreement {

    private int TblInId;
    private int AgreementID;
    private String AgreementNumber;
    private String Invoicedate;
    private String InvoiceNumber;
    private float Amount;
    private String PaymentMode;
    private String ExpiryDate;

    public int getTblInId() {
        return TblInId;
    }

    public void setTblInId(int tblInId) {
        TblInId = tblInId;
    }

    public int getAgreementID() {
        return AgreementID;
    }

    public void setAgreementID(int agreementID) {
        AgreementID = agreementID;
    }

    public String getAgreementNumber() {
        return AgreementNumber;
    }

    public void setAgreementNumber(String agreementNumber) {
        AgreementNumber = agreementNumber;
    }

    public String getInvoicedate() {
        return Invoicedate;
    }

    public void setInvoicedate(String invoicedate) {
        Invoicedate = invoicedate;
    }

    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        InvoiceNumber = invoiceNumber;
    }

    public float getAmount() {
        return Amount;
    }

    public void setAmount(float amount) {
        Amount = amount;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }
}
