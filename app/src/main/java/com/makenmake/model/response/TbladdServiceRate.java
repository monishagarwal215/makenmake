package com.makenmake.model.response;

/**
 * Created by monish on 11/09/16.
 */
public class TbladdServiceRate {
    private int addRateId;
    private int ServiceforId;
    private String ServiceRateName;

    public int getAddRateId() {
        return addRateId;
    }

    public void setAddRateId(int addRateId) {
        this.addRateId = addRateId;
    }

    public int getServiceforId() {
        return ServiceforId;
    }

    public void setServiceforId(int serviceforId) {
        ServiceforId = serviceforId;
    }

    public String getServiceRateName() {
        return ServiceRateName;
    }

    public void setServiceRateName(String serviceRateName) {
        ServiceRateName = serviceRateName;
    }
}
