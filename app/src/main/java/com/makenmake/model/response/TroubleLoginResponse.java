package com.makenmake.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 24/09/16.
 */

public class TroubleLoginResponse extends CommonJsonResponse {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
