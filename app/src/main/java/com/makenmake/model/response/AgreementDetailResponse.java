package com.makenmake.model.response;

import java.util.List;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class AgreementDetailResponse {
    private List<AgreementDetail> agreementDetail;
    private boolean Success;
    private boolean isAnyAgreement;

    public List<AgreementDetail> getAgreementDetail() {
        return agreementDetail;
    }

    public void setAgreementDetail(List<AgreementDetail> agreementDetail) {
        this.agreementDetail = agreementDetail;
    }

    public boolean isSuccess() {
        return Success;
    }

    public void setSuccess(boolean success) {
        Success = success;
    }

    public boolean isAnyAgreement() {
        return isAnyAgreement;
    }

    public void setAnyAgreement(boolean anyAgreement) {
        isAnyAgreement = anyAgreement;
    }
}
