package com.makenmake.model.response;

/**
 * Created by saurabh vardani on 13-09-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class UserModel {

    @SerializedName("UserID")
    @Expose
    private Integer userID;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("DOB")
    @Expose
    private String dOB;

    @SerializedName("Emailid")
    @Expose
    private String emailid;

    @SerializedName("UserAddress")
    @Expose
    private String userAddress;

    @SerializedName("UserCity")
    @Expose
    private String userCity;

    @SerializedName("UserCountry")
    @Expose
    private String userCountry;

    @SerializedName("UserState")
    @Expose
    private String userState;

    @SerializedName("Gender")
    @Expose
    private String gender;

    @SerializedName("MNumber")
    @Expose
    private String mNumber;

    @SerializedName("Modified")
    @Expose
    private String modified;

    @SerializedName("Created")
    @Expose
    private String created;

    @SerializedName("UserDistrict")
    @Expose
    private String userDistrict;

    @SerializedName("cityname")
    @Expose
    private String cityname;

    @SerializedName("districtname")
    @Expose
    private String districtname;

    @SerializedName("statename")
    @Expose
    private String statename;

    @SerializedName("countryname")
    @Expose
    private String countryname;

    @SerializedName("AlternateNumbers")
    @Expose
    private String alternateNumbers;

    /**
     * @return The userID
     */
    public Integer getUserID() {
        return userID;
    }

    /**
     * @param userID The UserID
     */
    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The dOB
     */
    public String getDOB() {
        return dOB;
    }

    /**
     * @param dOB The DOB
     */
    public void setDOB(String dOB) {
        this.dOB = dOB;
    }

    /**
     * @return The emailid
     */
    public String getEmailid() {
        return emailid;
    }

    /**
     * @param emailid The Emailid
     */
    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    /**
     * @return The userAddress
     */
    public String getUserAddress() {
        return userAddress;
    }

    /**
     * @param userAddress The UserAddress
     */
    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    /**
     * @return The userCity
     */
    public String getUserCity() {
        return userCity;
    }

    /**
     * @param userCity The UserCity
     */
    public void setUserCity(String userCity) {
        this.userCity = userCity;
    }

    /**
     * @return The userCountry
     */
    public String getUserCountry() {
        return userCountry;
    }

    /**
     * @param userCountry The UserCountry
     */
    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    /**
     * @return The userState
     */
    public String getUserState() {
        return userState;
    }

    /**
     * @param userState The UserState
     */
    public void setUserState(String userState) {
        this.userState = userState;
    }

    /**
     * @return The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender The Gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return The mNumber
     */
    public String getMNumber() {
        return mNumber;
    }

    /**
     * @param mNumber The MNumber
     */
    public void setMNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    /**
     * @return The modified
     */
    public String getModified() {
        return modified;
    }

    /**
     * @param modified The Modified
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     * @return The created
     */
    public String getCreated() {
        return created;
    }

    /**
     * @param created The Created
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     * @return The userDistrict
     */
    public String getUserDistrict() {
        return userDistrict;
    }

    /**
     * @param userDistrict The UserDistrict
     */
    public void setUserDistrict(String userDistrict) {
        this.userDistrict = userDistrict;
    }

    /**
     * @return The cityname
     */
    public String getCityname() {
        return cityname;
    }

    /**
     * @param cityname The cityname
     */
    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    /**
     * @return The districtname
     */
    public String getDistrictname() {
        return districtname;
    }

    /**
     * @param districtname The districtname
     */
    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }

    /**
     * @return The statename
     */
    public String getStatename() {
        return statename;
    }

    /**
     * @param statename The statename
     */
    public void setStatename(String statename) {
        this.statename = statename;
    }

    /**
     * @return The countryname
     */
    public String getCountryname() {
        return countryname;
    }

    /**
     * @param countryname The countryname
     */
    public void setCountryname(String countryname) {
        this.countryname = countryname;
    }

    /**
     * @return The alternateNumbers
     */
    public String getAlternateNumbers() {
        return alternateNumbers;
    }

    /**
     * @param alternateNumbers The AlternateNumbers
     */
    public void setAlternateNumbers(String alternateNumbers) {
        this.alternateNumbers = alternateNumbers;
    }

}