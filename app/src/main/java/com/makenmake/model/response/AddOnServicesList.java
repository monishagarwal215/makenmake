package com.makenmake.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jyoti goyal on 16-09-2016.
 */
public class AddOnServicesList {

    @SerializedName("serviceId")
    @Expose
    private Integer serviceId;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Plan")
    @Expose
    private String plan;
    @SerializedName("chkService")
    @Expose
    private Integer chkService;
    @SerializedName("ServiceType")
    @Expose
    private String serviceType;
    @SerializedName("planid")
    @Expose
    private Integer planid;
    @SerializedName("NofCalls")
    @Expose
    private Integer nofCalls;
    @SerializedName("Area")
    @Expose
    private String area;
    @SerializedName("UCategory")
    @Expose
    private String uCategory;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("unlimitedamount")
    @Expose
    private Integer unlimitedamount;
    @SerializedName("UPlanID")
    @Expose
    private Integer uPlanID;
    @SerializedName("VisitRequired")
    @Expose
    private String visitRequired;

    private boolean isSelected;

    /**
     * @return The serviceId
     */
    public Integer getServiceId() {
        return serviceId;
    }

    /**
     * @param serviceId The serviceId
     */
    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * @return The serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName The serviceName
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     * @return The category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category The Category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return The plan
     */
    public String getPlan() {
        return plan;
    }

    /**
     * @param plan The Plan
     */
    public void setPlan(String plan) {
        this.plan = plan;
    }

    /**
     * @return The chkService
     */
    public Integer getChkService() {
        return chkService;
    }

    /**
     * @param chkService The chkService
     */
    public void setChkService(Integer chkService) {
        this.chkService = chkService;
    }

    /**
     * @return The serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * @param serviceType The ServiceType
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * @return The planid
     */
    public Integer getPlanid() {
        return planid;
    }

    /**
     * @param planid The planid
     */
    public void setPlanid(Integer planid) {
        this.planid = planid;
    }

    /**
     * @return The nofCalls
     */
    public Integer getNofCalls() {
        return nofCalls;
    }

    /**
     * @param nofCalls The NofCalls
     */
    public void setNofCalls(Integer nofCalls) {
        this.nofCalls = nofCalls;
    }

    /**
     * @return The area
     */
    public String getArea() {
        return area;
    }

    /**
     * @param area The Area
     */
    public void setArea(String area) {
        this.area = area;
    }

    /**
     * @return The uCategory
     */
    public String getUCategory() {
        return uCategory;
    }

    /**
     * @param uCategory The UCategory
     */
    public void setUCategory(String uCategory) {
        this.uCategory = uCategory;
    }

    /**
     * @return The duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration The duration
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * @return The amount
     */
    public Integer getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    /**
     * @return The unlimitedamount
     */
    public Integer getUnlimitedamount() {
        return unlimitedamount;
    }

    /**
     * @param unlimitedamount The unlimitedamount
     */
    public void setUnlimitedamount(Integer unlimitedamount) {
        this.unlimitedamount = unlimitedamount;
    }

    /**
     * @return The uPlanID
     */
    public Integer getUPlanID() {
        return uPlanID;
    }

    /**
     * @param uPlanID The UPlanID
     */
    public void setUPlanID(Integer uPlanID) {
        this.uPlanID = uPlanID;
    }

    /**
     * @return The visitRequired
     */
    public String getVisitRequired() {
        return visitRequired;
    }

    /**
     * @param visitRequired The VisitRequired
     */
    public void setVisitRequired(String visitRequired) {
        this.visitRequired = visitRequired;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
