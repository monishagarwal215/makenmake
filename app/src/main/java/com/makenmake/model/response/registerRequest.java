package com.makenmake.model.response;

/**
 * Created by monish on 14/09/16.
 */
public class registerRequest {
    private String fullname;
    private String emalid;
    private String password;
    private String mobile;

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmalid() {
        return emalid;
    }

    public void setEmalid(String emalid) {
        this.emalid = emalid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
