package com.makenmake.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 04/09/16.
 */
public class LoginRespone extends CommonJsonResponse {
    private int status;
    private int roleId;
    private String userName;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
