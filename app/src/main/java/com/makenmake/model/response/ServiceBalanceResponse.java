package com.makenmake.model.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by saurabh vardani on 15-09-2016.
 */

public class ServiceBalanceResponse {

    @SerializedName("AgreementID")
    @Expose
    private String agreementID;
    @SerializedName("AgreementNumber")
    @Expose
    private String agreementNumber;
    @SerializedName("Invoicedate")
    @Expose
    private String invoicedate;
    @SerializedName("Expirationdate")
    @Expose
    private String expirationdate;
    @SerializedName("PlanStatus")
    @Expose
    private String planStatus;
    @SerializedName("TotalCalls")
    @Expose
    private String totalCalls;
    @SerializedName("PlanAmount")
    @Expose
    private String planAmount;
    @SerializedName("TotalCallDuration")
    @Expose
    private String totalCallDuration;
    @SerializedName("TotalUsedMinutes")
    @Expose
    private String totalUsedMinutes;
    @SerializedName("RemainingCalls")
    @Expose
    private String remainingCalls;
    @SerializedName("RemainingAmount")
    @Expose
    private String remainingAmount;
    @SerializedName("WalletMoney")
    @Expose
    private String walletMoney;

    /**
     *
     * @return
     * The agreementID
     */
    public String getAgreementID() {
        return agreementID;
    }

    /**
     *
     * @param agreementID
     * The AgreementID
     */
    public void setAgreementID(String agreementID) {
        this.agreementID = agreementID;
    }

    /**
     *
     * @return
     * The agreementNumber
     */
    public String getAgreementNumber() {
        return agreementNumber;
    }

    /**
     *
     * @param agreementNumber
     * The AgreementNumber
     */
    public void setAgreementNumber(String agreementNumber) {
        this.agreementNumber = agreementNumber;
    }

    /**
     *
     * @return
     * The invoicedate
     */
    public String getInvoicedate() {
        return invoicedate;
    }

    /**
     *
     * @param invoicedate
     * The Invoicedate
     */
    public void setInvoicedate(String invoicedate) {
        this.invoicedate = invoicedate;
    }

    /**
     *
     * @return
     * The expirationdate
     */
    public String getExpirationdate() {
        return expirationdate;
    }

    /**
     *
     * @param expirationdate
     * The Expirationdate
     */
    public void setExpirationdate(String expirationdate) {
        this.expirationdate = expirationdate;
    }

    /**
     *
     * @return
     * The planStatus
     */
    public String getPlanStatus() {
        return planStatus;
    }

    /**
     *
     * @param planStatus
     * The PlanStatus
     */
    public void setPlanStatus(String planStatus) {
        this.planStatus = planStatus;
    }

    /**
     *
     * @return
     * The totalCalls
     */
    public String getTotalCalls() {
        return totalCalls;
    }

    /**
     *
     * @param totalCalls
     * The TotalCalls
     */
    public void setTotalCalls(String totalCalls) {
        this.totalCalls = totalCalls;
    }

    /**
     *
     * @return
     * The planAmount
     */
    public String getPlanAmount() {
        return planAmount;
    }

    /**
     *
     * @param planAmount
     * The PlanAmount
     */
    public void setPlanAmount(String planAmount) {
        this.planAmount = planAmount;
    }

    /**
     *
     * @return
     * The totalCallDuration
     */
    public String getTotalCallDuration() {
        return totalCallDuration;
    }

    /**
     *
     * @param totalCallDuration
     * The TotalCallDuration
     */
    public void setTotalCallDuration(String totalCallDuration) {
        this.totalCallDuration = totalCallDuration;
    }

    /**
     *
     * @return
     * The totalUsedMinutes
     */
    public String getTotalUsedMinutes() {
        return totalUsedMinutes;
    }

    /**
     *
     * @param totalUsedMinutes
     * The TotalUsedMinutes
     */
    public void setTotalUsedMinutes(String totalUsedMinutes) {
        this.totalUsedMinutes = totalUsedMinutes;
    }

    /**
     *
     * @return
     * The remainingCalls
     */
    public String getRemainingCalls() {
        return remainingCalls;
    }

    /**
     *
     * @param remainingCalls
     * The RemainingCalls
     */
    public void setRemainingCalls(String remainingCalls) {
        this.remainingCalls = remainingCalls;
    }

    /**
     *
     * @return
     * The remainingAmount
     */
    public String getRemainingAmount() {
        return remainingAmount;
    }

    /**
     *
     * @param remainingAmount
     * The RemainingAmount
     */
    public void setRemainingAmount(String remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    /**
     *
     * @return
     * The walletMoney
     */
    public String getWalletMoney() {
        return walletMoney;
    }

    /**
     *
     * @param walletMoney
     * The WalletMoney
     */
    public void setWalletMoney(String walletMoney) {
        this.walletMoney = walletMoney;
    }

}
