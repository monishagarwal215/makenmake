package com.makenmake.model.response;

import com.krapps.model.CommonJsonResponse;
import com.krapps.utils.StringUtils;

/**
 * Created by monish on 11/09/16.
 */
public class LevelTwoResponse extends CommonJsonResponse {
    private int addServicesubcategoryId;
    private int CategoryID;
    private String SubcategoryName;
    private String Title;
    private String Content;
    private String ImageName;


    public int getAddServicesubcategoryId() {
        return addServicesubcategoryId;
    }

    public void setAddServicesubcategoryId(int addServicesubcategoryId) {
        this.addServicesubcategoryId = addServicesubcategoryId;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getSubcategoryName() {
        return SubcategoryName;
    }

    public void setSubcategoryName(String subcategoryName) {
        SubcategoryName = subcategoryName;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }
}
