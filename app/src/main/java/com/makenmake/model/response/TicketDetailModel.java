package com.makenmake.model.response;

/**
 * Created by saurabh vardani on 15-09-2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TicketDetailModel {

    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("AssignedTo")
    @Expose
    private String assignedTo;
    @SerializedName("ModifiedBy")
    @Expose
    private String modifiedBy;
    @SerializedName("Modified")
    @Expose
    private String modified;
    @SerializedName("Remark")
    @Expose
    private String remark;
    @SerializedName("ServiceType")
    @Expose
    private Object serviceType;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The Name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The Status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The assignedTo
     */
    public String getAssignedTo() {
        return assignedTo;
    }

    /**
     *
     * @param assignedTo
     * The AssignedTo
     */
    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    /**
     *
     * @return
     * The modifiedBy
     */
    public String getModifiedBy() {
        return modifiedBy;
    }

    /**
     *
     * @param modifiedBy
     * The ModifiedBy
     */
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /**
     *
     * @return
     * The modified
     */
    public String getModified() {
        return modified;
    }

    /**
     *
     * @param modified
     * The Modified
     */
    public void setModified(String modified) {
        this.modified = modified;
    }

    /**
     *
     * @return
     * The remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     *
     * @param remark
     * The Remark
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     *
     * @return
     * The serviceType
     */
    public Object getServiceType() {
        return serviceType;
    }

    /**
     *
     * @param serviceType
     * The ServiceType
     */
    public void setServiceType(Object serviceType) {
        this.serviceType = serviceType;
    }

}
