package com.makenmake.model.response;

import com.krapps.model.CommonJsonResponse;

/**
 * Created by monish on 15/09/16.
 */
public class ChangePasswordResponse extends CommonJsonResponse{

    private int status;
    private String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
