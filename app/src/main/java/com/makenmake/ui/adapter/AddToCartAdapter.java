package com.makenmake.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makenmake.model.response.AddToCartModel;
import com.makenmake.ui.activity.AddToCartActivity;

import java.util.List;

import wiredsoft.com.makenmake.R;

public class AddToCartAdapter extends RecyclerView.Adapter<AddToCartAdapter.ViewHolder> {
    private final LayoutInflater mInflator;
    private List<AddToCartModel> listing;
    private OnClickListener mClickListener;
    private SparseBooleanArray mSelectedItemsIds;
    private Context mContext;

    public AddToCartAdapter(AddToCartActivity addToCartActivity, Context mContext) {
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mContext = mContext;
        this.mClickListener = addToCartActivity;
        mSelectedItemsIds = new SparseBooleanArray();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_addtocart, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.txtAmount.setText(listing.get(position).getAmount() * listing.get(position).getNoOfService() + "");
        holder.txtNoOfService.setText(listing.get(position).getNoOfService() + "");

        holder.txtServiceName.setText(listing.get(position).getServiceName());
        holder.txtPlus.setTag(position);
        holder.txtMinus.setTag(position);
        holder.imgCoupons.setTag(listing.get(position));


        holder.txtMinus.setOnClickListener(mClickListener);
        holder.txtPlus.setOnClickListener(mClickListener);
        holder.imgCoupons.setOnClickListener(mClickListener);

        holder.imgCross.setTag(position);
        holder.imgCross.setOnClickListener(mClickListener);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listing == null ? 0 : listing.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtServiceName;
        RelativeLayout rtlParent;
        TextView txtAmount;
        TextView txtMinus;
        TextView txtPlus;
        TextView txtNoOfService;
        ImageView imgCoupons;
        ImageView imgCross;

        public ViewHolder(View itemView) {
            super(itemView);
            rtlParent = (RelativeLayout) itemView.findViewById(R.id.rtlParent);
            txtServiceName = (TextView) itemView.findViewById(R.id.txtServiceName);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            txtMinus = (TextView) itemView.findViewById(R.id.txtMinus);
            txtPlus = (TextView) itemView.findViewById(R.id.txtPlus);
            txtNoOfService = (TextView) itemView.findViewById(R.id.txtNoOfService);
            imgCoupons = (ImageView) itemView.findViewById(R.id.imgCoupons);
            imgCross = (ImageView) itemView.findViewById(R.id.imgCross);
        }
    }


    public void setListData(List<AddToCartModel> addToCartModels) {
        this.listing = addToCartModels;
    }

    public List<AddToCartModel> getListData() {
        return listing;
    }




}
