package com.makenmake.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makenmake.model.response.BasicServicesList;
import com.makenmake.model.response.TicketDetailModel;
import com.makenmake.model.response.TicketListModel;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 15-09-2016.
 */
public class TicketListAdapter extends RecyclerView.Adapter<TicketListAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<TicketDetailModel> mlistData;

    public TicketListAdapter(Context context) {
        this.mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<TicketDetailModel> ticketDetailModel) {
        this.mlistData = ticketDetailModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_ticket_history, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.Status.setText(mlistData.get(position).getStatus());
        holder.Modified.setText(mlistData.get(position).getModified());
        holder.Assign_to.setText(mlistData.get(position).getAssignedTo());
    }

    @Override
    public int getItemCount() {
       return mlistData == null ? 0 : mlistData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView Modified;
        TextView Assign_to;
        TextView Status;


        public ViewHolder(View itemView) {
            super(itemView);

            Modified = (TextView) itemView.findViewById(R.id.ServiceType);
            Assign_to = (TextView) itemView.findViewById(R.id.AssignTo);
            Status = (TextView) itemView.findViewById(R.id.Status);

        }
    }

}
