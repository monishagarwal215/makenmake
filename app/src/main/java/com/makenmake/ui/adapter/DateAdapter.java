package com.makenmake.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makenmake.model.response.DateModel;
import com.makenmake.ui.activity.OpenTicketSetDateActivity;
import com.makenmake.ui.activity.SetDateTimeActivity;

import org.joda.time.format.DateTimeFormat;

import java.util.List;

import wiredsoft.com.makenmake.R;

public class DateAdapter extends RecyclerView.Adapter<DateAdapter.ViewHolder> {

    private final float density;
    private Context mContext;
    private LayoutInflater mInflator;
    private List<DateModel> mlistData;
    private OnClickListener mClickListener;
    final int sdk = android.os.Build.VERSION.SDK_INT;


    public DateAdapter(Context context, SetDateTimeActivity setDateTimeActivity) {
        mContext = context;
        density = mContext.getResources().getDisplayMetrics().density;
        mClickListener = setDateTimeActivity;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public DateAdapter(Context context, OpenTicketSetDateActivity setDateTimeFragment) {
        mContext = context;
        density = mContext.getResources().getDisplayMetrics().density;
        mClickListener = setDateTimeFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<DateModel> dateModels) {
        this.mlistData = dateModels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_date, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {

        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) viewholder.lnrFullDate.getLayoutParams();


        if (mlistData.get(position).isSelected()) {
            viewholder.txtDate.setTextColor(Color.WHITE);
            viewholder.txtMonth.setTextColor(Color.WHITE);
            viewholder.txtDay.setTextColor(Color.WHITE);

        /*    int left = (int) (density * 35);
            int top = (int) (density * 30);
            viewholder.lnrFullDate.setPadding(left, top, left, top);*/

            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                viewholder.lnrFullDate.setBackground(mContext.getDrawable(R.drawable.bg_selected_calender));
            }else {
                viewholder.lnrFullDate.setBackgroundResource(R.drawable.bg_selected_calender);
            }


        //    viewholder.lnrFullDate.setBackground(mContext.getDrawable(R.drawable.bg_selected_calender));
        } else {
            viewholder.txtDate.setTextColor(Color.RED);
            viewholder.txtMonth.setTextColor(Color.RED);
            viewholder.txtDay.setTextColor(Color.RED);

/*
            int left = (int) (density * 25);
            int top = (int) (density * 20);
            viewholder.lnrFullDate.setPadding(left, top, left, top);*/
            if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                viewholder.lnrFullDate.setBackground(mContext.getDrawable(R.drawable.bg_unselected_calender));
            }else {
                viewholder.lnrFullDate.setBackgroundResource(R.drawable.bg_unselected_calender);
            }
        }

        viewholder.txtDate.setText(mlistData.get(position).getDateTime().toString("dd") + "");
        viewholder.txtDay.setText(DateTimeFormat.forPattern("EEE").print(mlistData.get(position).getDateTime()) + "");
        viewholder.txtMonth.setText(mlistData.get(position).getDateTime().toString("MMM") + "");

        viewholder.lnrFullDate.setOnClickListener(mClickListener);

        viewholder.lnrFullDate.setTag(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtMonth;
        TextView txtDate;
        TextView txtDay;
        LinearLayout lnrFullDate;

        public ViewHolder(View itemView) {
            super(itemView);

            txtMonth = (TextView) itemView.findViewById(R.id.txtMonth);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            txtDay = (TextView) itemView.findViewById(R.id.txtDay);
            lnrFullDate = (LinearLayout) itemView.findViewById(R.id.lnrFullDate);

        }
    }


}
