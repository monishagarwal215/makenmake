package com.makenmake.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.makenmake.ui.fragment.WalletAmountFragment;
import com.makenmake.ui.fragment.WalletHistoryFragment;

/**
 * Created by saurabh vardani on 19-09-2016.
 */
public class WalletViewPagerAdapter extends FragmentStatePagerAdapter {

    final static String LOG_TAG = "WalletViewPagerAdapter";

    private Context mContext;
    private WalletAmountFragment walletAmountFragment;
    private WalletHistoryFragment walletHistoryFragment;

    public WalletViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (walletAmountFragment == null) {
                walletAmountFragment = new WalletAmountFragment();
            }
            return walletAmountFragment;
        } else if (position == 1) {
            if (walletHistoryFragment == null) {
                walletHistoryFragment = new WalletHistoryFragment();
            }
            return walletHistoryFragment;
        }
        return walletAmountFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return walletAmountFragment;
        } else if (selectedTabPosition == 1) {
            return walletHistoryFragment;
        }
        return walletAmountFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }


    @Override
    public int getCount() {
        return 2;
    }

}
