package com.makenmake.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makenmake.model.response.BasicServicesList;
import com.makenmake.model.response.PlanDataResponse;
import com.makenmake.ui.fragment.BasicServiceFragment;
import com.makenmake.ui.fragment.PackagePlanFragment;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by jyoti goyal on 14-09-2016.
 */
public class UnlimitedPlanAdapter extends RecyclerView.Adapter<UnlimitedPlanAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<BasicServicesList> mlistData;
    private View.OnClickListener mOnClickListener;

    public UnlimitedPlanAdapter(Context context, BasicServiceFragment basicServiceFragment) {
        mContext = context;
        mOnClickListener = basicServiceFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<BasicServicesList> packageServiceModels) {
        this.mlistData = packageServiceModels;
    }

    public List<BasicServicesList> getListData() {
        return mlistData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_unlimited_plan, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        viewholder.txtServiceName.setText(mlistData.get(position).getServiceName());
        viewholder.txtAmount.setText(mlistData.get(position).getAmount() + "");
        viewholder.txtArea.setText(mlistData.get(position).getArea() + "");

        if (mlistData.get(position).isSelected()) {
            viewholder.rtlParent.setBackgroundColor(Color.BLUE);
        } else {
            viewholder.rtlParent.setBackgroundColor(Color.WHITE);
        }

        viewholder.rtlParent.setOnClickListener(mOnClickListener);

        viewholder.rtlParent.setTag(position);

    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtServiceName;
        TextView txtAmount;
        TextView txtArea;
        RelativeLayout rtlParent;


        public ViewHolder(View itemView) {
            super(itemView);
            txtServiceName = (TextView) itemView.findViewById(R.id.txtServiceName);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            txtArea = (TextView) itemView.findViewById(R.id.txtArea);
            rtlParent = (RelativeLayout) itemView.findViewById(R.id.rtlParent);
        }
    }
}
