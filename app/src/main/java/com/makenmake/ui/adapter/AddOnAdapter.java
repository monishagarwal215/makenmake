package com.makenmake.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.makenmake.model.response.AddOnServicesList;
import com.makenmake.model.response.BasicServicesList;
import com.makenmake.ui.fragment.AddOnFragment;
import com.makenmake.ui.fragment.BasicServiceFragment;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by jyoti goyal on 14-09-2016.
 */
public class AddOnAdapter<T> extends RecyclerView.Adapter<AddOnAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<T> mlistData;
    private View.OnClickListener mClickListener;

    public AddOnAdapter(Context context, AddOnFragment addOnFragment) {
        mContext = context;
        mClickListener = (View.OnClickListener) addOnFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AddOnAdapter(Context context, BasicServiceFragment basicServiceFragment) {
        mContext = context;
        mClickListener = (View.OnClickListener) basicServiceFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_add_on, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        if (mlistData.get(position) instanceof BasicServicesList) {
            viewholder.txtServiceName.setText(((BasicServicesList) mlistData.get(position)).getServiceName());

            viewholder.txtAmount.setText("\u20B9" + ((BasicServicesList) mlistData.get(position)).getAmount() * ((BasicServicesList) mlistData.get(position)).getNofCalls() + "");
            viewholder.txtNoOfCalls.setText(((BasicServicesList) mlistData.get(position)).getNofCalls() + "");

            if (((BasicServicesList) mlistData.get(position)).isSelected()) {
                viewholder.rtlParentAddOn.setBackgroundColor(Color.BLUE);
            } else {
                viewholder.rtlParentAddOn.setBackgroundColor(Color.WHITE);
            }
        } else {
            viewholder.txtServiceName.setText(((AddOnServicesList) mlistData.get(position)).getServiceName());

            viewholder.txtAmount.setText("\u20B9" + ((AddOnServicesList) mlistData.get(position)).getAmount() * ((AddOnServicesList) mlistData.get(position)).getNofCalls() + "");
            viewholder.txtNoOfCalls.setText(((AddOnServicesList) mlistData.get(position)).getNofCalls() + "");

            if (((AddOnServicesList) mlistData.get(position)).isSelected()) {
                viewholder.rtlParentAddOn.setBackgroundColor(Color.BLUE);
            } else {
                viewholder.rtlParentAddOn.setBackgroundColor(Color.WHITE);
            }
        }


        viewholder.rtlParentAddOn.setTag(position);
        viewholder.txtPlus.setTag(position);
        viewholder.txtMinus.setTag(position);

        viewholder.rtlParentAddOn.setOnClickListener(mClickListener);
        viewholder.txtMinus.setOnClickListener(mClickListener);
        viewholder.txtPlus.setOnClickListener(mClickListener);
    }

    public void setListData(List<T> addOnServicesLists) {
        this.mlistData = addOnServicesLists;
    }

    public List<T> getListData() {
        return mlistData;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtServiceName;
        TextView txtAmount;
        TextView txtMinus;
        TextView txtPlus;
        TextView txtNoOfCalls;
        RelativeLayout rtlParentAddOn;

        public ViewHolder(View itemView) {
            super(itemView);

            rtlParentAddOn = (RelativeLayout) itemView.findViewById(R.id.rtlParentAddOn);
            txtServiceName = (TextView) itemView.findViewById(R.id.txtServiceName);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            txtNoOfCalls = (TextView) itemView.findViewById(R.id.txtNoOfService);
            txtMinus = (TextView) itemView.findViewById(R.id.txtMinus);
            txtPlus = (TextView) itemView.findViewById(R.id.txtPlus);
        }
    }
}
