package com.makenmake.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.MenuBarResponse;
import com.makenmake.ui.activity.HomeActivity;
import com.makenmake.ui.fragment.HomeFragment;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by Saurabh Vardani on 18-11-2016.
 */

public class MenuBarAdapter2 extends RecyclerView.Adapter<MenuBarAdapter2.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<MenuBarResponse> mListdata;
    private View.OnClickListener mClickListener;

    public MenuBarAdapter2(HomeFragment homeFragment, Context context) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mClickListener = homeFragment;
    }

    public void setListData(List<MenuBarResponse> menuBarResponses) {
        this.mListdata = menuBarResponses;
    }

    public List<MenuBarResponse> getListData() {
        return mListdata;
    }

    @Override
    public MenuBarAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_menu_bar, parent, false);
        return new MenuBarAdapter2.ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(MenuBarAdapter2.ViewHolder viewholder, int position) {
        if (mListdata.get(position).isSelected()) {
            viewholder.txtMenu.setTextColor(Color.parseColor("#E64E3F"));
        } else {
            viewholder.txtMenu.setTextColor(Color.BLACK);
        }

        ((HomeActivity) mContext).loadImage(ApiConstants.URL_IMAGE_BASE + mListdata.get(position).getImageIcon(), viewholder.imgMenu, R.drawable.icon_electrical);

        viewholder.txtMenu.setText(mListdata.get(position).getTitle() + "");
        viewholder.llMenuBar.setTag(position);
        viewholder.llMenuBar.setOnClickListener(mClickListener);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mListdata == null ? 0 : mListdata.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtMenu;
        ImageView imgMenu;
        LinearLayout llMenuBar;

        public ViewHolder(View itemView) {
            super(itemView);
            txtMenu = (TextView) itemView.findViewById(R.id.txtMenu);
            imgMenu = (ImageView) itemView.findViewById(R.id.imgMenu);
            llMenuBar = (LinearLayout) itemView.findViewById(R.id.llMenuBar);
        }
    }
}
