package com.makenmake.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.makenmake.model.response.ServicesList;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 17-09-2016.
 */
public class OpenTicketAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflator;
    private List<ServicesList> mListdata;

    public OpenTicketAdapter(Context context) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<ServicesList> opentTicketlist) {
        this.mListdata = opentTicketlist;
    }

    public List<ServicesList> getListData() {
        return mListdata;
    }


    @Override
    public int getCount() {
        return mListdata == null ? 0 : mListdata.size();
    }

    @Override
    public Object getItem(int position) {
        return mListdata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup container) {
        final ViewHolder viewholder;
        if (convertView == null) {
            viewholder = new ViewHolder();

            convertView = mInflator.inflate(R.layout.list_item_open_ticket_bar, null);

            viewholder.txtMenu = (TextView) convertView.findViewById(R.id.txtMenu);

            convertView.setTag(viewholder);
        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }

        viewholder.txtMenu.setText(mListdata.get(position).getServiceName());


        return convertView;
    }

    public static class ViewHolder {
        TextView txtMenu;
    }

}

