package com.makenmake.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makenmake.model.response.WalletHistoryResponse;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 19-09-2016.
 */
public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<WalletHistoryResponse> mlistData;

    public WalletHistoryAdapter(Context context) {
        mContext = context;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListData(List<WalletHistoryResponse> walletHistoryResponses) {
        this.mlistData = walletHistoryResponses;
    }

    public List<WalletHistoryResponse> getListData() {
        return mlistData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wallet_history, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        viewholder.txtNewAmount.setText("\u20B9 "+mlistData.get(position).getAmount());
        viewholder.txtDescription.setText(mlistData.get(position).getDescription());
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNewAmount;
        TextView txtDescription;


        public ViewHolder(View itemView) {
            super(itemView);
            txtNewAmount = (TextView) itemView.findViewById(R.id.txtNewAmount);
            txtDescription = (TextView) itemView.findViewById(R.id.txtDescription);
        }
    }
}
