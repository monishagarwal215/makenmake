package com.makenmake.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.makenmake.model.response.BasicServicesList;
import com.makenmake.model.response.PlanDataResponse;
import com.makenmake.ui.fragment.PackagePlanFragment;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by jyoti goyal on 14-09-2016.
 */
public class MakeAndALACarteAdapter extends RecyclerView.Adapter<MakeAndALACarteAdapter.ViewHolder> {
    private Context mContext;
    private LayoutInflater mInflator;
    private List<BasicServicesList> mlistData;
    private View.OnClickListener mClickListener;

    public MakeAndALACarteAdapter(Context context, PackagePlanFragment packagePlanFragment) {
        mContext = context;
        mClickListener = (View.OnClickListener) packagePlanFragment;
        mInflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setListdata(List<BasicServicesList> packageServiceModels) {
        this.mlistData = packageServiceModels;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_make_ala_plan, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewholder, int position) {
        viewholder.txtServiceName.setText(mlistData.get(position).getServiceName());


        viewholder.txtAmount.setText(mlistData.get(position).getAmount() * mlistData.get(position).getNofCalls() + "");
        viewholder.txtNoOfCalls.setText(mlistData.get(position).getNofCalls() + "");

        viewholder.txtPlus.setTag(position);
        viewholder.txtMinus.setTag(position);

        viewholder.txtMinus.setOnClickListener(mClickListener);
        viewholder.txtPlus.setOnClickListener(mClickListener);
    }

    public void setListData(List<BasicServicesList> basicServicesLists) {
        this.mlistData = basicServicesLists;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mlistData == null ? 0 : mlistData.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtServiceName;
        TextView txtAmount;
        TextView txtMinus;
        TextView txtPlus;
        TextView txtNoOfCalls;

        public ViewHolder(View itemView) {
            super(itemView);
            txtServiceName = (TextView) itemView.findViewById(R.id.txtServiceName);
            txtAmount = (TextView) itemView.findViewById(R.id.txtAmount);
            txtNoOfCalls = (TextView) itemView.findViewById(R.id.txtNoOfService);
            txtMinus = (TextView) itemView.findViewById(R.id.txtMinus);
            txtPlus = (TextView) itemView.findViewById(R.id.txtPlus);
        }
    }
}
