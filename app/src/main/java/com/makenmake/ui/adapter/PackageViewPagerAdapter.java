package com.makenmake.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.makenmake.ui.fragment.AddOnFragment;
import com.makenmake.ui.fragment.BasicServiceFragment;

/**
 * Created by Monish on 2/2/2016.
 */
public class PackageViewPagerAdapter extends FragmentStatePagerAdapter {

    final static String LOG_TAG = "PackageViewPagerAdapter";

    private Context mContext;
    private BasicServiceFragment basicServiceFragment;
    private AddOnFragment addOnFragment;

    public PackageViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (basicServiceFragment == null) {
                basicServiceFragment = new BasicServiceFragment();
            }
            return basicServiceFragment;
        } else if (position == 1) {
            if (addOnFragment == null) {
                addOnFragment = new AddOnFragment();
            }
            return addOnFragment;
        }
        return basicServiceFragment;
    }

    public Fragment getCurrentFragment(int selectedTabPosition) {
        if (selectedTabPosition == 0) {
            return basicServiceFragment;
        } else if (selectedTabPosition == 1) {
            return addOnFragment;
        }
        return basicServiceFragment;
    }

    public int getItemPosition(Object object) {
        // hack to update adapter on notify data set changes
        return POSITION_NONE;
    }


    @Override
    public int getCount() {
        return 2;
    }

}
