package com.makenmake.ui.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.model.response.AddToCartModel;

import wiredsoft.com.makenmake.R;


@SuppressLint("ValidFragment")
public class CouponDialog extends DialogFragment implements OnClickListener {

    private AddToCartModel addToCartmodel;
    private OnClickListener mClickListener;
    private EditText edtCoupon;
    private TextView txtServiceName;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        setRetainInstance(true);
        return super.onCreateDialog(savedInstanceState);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // remove title and frame from dialog-fragment
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialogDark);
    }

    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogFadeAnimation;
        getDialog().setCanceledOnTouchOutside(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_coupon, container, false);

        edtCoupon = (EditText) view.findViewById(R.id.edtCoupon);
        txtServiceName = (TextView) view.findViewById(R.id.txtServiceName);
        view.findViewById(R.id.btnClose).setOnClickListener(this);

        view.findViewById(R.id.btnCheck).setOnClickListener(mClickListener);

        view.findViewById(R.id.btnCheck).setTag(addToCartmodel);
        txtServiceName.setText(addToCartmodel.getServiceName());
        return view;
    }

    @Override
    public void onClick(View pClickSource) {
        switch (pClickSource.getId()) {
            case R.id.btnClose:
                dismiss();
                break;
        }
    }



    public void setData(AddToCartModel addToCartModel, OnClickListener onClickListener) {
        mClickListener = onClickListener;
        this.addToCartmodel = addToCartModel;
    }

    public String getCouponcode(){
        return edtCoupon.getText().toString();
    }
}