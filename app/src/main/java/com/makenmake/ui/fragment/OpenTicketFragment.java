package com.makenmake.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.ServicesList;
import com.makenmake.model.response.ServicesListResponse;
import com.makenmake.model.response.TimeSloteModel;
import com.makenmake.model.response.UserAgreement;
import com.makenmake.ui.activity.OpenTicketSetDateActivity;
import com.makenmake.ui.adapter.OpenTicketAdapter;
import com.makenmake.utils.MakenMakePrefernece;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 17-09-2016.
 */
public class OpenTicketFragment extends BaseFragment {

    private View mView;
    private GridView gridView;
    private OpenTicketAdapter openTicketAdapter;
    ServicesList servicesList;
    private List<TimeSloteModel> timeSloteModel;
    ArrayList<String> slots;
    private String Agreement_id;

    @Override
    public void setHeader() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_open_ticket, null);

        MakenMakePrefernece.getInstance().setShowDilogorNot(false);

        gridView = (GridView) mView.findViewById(R.id.gridview);
        openTicketAdapter = new OpenTicketAdapter(getActivity());
        gridView.setAdapter(openTicketAdapter);
        slots = new ArrayList<>();

        hitApiRequest(ApiConstants.REQUEST_GET_SERVICES_CLIENT);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                servicesList = openTicketAdapter.getListData().get(position);
                hitApiRequest(ApiConstants.REQUEST_GET_SLOT_ID);
            }

        });


        return mView;
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        String url;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_SERVICES_CLIENT:
                url = ApiConstants.URL_GET_SERVICES_CLIENT + MakenMakePrefernece.getInstance().getUserId() + "&category=d&ticketType=2";
                className = ServicesListResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_GET_SLOT_ID:
                url = ApiConstants.URL_GET_SLOT_ID + servicesList.getServiceId();
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                super.hitApiRequest(reqType);

                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_SERVICES_CLIENT:
                    ServicesListResponse servicesListResponse = (ServicesListResponse) responseObject;
                    if(servicesListResponse.getServicesList().size()==0){
                        ToastUtils.showToast(getActivity(),"No Service available !");
                    }
                    List<ServicesList> temp1 = new ArrayList<>();
                    setAdapter(servicesListResponse.getServicesList());
                    break;
                case ApiConstants.REQUEST_GET_SLOT_ID:
                    Type listType = new TypeToken<List<TimeSloteModel>>() {
                    }.getType();
                    timeSloteModel = new Gson().fromJson(new Gson().toJson(responseObject), listType);
                    getSlotId(timeSloteModel);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setUI(List<UserAgreement> userAgreement) {
        Agreement_id = userAgreement.get(0).getAgreementID() + "";
    }

    private void getSlotId(List<TimeSloteModel> timeSloteModel) {
        for (int a = 0; a < timeSloteModel.size(); a++) {
            slots.add(timeSloteModel.get(a).getSlotID() + "");
        }

        Bundle bundle = new Bundle();
        bundle.putStringArrayList(AppConstants.BUNDLE_SLOTID, slots);
        //bundle.putString(AppConstants.BUNDLE_AGREEMENTID, MakenMakePrefernece.getInstance().getAgreementId());
        bundle.putString(AppConstants.BUNDLE_SERVICE_NAME, servicesList.getServiceName());
        bundle.putString(AppConstants.BUNDLE_CATEGORY, servicesList.getCategory());

        bundle.putString(AppConstants.BUNDLE_SERVICEID, servicesList.getServiceId() + "");
        Intent i = new Intent(getActivity(),OpenTicketSetDateActivity.class);
        i.putExtras(bundle);
        startActivity(i);
    }

    private void setAdapter(List<ServicesList> servicesList) {
        List<ServicesList> temp1 = new ArrayList<>();
        for (ServicesList temp : servicesList) {
            int quantity = Integer.parseInt(temp.getNofCalls());
            if (quantity > 0) {
                temp1.add(temp);
                openTicketAdapter.setListData(temp1);
                openTicketAdapter.notifyDataSetChanged();
            }
        }
    }


}
