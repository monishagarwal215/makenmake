package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.AddOnServicesList;
import com.makenmake.model.response.TicketDetailModel;
import com.makenmake.model.response.TicketListModel;
import com.makenmake.model.response.WalletHistoryResponse;
import com.makenmake.ui.adapter.AddOnAdapter;
import com.makenmake.ui.adapter.WalletHistoryAdapter;
import com.makenmake.utils.MakenMakePrefernece;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani  on 19-09-2016.
 */
public class WalletHistoryFragment extends BaseFragment {
    private View mView;
    private RecyclerView listview;
    private WalletHistoryAdapter walletHistoryAdapter;
    String url;
    private WalletHistoryResponse walletHistoryResponse;


    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_wallet_history, null);

        listview = (RecyclerView) mView.findViewById(R.id.listview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        listview.setLayoutManager(linearLayoutManager);
        walletHistoryAdapter = new WalletHistoryAdapter(getActivity());
        listview.setAdapter(walletHistoryAdapter);
        hitApiRequest(ApiConstants.REQUEST_WALLET_HISTORY);
        return mView;
    }

    public void setWalletHistoryAdapter(List<WalletHistoryResponse> walletHistoryResponses) {
        walletHistoryAdapter.setListData(walletHistoryResponses);
        walletHistoryAdapter.notifyDataSetChanged();
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_WALLET_HISTORY:
                url = ApiConstants.URL_WALLET_History + MakenMakePrefernece.getInstance().getUserId();
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_WALLET_HISTORY:
                    Type listType = new TypeToken<List<WalletHistoryResponse>>() {
                    }.getType();
                    List<WalletHistoryResponse> walletHistoryResponses = new Gson().fromJson(new Gson().toJson(responseObject), listType);

                    setWalletHistoryAdapter(walletHistoryResponses);
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
