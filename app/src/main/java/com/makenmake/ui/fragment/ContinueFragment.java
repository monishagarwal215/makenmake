package com.makenmake.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.krapps.ui.BaseFragment;
import com.makenmake.ui.activity.LoginActivity;
import com.makenmake.ui.activity.RegisterActivity;
import com.makenmake.utils.MakenMakePrefernece;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class ContinueFragment extends BaseFragment {

    Button btnNewUser, btnExistingUser;
    View mView;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_continue, null);
        MakenMakePrefernece.getInstance().setShowDilogorNot(false);
        btnNewUser = (Button) mView.findViewById(R.id.btnNewUser);
        btnExistingUser = (Button) mView.findViewById(R.id.btnExistingUser);

        btnNewUser.setOnClickListener(this);
        btnExistingUser.setOnClickListener(this);

        return mView;
    }


    @Override
    public void setHeader() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnNewUser:
                Intent register  = new Intent(getActivity(),RegisterActivity.class);
                startActivity(register);
                break;
            case R.id.btnExistingUser:
                Intent login  = new Intent(getActivity(),LoginActivity.class);
                startActivity(login);
                break;
        }

    }
}
