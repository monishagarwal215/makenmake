package com.makenmake.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.UserInfoResponse;
import com.makenmake.ui.activity.FirstTimeAddressActivity;
import com.makenmake.utils.MakenMakePrefernece;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 10/09/16.
 */
public class PlanFragment extends BaseFragment{
    private View mView;
    private TextView txtBookNow, txtBigMessage, txtUnlimited, txtMYP, txtALACarte, txtDomestic, txtCommercial;
    public String pack = "U", ser = "D";
    private UserInfoResponse userInfoResponse;

    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_plan, null);
        mView.findViewById(R.id.txtBookNow).setOnClickListener(this);

        MakenMakePrefernece.getInstance().setShowDilogorNot(false);

        txtUnlimited = (TextView) mView.findViewById(R.id.txtUnlimited);
        txtMYP = (TextView) mView.findViewById(R.id.txtMYP);
        txtALACarte = (TextView) mView.findViewById(R.id.txtALACarte);
        txtDomestic = (TextView) mView.findViewById(R.id.txtDomestic);
        txtCommercial = (TextView) mView.findViewById(R.id.txtCommercial);
        txtBookNow = (TextView) mView.findViewById(R.id.txtBookNow);
        txtBigMessage = (TextView) mView.findViewById(R.id.txtBigMessage);

        txtUnlimited.setOnClickListener(this);
        txtMYP.setOnClickListener(this);
        txtALACarte.setOnClickListener(this);
        txtDomestic.setOnClickListener(this);
        txtCommercial.setOnClickListener(this);

        txtUnlimited.setSelected(true);
        txtDomestic.setSelected(true);

        userInfoResponse = new UserInfoResponse();
        if (MakenMakePrefernece.getInstance().getLoggedIn()) {
            hitApiRequest(ApiConstants.REQUEST_USER_INFO);
        }
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_USER_INFO:
                String url = ApiConstants.URL_USER_INFO + MakenMakePrefernece.getInstance().getUserId();
                className = UserInfoResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                super.hitApiRequest(reqType);
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_USER_INFO:
                    userInfoResponse = (UserInfoResponse) responseObject;
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtBookNow:
                if (pack.equals("U") && ser.equals("C")) {
                    AlertDialogUtils.showAlertDialog(getActivity(), "MakenMake", "Please call our customer care at 0124-417-00-00 for further assistance", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                        }
                    });
                } else {
                    if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                        if (!MakenMakePrefernece.getInstance().getPlan().equals("NoPlan")) {
                            AlertDialogUtils.showAlertDialog(getActivity(), "MakenMake", "You have already bought one of the plan. Please call Customer Support 0124-417-00-00 in case of Plan Switch. Visit Home page for buying Additional services", new AlertDialogUtils.OnButtonClickListener() {
                                @Override
                                public void onButtonClick(int buttonId) {
                                }
                            });
                            return;
                        }
                        if (userInfoResponse == null) {
                            return;
                        }
                        if (StringUtils.isNullOrEmpty(userInfoResponse.getDistrictname())) {
                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.EXTRA_USERID, String.valueOf(userInfoResponse.getUserID()));
                            bundle.putString(AppConstants.EXTRA_NAME, userInfoResponse.getName());
                            bundle.putString(AppConstants.EXTRA_DOB, userInfoResponse.getDOB());
                            bundle.putString(AppConstants.EXTRA_EMAILID, userInfoResponse.getEmailid());
                            bundle.putString(AppConstants.EXTRA_GENDER, userInfoResponse.getGender());
                            bundle.putString(AppConstants.EXTRA_MNUMBER, userInfoResponse.getMNumber());
                            bundle.putString(AppConstants.EXTRA_MODIFIED, userInfoResponse.getModified());
                            bundle.putString(AppConstants.EXTRA_CREATED, userInfoResponse.getCreated());
                            bundle.putString(AppConstants.EXTRA_ALTERNATENUMBERS, userInfoResponse.getAlternateNumbers());
                            bundle.putString(AppConstants.EXTRA_FNAME, PlanFragment.class.getSimpleName());
                            Intent i = new Intent(getActivity(), FirstTimeAddressActivity.class);
                            i.putExtras(bundle);
                            startActivity(i);
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putString(AppConstants.EXTRA_PLAN, pack);
                            bundle.putString(AppConstants.EXTRA_SERVICE, ser);
                            ((BaseActivity) getActivity()).replaceFragment(PlanFragment.class.getSimpleName(), new PackagePlanFragment(), bundle, true);
                        }
                    } else {
                        ((BaseActivity) getActivity()).replaceFragment(PlanFragment.class.getSimpleName(), new ContinueFragment(), null, true);

                    }
                }
            case R.id.txtUnlimited:
                txtBigMessage.setText(R.string.unlimited);
                pack = "U";
                txtUnlimited.setSelected(true);
                txtMYP.setSelected(false);
                txtALACarte.setSelected(false);
                break;
            case R.id.txtMYP:
                txtBigMessage.setText(R.string.make_your_plans);
                pack = "M";
                txtUnlimited.setSelected(false);
                txtMYP.setSelected(true);
                txtALACarte.setSelected(false);
                break;
            case R.id.txtALACarte:
                txtBigMessage.setText(R.string.alacarte);
                pack = "F";
                txtUnlimited.setSelected(false);
                txtMYP.setSelected(false);
                txtALACarte.setSelected(true);
                break;
            case R.id.txtDomestic:
                ser = "D";
                txtDomestic.setSelected(true);
                txtCommercial.setSelected(false);
                break;
            case R.id.txtCommercial:
                ser = "C";
                txtDomestic.setSelected(false);
                txtCommercial.setSelected(true);
                break;
            default:
                super.onClick(view);
        }
    }
}
