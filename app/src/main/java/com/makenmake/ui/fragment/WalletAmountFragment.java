package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.WalletAmountResponse;
import com.makenmake.utils.MakenMakePrefernece;

import org.w3c.dom.Text;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 19-09-2016.
 */
public class WalletAmountFragment extends BaseFragment {
    private View mView;
    private TextView txtAmount;
    private String url;
    private WalletAmountResponse walletAmountResponse;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_wallet_amount, null);
        txtAmount = (TextView) mView.findViewById(R.id.txtAmount);
        hitApiRequest(ApiConstants.REQUEST_WALLET_AMOUNT);
        return mView;
    }
    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {

            case ApiConstants.REQUEST_WALLET_AMOUNT:
                url = ApiConstants.URL_WALLET_AMOUNT + MakenMakePrefernece.getInstance().getUserId();
                className = WalletAmountResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_WALLET_AMOUNT:
                    walletAmountResponse = (WalletAmountResponse) responseObject;
                    if(walletAmountResponse.getTotalAmount().equals("-99"))
                        txtAmount.setText("\u20B9" +"0");
                    else
                     txtAmount.setText("\u20B9" +walletAmountResponse.getTotalAmount()+"");


                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
