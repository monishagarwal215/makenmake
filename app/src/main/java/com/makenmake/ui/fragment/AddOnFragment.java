package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.ui.BaseFragment;
import com.makenmake.model.response.AddOnServicesList;
import com.makenmake.ui.adapter.AddOnAdapter;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by monish on 17/09/16.
 */
public class AddOnFragment extends BaseFragment {
    private View mView;
    private RecyclerView listview;
    private AddOnAdapter addOnAdapter;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_add_on, null);

        listview = (RecyclerView) mView.findViewById(R.id.listview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        listview.setLayoutManager(linearLayoutManager);

        addOnAdapter = new AddOnAdapter(getActivity(), this);
        listview.setAdapter(addOnAdapter);

        return mView;
    }

    public void setAddOnAdapter(List<AddOnServicesList> addOnServicesList) {
        addOnAdapter.setListData(addOnServicesList);
        addOnAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        int pos;
        switch (view.getId()) {
            case R.id.txtPlus:
                pos = (int) view.getTag();
                if (addOnAdapter.getListData().get(pos) instanceof AddOnServicesList) {
                    ((AddOnServicesList) addOnAdapter.getListData().get(pos)).setNofCalls(((AddOnServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() + 1);
                    setAddOnAdapter(addOnAdapter.getListData());
                }
                break;
            case R.id.txtMinus:
                pos = (int) view.getTag();
                if (addOnAdapter.getListData().get(pos) instanceof AddOnServicesList) {
                    if (((AddOnServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() > 0) {
                        ((AddOnServicesList) addOnAdapter.getListData().get(pos)).setNofCalls(((AddOnServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() - 1);
                        setAddOnAdapter(addOnAdapter.getListData());
                    }
                }
                break;
            case R.id.rtlParentAddOn:
                pos = (int) view.getTag();
                if (addOnAdapter.getListData().get(pos) instanceof AddOnServicesList) {
                    if (((AddOnServicesList) addOnAdapter.getListData().get(pos)).isSelected()) {
                        ((AddOnServicesList) addOnAdapter.getListData().get(pos)).setSelected(false);
                    } else {
                        ((AddOnServicesList) addOnAdapter.getListData().get(pos)).setSelected(true);
                    }
                    setAddOnAdapter(addOnAdapter.getListData());
                }
                break;
        }
    }
}
