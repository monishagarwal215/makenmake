package com.makenmake.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.AddOnServicesList;
import com.makenmake.model.response.BasicServicesList;
import com.makenmake.model.response.PlanDataResponse;
import com.makenmake.ui.activity.ServiceConfirmationActivity;
import com.makenmake.ui.adapter.PackageViewPagerAdapter;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

import static com.makenmake.ui.fragment.BasicServiceFragment.totalAmount;

/**
 * Created by saurabh vardani on 14-09-2016.
 */
public class PackagePlanFragment extends BaseFragment implements TabLayout.OnTabSelectedListener {
    private View mView;
    private PackageViewPagerAdapter packageViewPagerAdapter;
    private String ser = "D", pack = "U";
    private String url;
    private ViewPager viewPager;
    private TabLayout sliding_tabs;
    private String basic = "<data><ID>%s</ID><Type>%s</Type><PlanID>%s</PlanID><Splan>%s</Splan><quantity>%s</quantity></data>";
    private PlanDataResponse planDataResponse;
    private String data;
    private int totalCount;

    @Override
    public void setHeader() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_package_plan, null);

        setUpSlidingTabLayout();

        Bundle bundle = getArguments();
        if (bundle != null) {
            ser = bundle.getString(AppConstants.EXTRA_SERVICE);
            pack = bundle.getString(AppConstants.EXTRA_PLAN);


            hitApiRequest(ApiConstants.REQUEST_PLAN);
        }

        mView.findViewById(R.id.txtPayment).setOnClickListener(this);
        return mView;
    }

    private void setUpSlidingTabLayout() {
        viewPager = (ViewPager) mView.findViewById(R.id.viewpager);
        //viewPager.setPagingEnabled(false);
        packageViewPagerAdapter = new PackageViewPagerAdapter(getChildFragmentManager(), getActivity());
        viewPager.setAdapter(packageViewPagerAdapter);

        sliding_tabs = (TabLayout) mView.findViewById(R.id.sliding_tabs);
        sliding_tabs.setupWithViewPager(viewPager);
        sliding_tabs.setOnTabSelectedListener(this);

        String title[] = {"Basic", "Add On"};
        // Iterate over all tabs and set the custom view
        for (int i = 0; i < sliding_tabs.getTabCount(); i++) {
            TabLayout.Tab tab = sliding_tabs.getTabAt(i);
            tab.setText(title[i]);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {

            case ApiConstants.REQUEST_PLAN:
                url = ApiConstants.URL_PLAN;
                className = PlanDataResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(getActivity(), this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_SERVICE_SAVE:
                String url = ApiConstants.URL_SERVICE_SAVE + MakenMakePrefernece.getInstance().getUserId() + "&plan=" + pack + "&services=" + data;
                className = String.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_PLAN) {
            params.put(ApiConstants.PARAMS.KEY_CATEGORY, ser);
            params.put(ApiConstants.PARAMS.KEY_PLAN, pack);

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_PLAN:
                    planDataResponse = (PlanDataResponse) responseObject;

                    BasicServiceFragment basicServiceFragment = (BasicServiceFragment) packageViewPagerAdapter.getCurrentFragment(0);
                    AddOnFragment addOnFragment = (AddOnFragment) packageViewPagerAdapter.getCurrentFragment(1);

                    addOnFragment.setAddOnAdapter(planDataResponse.getAddOnServicesList());

                    if (pack.equals("U")) {
                        basicServiceFragment.setBasicServiceAdapter(planDataResponse.getBasicServicesList());
                    } else {
                        basicServiceFragment.setAddOnAdapter(planDataResponse.getBasicServicesList());

                    }
                    break;
                case ApiConstants.REQUEST_SERVICE_SAVE:
                    String reponse = (String) responseObject;
                    if (reponse.equalsIgnoreCase("1")) {
                        Bundle bundle   = new Bundle();
                        bundle.putString("TAG",PackagePlanFragment.class.getSimpleName());
                        bundle.putString("totalAmount", String.valueOf(totalAmount));
                        Intent i  = new Intent(getActivity(), ServiceConfirmationActivity.class);
                        i.putExtras(bundle);
                        startActivity(i);

                    } else {
                        AlertDialogUtils.showAlertDialog(getActivity(), "Service Manager", "Unable to add service", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {

                            }
                        });
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        totalCount = 0;
        switch (view.getId()) {
            case R.id.txtPayment:
                data = getSelectedService();
                if (pack.equals("M")) {
                    if (totalCount < 6) {
                        ToastUtils.showToast(getActivity(), "Please select at least six service");
                        return;
                    }
                }
                if (StringUtils.isNullOrEmpty(data)) {
                    ToastUtils.showToast(getActivity(), "Please select any one service");
                } else {
                    hitApiRequest(ApiConstants.REQUEST_SERVICE_SAVE);
                }
                break;
        }

    }

    private String getSelectedService() {
        StringBuilder buidler = new StringBuilder();
        for (BasicServicesList temp : planDataResponse.getBasicServicesList()) {
            if (temp.isSelected()) {
                String data = String.format(basic, temp.getServiceId(), "B", temp.getPlanid(), pack, temp.getNofCalls()+1);
                totalCount = totalCount + temp.getNofCalls();
                buidler.append(data);
            }
        }

        for (AddOnServicesList temp : planDataResponse.getAddOnServicesList()) {
            if (temp.isSelected()) {
                String data = String.format(basic, temp.getServiceId(), "A", temp.getPlanid(), pack, temp.getNofCalls());
                buidler.append(data);
            }
        }
        return buidler.toString();

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    public String getPack() {
        return pack;
    }

    public String getSer() {
        return ser;
    }
}
