package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.AddOnServicesList;
import com.makenmake.model.response.BasicServicesList;
import com.makenmake.model.response.PlanDataResponse;
import com.makenmake.model.response.WalletAmountResponse;
import com.makenmake.ui.adapter.PackageViewPagerAdapter;
import com.makenmake.ui.adapter.WalletViewPagerAdapter;
import com.makenmake.utils.MakenMakePrefernece;

import org.w3c.dom.Text;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 14-09-2016.
 */
public class MyWalletFragment extends BaseFragment implements TabLayout.OnTabSelectedListener {
    private View mView;
    private ViewPager viewPager;
    private TabLayout sliding_tabs;
    private WalletViewPagerAdapter walletViewPagerAdapter;


    @Override
    public void setHeader() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_mywallet, null);
        MakenMakePrefernece.getInstance().setShowDilogorNot(false);
        setUpSlidingTabLayout();
        return mView;
    }

    private void setUpSlidingTabLayout() {
        viewPager = (ViewPager) mView.findViewById(R.id.viewpager);
        //viewPager.setPagingEnabled(false);
        walletViewPagerAdapter = new WalletViewPagerAdapter(getChildFragmentManager(), getActivity());
        viewPager.setAdapter(walletViewPagerAdapter);

        sliding_tabs = (TabLayout) mView.findViewById(R.id.sliding_tabs);
        sliding_tabs.setupWithViewPager(viewPager);
        sliding_tabs.setOnTabSelectedListener(this);

        String title[] = {"Current", "History"};
        // Iterate over all tabs and set the custom view
        for (int i = 0; i < sliding_tabs.getTabCount(); i++) {
            TabLayout.Tab tab = sliding_tabs.getTabAt(i);
            tab.setText(title[i]);
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
