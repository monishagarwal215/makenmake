
package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.FeedbackResponse;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.ArrayList;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 15/09/16.
 */
public class FeedbackFragment extends BaseFragment {
    private View mView;
    private EditText edtDescription;
    private Spinner Ticket_List;
    private List<String> list_Ticket;
    private int a = 0;
    private String url;

    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_feedback, null);

        MakenMakePrefernece.getInstance().setShowDilogorNot(false);

        list_Ticket = new ArrayList<String>();

        edtDescription = (EditText) mView.findViewById(R.id.edtDescription);

        Ticket_List = (Spinner) mView.findViewById(R.id.spinnerTicket);


        mView.findViewById(R.id.btnStar5).setOnClickListener(this);
        mView.findViewById(R.id.btnStar4).setOnClickListener(this);
        mView.findViewById(R.id.btnStar3).setOnClickListener(this);
        mView.findViewById(R.id.btnStar2).setOnClickListener(this);
        mView.findViewById(R.id.btnStar1).setOnClickListener(this);
        mView.findViewById(R.id.btnSubmit).setOnClickListener(this);

        //    hitApiRequest(ApiConstants.REQUEST_CHECK_TICKET);
        hitApiRequest(ApiConstants.REQUEST_CHECK_TICKET);

        return mView;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_CHECK_TICKET:
                //    url = ApiConstants.URL_CHECK_TICKET + MakenMakePrefernece.getInstance().getUserId() + "&ticketID=" + String.valueOf(Ticket_List.getSelectedItem()) + "&Rating=" + a + "&Description=" + edtDescription.getText().toString().trim();
                url = ApiConstants.URL_CHECK_TICKET + MakenMakePrefernece.getInstance().getUserId();
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_SEND_FEEDBACK_DATA:

                url = ApiConstants.URL_SEND_FEEDBACK_DATA + MakenMakePrefernece.getInstance().getUserId() + "&ticketID=" + Ticket_List.getSelectedItem() + "&Rating=" + a + "&Description=" + edtDescription.getText().toString().trim();
                ;
                className = FeedbackResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }


    //TODO
    //full integration

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }

            switch (reqType) {
                case ApiConstants.REQUEST_CHECK_TICKET:

                    List list = (List) responseObject;
                    if (list.size() != 0) {
                        InsertInSpinner(list);
                    } else {
                        InsetNullInSpinner();
                        Toast.makeText(getActivity(), "There is no purchased Ticket", Toast.LENGTH_LONG).show();
                    }


                    break;
                case ApiConstants.REQUEST_SEND_FEEDBACK_DATA:

                    FeedbackResponse feedbackResponse = (FeedbackResponse) responseObject;
                    if (feedbackResponse.getCode() == 1) {
                        AlertDialogUtils.showAlertDialog(getActivity(), "Service Manager", "Ticket feedback submitted successfully", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {

                                ((BaseActivity) getActivity()).replaceFragment(null, new HomeFragment(), null, false);
                            }
                        });

                    } else {
                        Toast.makeText(getActivity(), "You cannot add more than 1 feedback for one ticket", Toast.LENGTH_LONG).show();
                    }
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private void InsetNullInSpinner() {
        list_Ticket.add("No Ticket");
        Ticket_List.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Ticket));
    }

    private void InsertInSpinner(List j) {
        list_Ticket.add("Select the ticket");
        for (int i = 0; i < j.size(); i++) {

            if (j.get(i).toString().contains(".")) {
                String split[] = j.get(i).toString().split(".");
                int as = j.get(i).toString().indexOf(".");

                String ls = j.get(i).toString().substring(0, as);

                list_Ticket.add(ls);
            }
        }
        Ticket_List.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, list_Ticket));
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnStar5:
                a = 5;
                mView.findViewById(R.id.btnStar5).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_orange));
                mView.findViewById(R.id.btnStar4).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar3).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar2).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar1).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                break;
            case R.id.btnStar4:
                a = 4;
                mView.findViewById(R.id.btnStar4).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_orange));
                mView.findViewById(R.id.btnStar5).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar3).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar2).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar1).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                break;
            case R.id.btnStar3:
                a = 3;
                mView.findViewById(R.id.btnStar3).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_orange));
                mView.findViewById(R.id.btnStar4).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar5).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar2).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar1).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                break;
            case R.id.btnStar2:
                a = 2;
                mView.findViewById(R.id.btnStar2).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_orange));
                mView.findViewById(R.id.btnStar4).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar3).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar5).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar1).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                break;

            case R.id.btnStar1:
                a = 1;
                mView.findViewById(R.id.btnStar1).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_orange));
                mView.findViewById(R.id.btnStar4).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar3).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar2).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                mView.findViewById(R.id.btnStar5).setBackground(getResources().getDrawable(R.drawable.bg_rounded_btn_black));
                break;
            case R.id.btnSubmit:
                if (valid()) {

                    hitApiRequest(ApiConstants.REQUEST_SEND_FEEDBACK_DATA);
                }
                break;


        }
    }

    private boolean valid() {
        if (a == 0) {
            Toast.makeText(getActivity(), "please click on ratings", Toast.LENGTH_LONG).show();
            return false;
        } else if (Ticket_List.getSelectedItem().equals("No Ticket")) {
            Toast.makeText(getActivity(), "please Select the ticket", Toast.LENGTH_LONG).show();
            return false;

        }
        return true;
    }
}

