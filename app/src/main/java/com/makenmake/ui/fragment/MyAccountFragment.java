package com.makenmake.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.ui.BaseFragment;
import com.makenmake.ui.activity.ChangePasswordActivity;
import com.makenmake.ui.activity.HomeActivity;
import com.makenmake.ui.activity.ServiceBalanceActivity;
import com.makenmake.ui.activity.UpdateProfileActivity;
import com.makenmake.utils.MakenMakePrefernece;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 13/09/16.
 */
public class MyAccountFragment extends BaseFragment {
    private View mView;

    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

       // setHasOptionsMenu(true);
        mView = inflater.inflate(R.layout.fragment_my_account, null);

        MakenMakePrefernece.getInstance().setShowDilogorNot(false);

        mView.findViewById(R.id.txtViewUpdateInfo).setOnClickListener(this);
        mView.findViewById(R.id.txtChangePassword).setOnClickListener(this);
        mView.findViewById(R.id.txtServiceBalance).setOnClickListener(this);
        mView.findViewById(R.id.txtLogOut).setOnClickListener(this);

        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.txtViewUpdateInfo:
                Intent update  = new Intent(getActivity(),UpdateProfileActivity.class);
                startActivity(update);
                break;
            case R.id.txtChangePassword:
                Intent change  = new Intent(getActivity(),ChangePasswordActivity.class);
                startActivity(change);
                break;
            case R.id.txtServiceBalance:
                Intent service  = new Intent(getActivity(),ServiceBalanceActivity.class);
                startActivity(service);
                break;
            case R.id.txtLogOut:
                MakenMakePrefernece.getInstance().setLoggedIn(false);
                MakenMakePrefernece.getInstance().createPlanDetails("NoType", "NoPlan", "NoName", "NoAgreementId", "NoCategory");
                Intent log = new Intent(getActivity(), HomeActivity.class);
                log.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(log);
                break;
            default:
                super.onClick(view);
        }
    }

  /*  @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.logout,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:

                break;
        }
        return true;
    }*/
}
