package com.makenmake.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.ReferModel;
import com.makenmake.ui.activity.ReferAndEarnActivity;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 14-09-2016.
 */
public class ReferAndEarnFragment extends BaseFragment {
    private View mView;
    private TextView txtReferCode, txtRefer;
    private String url, referCode;
    private Bundle args;

    @Override
    public void setHeader() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_refer_earn, null);

        MakenMakePrefernece.getInstance().setShowDilogorNot(false);

        args = new Bundle();
        txtReferCode = (TextView) mView.findViewById(R.id.txtReferCode);
        txtRefer = (TextView) mView.findViewById(R.id.txtRefer);
        hitApiRequest(ApiConstants.REQUEST_REFER_EARN);
        mView.findViewById(R.id.txtRefer).setOnClickListener(this);
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_REFER_EARN:
                url = ApiConstants.REFER_EARN + MakenMakePrefernece.getInstance().getUserId();
                className = ReferModel.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_REFER_EARN) {

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_REFER_EARN:
                    ReferModel referModel = (ReferModel) responseObject;
                    txtReferCode.setText(referModel.getCode().toString().trim());
                    referCode =  referModel.getCode().toString();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtRefer:
                Intent refer = new Intent(getActivity(),ReferAndEarnActivity.class);
                refer.putExtra("ReferCode",referCode);
                startActivity(refer);
                break;
            default:
                super.onClick(view);
        }
    }

}
