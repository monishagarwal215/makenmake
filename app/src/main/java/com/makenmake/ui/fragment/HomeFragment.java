package com.makenmake.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.MenuBarResponse;
import com.makenmake.model.response.ServicesList;
import com.makenmake.model.response.ServicesListResponse;
import com.makenmake.model.response.TimeSloteModel;
import com.makenmake.ui.activity.CategoryDetailActivity;
import com.makenmake.ui.activity.OpenTicketSetDateActivity;
import com.makenmake.ui.adapter.MenuBarAdapter2;
import com.makenmake.utils.MakenMakePrefernece;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by monish on 10/09/16.
 */
public class HomeFragment extends BaseFragment implements TextWatcher {
    private View mView;
    private RecyclerView listView;
    private MenuBarAdapter2 menuBarAdapter;
    private List<MenuBarResponse> originalMenuBarResponseList;
    private List<MenuBarResponse> menuBarResponseList = new ArrayList<>();
    MenuBarResponse menuBarResponse;
    private List<TimeSloteModel> timeSloteModel;
    ArrayList<String> slots;


    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_home, null);
        MakenMakePrefernece.getInstance().setShowDilogorNot(true);

        listView = (RecyclerView) mView.findViewById(R.id.listView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        listView.setLayoutManager(linearLayoutManager);
        menuBarAdapter = new MenuBarAdapter2(this, getActivity());
        listView.setAdapter(menuBarAdapter);
        slots = new ArrayList<>();

        if (MakenMakePrefernece.getInstance().getAgreementId() == null && MakenMakePrefernece.getInstance().getLoggedIn()) {
            hitApiRequest(ApiConstants.REQUEST_GET_SERVICE_PURCHASED);
        }

        ((EditText) mView.findViewById(R.id.edtSearch)).addTextChangedListener(this);

        hitApiRequest(ApiConstants.REQUEST_GET_MENU_BAR);

        return mView;
    }

    @Override
    public void onClick(View view) {
        int pos;
        switch (view.getId()) {
            case R.id.llMenuBar:
                pos = (int) view.getTag();
                menuBarResponse = menuBarAdapter.getListData().get(pos);
                boolean selected = menuBarResponse.isSelected();
                if (selected == false) {
                    Intent category  = new Intent(getActivity(),CategoryDetailActivity.class);
                    category.putExtra(AppConstants.BUNDLE_CATEGORY_ID, menuBarResponse.getCategroy1());
                    startActivity(category);
                } else {
                    hitApiRequest(ApiConstants.REQUEST_GET_SLOT_ID);
                }
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_MENU_BAR:
                String url = ApiConstants.URL_GET_MENU_BAR;
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_GET_SERVICES_CLIENT:
                url = ApiConstants.URL_GET_SERVICES_CLIENT + MakenMakePrefernece.getInstance().getUserId() + "&category=d&ticketType=2";
                Log.d("getUserId", "" + MakenMakePrefernece.getInstance().getUserId());
                className = ServicesListResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_GET_SLOT_ID:
                url = ApiConstants.URL_GET_SLOT_ID + menuBarResponse.getServiceID();
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                super.hitApiRequest(reqType);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_MENU_BAR) {
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_MENU_BAR:
                    Type listType = new TypeToken<List<MenuBarResponse>>() {
                    }.getType();
                    originalMenuBarResponseList = new Gson().fromJson(new Gson().toJson(responseObject), listType);
                    setMenuAdapter(originalMenuBarResponseList);
                    if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                        hitApiRequest(ApiConstants.REQUEST_GET_SERVICES_CLIENT);
                    }
                    break;
                case ApiConstants.REQUEST_GET_SERVICES_CLIENT:
                    ServicesListResponse servicesListResponse = (ServicesListResponse) responseObject;
                    setMenuBarSelectedService(servicesListResponse);
                    break;
                case ApiConstants.REQUEST_GET_SLOT_ID:
                    Type listType1 = new TypeToken<List<TimeSloteModel>>() {
                    }.getType();
                    timeSloteModel = new Gson().fromJson(new Gson().toJson(responseObject), listType1);
                    getSlotId(timeSloteModel);
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);//responseObject, isSuccess,reqType
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setMenuBarSelectedService(ServicesListResponse servicesListResponse) {
        for (ServicesList temp : servicesListResponse.getServicesList()) {
            MenuBarResponse tempMenuBar = new MenuBarResponse();
            tempMenuBar.setCategroy1(temp.getCategory1() + "");
            int Quantity = Integer.parseInt(temp.getNofCalls());
            if (originalMenuBarResponseList != null && originalMenuBarResponseList.contains(tempMenuBar) && Quantity>0) {
                int pos = originalMenuBarResponseList.indexOf(tempMenuBar);
                originalMenuBarResponseList.get(pos).setSelected(true);
            }
        }
        setMenuAdapter(originalMenuBarResponseList);
    }


    private void setMenuAdapter(List<MenuBarResponse> menuBarResponseList) {
        menuBarAdapter.setListData(menuBarResponseList);
        menuBarAdapter.notifyDataSetChanged();
    }

    private void getSlotId(List<TimeSloteModel> timeSloteModel) {
        for (int a = 0; a < timeSloteModel.size(); a++) {
            slots.add(timeSloteModel.get(a).getSlotID() + "");
        }


        Bundle bundle = new Bundle();
        bundle.putStringArrayList(AppConstants.BUNDLE_SLOTID, slots);
        bundle.putString(AppConstants.BUNDLE_CATEGORY, MakenMakePrefernece.getInstance().getCategory());
        // bundle.putString(AppConstants.BUNDLE_AGREEMENTID, MakenMakePrefernece.getInstance().getAgreementId());
        bundle.putString(AppConstants.BUNDLE_SERVICE_NAME, menuBarResponse.getTitle());
        bundle.putString(AppConstants.BUNDLE_SERVICEID, menuBarResponse.getServiceID() + "");

        // if (!StringUtils.isNullOrEmpty(MakenMakePrefernece.getInstance().getCategory()) && !StringUtils.isNullOrEmpty(MakenMakePrefernece.getInstance().getAgreementId())) {
        Intent i = new Intent(getActivity(),OpenTicketSetDateActivity.class);
        i.putExtras(bundle);
        startActivity(i);


        //  }

    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence.length() > 1) {
            menuBarResponseList.clear();
            for (MenuBarResponse temp : originalMenuBarResponseList) {
                if (temp.getTitle() != null && temp.getTitle().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                    menuBarResponseList.add(temp);
                }
            }
            setMenuAdapter(menuBarResponseList);
        } else {
            menuBarResponseList.clear();
            menuBarResponseList.addAll(originalMenuBarResponseList);
            setMenuAdapter(menuBarResponseList);
        }


    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
