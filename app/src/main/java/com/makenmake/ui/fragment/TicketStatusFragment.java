package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.TicketListModel;
import com.makenmake.model.response.TicketDetailModel;
import com.makenmake.ui.adapter.TicketListAdapter;
import com.makenmake.utils.MakenMakePrefernece;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 15-09-2016.
 */
public class TicketStatusFragment extends BaseFragment {
    private View mView;
    private String url;
    List<String> list_Ticket;
    String ticket;
    List<TicketListModel> ticket_list;
    List<String> ticket_list1 = new ArrayList<String>();
    Spinner spin;
    private RecyclerView listview;
    private TicketListAdapter ticketListAdapter;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        mView = inflater.inflate(R.layout.fragment_ticket_history, null);
        MakenMakePrefernece.getInstance().setShowDilogorNot(false);

        listview = (RecyclerView) mView.findViewById(R.id.listview);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        listview.setLayoutManager(linearLayoutManager);

        ticketListAdapter = new TicketListAdapter(getActivity());
        listview.setAdapter(ticketListAdapter);


        spin = (Spinner) mView.findViewById(R.id.spinner);
        list_Ticket = new ArrayList<String>();
        hitApiRequest(ApiConstants.REQUEST_TICKET_HISTORY);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View view, int arg2, long arg3) {

                ticket = String.valueOf(spin.getSelectedItem());

                if (!ticket.equals("Please select the ticket")) {
                    hitApiRequest(ApiConstants.REQUEST_DETAILS_TICKET_HISTORY);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });


        return mView;
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(), "Device is out of network");
            return;
        }
        ((BaseActivity) getActivity()).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_TICKET_HISTORY:
                url = ApiConstants.URL_TICKET_HISTORY + MakenMakePrefernece.getInstance().getUserId();
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_DETAILS_TICKET_HISTORY:
                url = ApiConstants.URL_DETAILS_TICKET_HISTORY + ticket;
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(), "Some error occured.");
                return;
            }
            ((BaseActivity) getActivity()).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_TICKET_HISTORY:
                    Type listTicket = new TypeToken<List<TicketListModel>>() {
                    }.getType();
                    ticket_list = new Gson().fromJson(new Gson().toJson(responseObject), listTicket);
                    setSpinnerUi(ticket_list);

                    break;
                case ApiConstants.REQUEST_DETAILS_TICKET_HISTORY:

                    Type listType = new TypeToken<List<TicketDetailModel>>() {
                    }.getType();
                    List<TicketDetailModel> ticketDetailList = new Gson().fromJson(new Gson().toJson(responseObject), listType);
                    setListAdapter(ticketDetailList);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setListAdapter(List<TicketDetailModel> ticketDetailModelList) {
        ticketListAdapter.setListData(ticketDetailModelList);
        ticketListAdapter.notifyDataSetChanged();

    }

    private void setSpinnerUi(List<TicketListModel> checkNumberResponse) {
        //checkNumberResponse.add();
        ticket_list1.add("Please select the ticket");
        for (int i = 0; i < checkNumberResponse.size(); i++) {


            ticket_list1.add(checkNumberResponse.get(i).getTicketID() + "");

        }
        spin.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, ticket_list1));

    }

}
