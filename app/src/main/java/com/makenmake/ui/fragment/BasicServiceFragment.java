package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.krapps.ui.BaseFragment;
import com.makenmake.model.response.BasicServicesList;
import com.makenmake.ui.adapter.AddOnAdapter;
import com.makenmake.ui.adapter.UnlimitedPlanAdapter;

import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by monish on 17/09/16.
 */
public class BasicServiceFragment extends BaseFragment {
    private View mView;
    private RecyclerView listview;
    private UnlimitedPlanAdapter unlimitedPlanAdapter;
    private AddOnAdapter addOnAdapter;

     static int totalAmount=0;

    @Override
    public void setHeader() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_basic_service, null);

        listview = (RecyclerView) mView.findViewById(R.id.listview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        listview.setLayoutManager(linearLayoutManager);
        unlimitedPlanAdapter = new UnlimitedPlanAdapter(getActivity(), this);
        listview.setAdapter(unlimitedPlanAdapter);


        return mView;
    }

    public void setBasicServiceAdapter(List<BasicServicesList> basicServicesList) {
        unlimitedPlanAdapter.setListData(basicServicesList);
        unlimitedPlanAdapter.notifyDataSetChanged();

    }


    public void setAddOnAdapter(List<BasicServicesList> basicServicesList) {
        if (addOnAdapter == null) {
            addOnAdapter = new AddOnAdapter(getActivity(), this);
        }
        listview.setAdapter(addOnAdapter);

        addOnAdapter.setListData(basicServicesList);
        addOnAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View view) {
        int pos;
        switch (view.getId()) {
            case R.id.txtPlus:
                pos = (int) view.getTag();
                if (addOnAdapter.getListData().get(pos) instanceof BasicServicesList) {
                    ((BasicServicesList) addOnAdapter.getListData().get(pos)).setNofCalls(((BasicServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() + 1);
                    setAddOnAdapter(addOnAdapter.getListData());
                }
                break;
            case R.id.txtMinus:
                pos = (int) view.getTag();
                if (addOnAdapter.getListData().get(pos) instanceof BasicServicesList) {
                    if (((PackagePlanFragment) getParentFragment()).getPack().equals("M")) {
                        if (((BasicServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() > 2) {
                            ((BasicServicesList) addOnAdapter.getListData().get(pos)).setNofCalls(((BasicServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() - 1);
                            setAddOnAdapter(addOnAdapter.getListData());
                        }
                    } else if (((PackagePlanFragment) getParentFragment()).getPack().equals("F")) {
                        if (((BasicServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() > 1) {
                            ((BasicServicesList) addOnAdapter.getListData().get(pos)).setNofCalls(((BasicServicesList) addOnAdapter.getListData().get(pos)).getNofCalls() - 1);
                            setAddOnAdapter(addOnAdapter.getListData());
                        }
                    }
                }

                break;
            case R.id.rtlParent:
                pos = (int) view.getTag();
                unSelectAllItem(unlimitedPlanAdapter.getListData());
                selectPosElements(pos, unlimitedPlanAdapter.getListData());
                setBasicServiceAdapter(unlimitedPlanAdapter.getListData());
                break;
            case R.id.rtlParentAddOn:
                pos = (int) view.getTag();
                if (addOnAdapter.getListData().get(pos) instanceof BasicServicesList) {
                    if (((BasicServicesList) addOnAdapter.getListData().get(pos)).isSelected()) {
                        ((BasicServicesList) addOnAdapter.getListData().get(pos)).setSelected(false);
                    } else {
                        ((BasicServicesList) addOnAdapter.getListData().get(pos)).setSelected(true);
                    }
                    setAddOnAdapter(addOnAdapter.getListData());
                }
                break;
        }
    }

    private void selectPosElements(int pos, List<BasicServicesList> listData) {
        int slot = pos / 4;
        int start = slot * 4;
        int end = slot * 4 + 3;
        totalAmount= 0;
        for (int i = start; i <= end; i++) {
            totalAmount = totalAmount+listData.get(i).getAmount();
            listData.get(i).setSelected(true);
        }
    }

    private void unSelectAllItem(List<BasicServicesList> listData) {
        for (BasicServicesList temp : listData) {
            temp.setSelected(false);
        }
    }

}
