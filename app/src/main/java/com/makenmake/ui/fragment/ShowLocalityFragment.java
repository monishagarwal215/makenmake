package com.makenmake.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.krapps.ui.BaseActivity;
import com.krapps.ui.BaseFragment;
import com.makenmake.ui.activity.HomeActivity;
import com.makenmake.utils.MakenMakePrefernece;

import wiredsoft.com.makenmake.R;

/**
 * Created by Saurabh Vardani on 18-11-2016.
 */

public class ShowLocalityFragment extends BaseFragment {
    View mView;
    private Button btnServicesLocality;
    private EditText edtLocation;

    @Override
    public void setHeader() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mView = inflater.inflate(R.layout.fragment_show_locality, null);

        MakenMakePrefernece.getInstance().setShowDilogorNot(false);

        btnServicesLocality = (Button) mView.findViewById(R.id.btnServicesLocality);
        edtLocation = (EditText) mView.findViewById(R.id.edtLocation);
        btnServicesLocality.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnServicesLocality:
                ((HomeActivity)getActivity()).ChangeColor(1);
                ((BaseActivity) getActivity()).replaceFragment(ShowLocalityFragment.class.getSimpleName(), new HomeFragment(), null, true);
                break;
        }
    }
}