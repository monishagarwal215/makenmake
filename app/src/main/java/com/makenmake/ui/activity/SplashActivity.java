package com.makenmake.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.krapps.ui.BaseActivity;
import com.makenmake.utils.MakenMakePrefernece;

import wiredsoft.com.makenmake.R;

/**
 * Created by jyoti goyal on 04-09-2016.
 */
public class SplashActivity extends BaseActivity {
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (MakenMakePrefernece.getInstance().isFirstTimeLaunch()){
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    finish();

                }else {
                    Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }


            }
        }, SPLASH_TIME_OUT);
    }
}

