package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class RegisterActivity extends BaseActivity {
    private EditText edtName,edtPhone,edtEmail,edtPassword,edtConfirmPassword,edtOptionalRefer;
    private Button btnRegister;
    String url;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        edtName = (EditText) findViewById(R.id.name);
        edtPhone = (EditText) findViewById(R.id.number);
        edtEmail = (EditText) findViewById(R.id.email);
        edtPassword = (EditText) findViewById(R.id.password);
        edtOptionalRefer = (EditText) findViewById(R.id.edtOptionalRefer);
        edtConfirmPassword = (EditText) findViewById(R.id.confirmpassword);

        findViewById(R.id.txtReferView).setOnClickListener(this);
        btnRegister = (Button) findViewById(R.id.register);
        btnRegister.setOnClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register:
                if (validateData()) {
                    if (((CheckBox) findViewById(R.id.checkboxAgree)).isChecked()) {
                        hitApiRequest(ApiConstants.REQUEST_CHECK_NUMBER_REGISTER);
                    }
                    else {
                        ToastUtils.showToast(this, "Please check terms and condition");
                    }
                }
                break;
            case R.id.txtReferView:
                findViewById(R.id.txtReferView).setVisibility(View.GONE);
                edtOptionalRefer.setVisibility(View.VISIBLE);
                break;
            default:
                super.onClick(view);
        }

    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtName.getText().toString())) {
            ToastUtils.showToast(this, "Please enter name.");
            return false;
        } else if (!StringUtils.isValidMobileNumber(edtPhone.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile number");
            return false;
        } else if (!StringUtils.isValidEmail(edtEmail.getText().toString(), false)) {
            ToastUtils.showToast(this, "Please enter valid Email.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtConfirmPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (!edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            ToastUtils.showToast(this, "Confirm password do not match");
            return false;
        }
        return true;
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_CHECK_NUMBER_REGISTER:
                url = ApiConstants.URL_CHECK_NUMBER_REGISTER + edtPhone.getText().toString();
                className = String.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_REGISTER:
                url = ApiConstants.URL_REGISTER;
                className = String.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_REGISTER) {
            params.put(ApiConstants.PARAMS.FULLNAME, edtName.getText().toString());
            params.put(ApiConstants.PARAMS.MOBILE, edtPhone.getText().toString());
            params.put(ApiConstants.PARAMS.EMAILID, edtEmail.getText().toString());
            params.put(ApiConstants.PARAMS.PASSWORD1, StringUtils.getBase64String(edtConfirmPassword.getText().toString()));
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_CHECK_NUMBER_REGISTER:
                    String checkNumberResponse = (String) responseObject;
                    if (Integer.parseInt(checkNumberResponse) < 1) {
                        hitApiRequest(ApiConstants.REQUEST_REGISTER);
                    } else {
                        ToastUtils.showToast(this, "Number already exists");
                    }
                    //getUserLooks(userLooksResponse.getResult().getIds());
                    break;
                case ApiConstants.REQUEST_REGISTER:
                    String registerRespone = (String) responseObject;
                    if (!StringUtils.isNullOrEmpty(registerRespone) && TextUtils.isDigitsOnly(registerRespone)) {
                        int res = Integer.parseInt(registerRespone);
                        if (res > 1) {

                            Intent otp  = new Intent(this,OtpActivity.class);
                            otp.putExtra(AppConstants.EXTRA_PHONE_NO, edtPhone.getText().toString());
                            otp.putExtra(AppConstants.EXTRA_USERNAME, edtName.getText().toString());
                            otp.putExtra(AppConstants.FROM_TROUBLE, false);
                            startActivity(otp);
                            finish();
                        }

                    } else {
                        ToastUtils.showToast(this, "Registration failed");
                    }
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
     }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
