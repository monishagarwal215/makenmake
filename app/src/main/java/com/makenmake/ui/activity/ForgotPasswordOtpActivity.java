package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.ChangePasswordResponse;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class ForgotPasswordOtpActivity extends BaseActivity {
    private EditText entOtp;
    private String url;
    private String phoneNumber;
    private EditText edtPass, edtCnfPass;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_otp);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);

        phoneNumber = getIntent().getExtras().getString(AppConstants.EXTRA_PHONE_NO);

        entOtp = (EditText) findViewById(R.id.entOtp);
        edtPass = (EditText) findViewById(R.id.edtpass);
        edtCnfPass = (EditText) findViewById(R.id.textcnfpass);

        findViewById(R.id.txtVerify).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtVerify:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_OTP_VERFICATION);
                }
                break;
            default:
                super.onClick(view);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_OTP_VERFICATION:
                url = ApiConstants.URL_OTP_VERIFICATION;
                className = ChangePasswordResponse.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_OTP_VERFICATION) {
            params.put(ApiConstants.PARAMS.OTP, entOtp.getText().toString());
            params.put(ApiConstants.PARAMS.MOBILE_NUMBER, phoneNumber);
            params.put(ApiConstants.PARAMS.USER_PASSWORD, edtPass.getText().toString());
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_OTP_VERFICATION:
                    ChangePasswordResponse changePasswordResponse = (ChangePasswordResponse) responseObject;
                    if (changePasswordResponse.getStatus() > 0) {
                        AlertDialogUtils.showAlertDialog(this, "Service Manager", "Wow! Password updated successfully", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent intent = new Intent(ForgotPasswordOtpActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        });

                    } else {
                        ToastUtils.showToast(this, "Wrong OTP");
                    }
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(entOtp.getText().toString())) {
            ToastUtils.showToast(this, "Please enter otp.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPass.getText().toString())) {
            ToastUtils.showToast(this, "Please enter Password");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtCnfPass.getText().toString())) {
            ToastUtils.showToast(this, "Please confirm Password");
            return false;
        } else if (!edtPass.getText().toString().equals(edtCnfPass.getText().toString())) {
            ToastUtils.showToast(this, "The password and confirm password do not match");
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}