package com.makenmake.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.UserModel;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 13/09/16.
 */
public class UpdateProfileActivity extends BaseActivity {
    private EditText edtName;
    private EditText edtEmail;
    private EditText edtMobile;
    private EditText edtAddress;
    private EditText edtMobile1;
    private EditText edtMobile2;
    private EditText edtMobile3;
    private EditText edtMobile4;
    String url;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);
        edtName = (EditText) findViewById(R.id.edtName);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtMobile = (EditText) findViewById(R.id.edtMobile);
        edtAddress = (EditText) findViewById(R.id.edtAddress);
        edtMobile1 = (EditText) findViewById(R.id.edtMobile1);
        edtMobile2 = (EditText) findViewById(R.id.edtMobile2);
        edtMobile3 = (EditText) findViewById(R.id.edtMobile3);
        edtMobile4 = (EditText) findViewById(R.id.edtMobile4);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);

        hitApiRequest(ApiConstants.REQUEST_ACCOUNT);
        findViewById(R.id.txtSaveChanges).setOnClickListener(this);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {

            case ApiConstants.REQUEST_ACCOUNT:
                url = ApiConstants.ACCOUNT_URL + MakenMakePrefernece.getInstance().getUserId();
                className = UserModel.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.ALTERNATE_MOBILE:
                String alter_mob1 = edtMobile1.getText().toString().trim();
                String alter_mob2 = edtMobile2.getText().toString().trim();
                String alter_mob3 = edtMobile3.getText().toString().trim();
                String alter_mob4 = edtMobile4.getText().toString().trim();
                url = ApiConstants.ALTERNATE_MOBILE_URL + alter_mob1 + "&alternatemobileno2=" + alter_mob2 + "&alternatemobileno3=" + alter_mob3 + "&alternatemobileno4=" + alter_mob4 + "&userID=" + MakenMakePrefernece.getInstance().getUserId();

                className = String.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_ACCOUNT) {
            //     params.put(ApiConstants.PARAMS.USERID, MakenMakePrefernece.getInstance().getUserId());

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_ACCOUNT:
                    UserModel userModel = (UserModel) responseObject;
                    updateUserModelData(userModel);
                    break;
                case ApiConstants.ALTERNATE_MOBILE:
                    String response = (String) responseObject;
                    if (Integer.parseInt(response) == 1) {
                        AlertDialogUtils.showAlertDialog(this, "Service Manager", "You updated numbers successfully", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                finish();
                            }
                        });
                    } else {
                        ToastUtils.showToast(this, "Number cannot be changed");

                    }
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateUserModelData(UserModel userModel) {
        edtName.setText(userModel.getName());
        edtAddress.setText(userModel.getUserAddress());
        edtMobile.setText(userModel.getMNumber());
        edtEmail.setText(userModel.getEmailid());
        String alternate_number = userModel.getAlternateNumbers();
        if (StringUtils.isNullOrEmpty(alternate_number)) {
            edtMobile1.setText("");
            edtMobile2.setText("");
            edtMobile3.setText("");
            edtMobile4.setText("");

        } else {
            String phoneNos[] = alternate_number.split(",");
            for (int i = 0; i <= phoneNos.length - 1; i++) {
                switch (i) {
                    case 0:
                        edtMobile1.setText(phoneNos[i]);
                        break;
                    case 1:
                        edtMobile2.setText(phoneNos[i]);
                        break;
                    case 2:
                        edtMobile3.setText(phoneNos[i]);
                        break;
                    case 3:
                        edtMobile4.setText(phoneNos[i]);
                        break;
                }
            }
        }
    }

    private boolean validateData() {
        if (!StringUtils.isNullOrEmpty(edtMobile1.getText().toString()) && !StringUtils.isValidMobileNumber(edtMobile1.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile No.1");
            return false;
        } else if (!StringUtils.isNullOrEmpty(edtMobile2.getText().toString()) && !StringUtils.isValidMobileNumber(edtMobile2.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile No.2");
            return false;
        } else if (!StringUtils.isNullOrEmpty(edtMobile3.getText().toString()) && !StringUtils.isValidMobileNumber(edtMobile3.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile No.3");
            return false;
        } else if (!StringUtils.isNullOrEmpty(edtMobile4.getText().toString()) && !StringUtils.isValidMobileNumber(edtMobile4.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile No.4");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtSaveChanges:
                if (validateData()) {
                    hitApiRequest(ApiConstants.ALTERNATE_MOBILE);
                }
                break;
            default:
                super.onClick(view);
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
