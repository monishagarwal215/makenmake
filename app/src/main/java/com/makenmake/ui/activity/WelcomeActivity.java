package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.krapps.ui.BaseActivity;
import com.makenmake.ui.adapter.MyViewPagerAdapter;
import com.makenmake.utils.MakenMakePrefernece;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 15-11-2016.
 */

public class WelcomeActivity extends BaseActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private TextView txtStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        txtStart = (TextView) findViewById(R.id.txtStart);

        txtStart.setOnClickListener(this);

        layouts = new int[]
                {
                        R.layout.view_img_one,
                        R.layout.view_img_two,
                        R.layout.view_img_three
                };

        // adding bottom dots
        addBottomDots(0);


        myViewPagerAdapter = new MyViewPagerAdapter();
        myViewPagerAdapter.setLayout(layouts, getApplicationContext());
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }


        @Override
        public void onPageScrollStateChanged(int arg0) {

        }

    };

    @Override
    public void onClick(View view) {
        Bundle bundle;
        switch (view.getId()) {
            case R.id.txtStart:
                MakenMakePrefernece.getInstance().setFirstTimeLaunch(true);
                Intent intent = new Intent(this,HomeActivity.class);
                startActivity(intent);
                finish();
                break;

            default:
                super.onClick(view);
        }

    }


}
