package com.makenmake.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;

import com.krapps.ui.BaseActivity;
import com.makenmake.constants.AppConstants;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 22-11-2016.
 */

public class WebviewActivity extends BaseActivity {

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);


        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String url = bundle.getString(AppConstants.EXTRA_URL);
            WebView webView = (WebView) findViewById(R.id.webview);
            //webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl(url);
            /*webView.setWebChromeClient(new WebViewClient(
                    on
            ));*/


        }

    }
}
