package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.AddToCartModel;
import com.makenmake.model.response.CouponResponse;
import com.makenmake.ui.adapter.AddToCartAdapter;
import com.makenmake.ui.dialog.CouponDialog;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by monish on 17/09/16.
 */
public class AddToCartActivity extends BaseActivity {
    private RecyclerView listView;
    private AddToCartAdapter addToCartAdapter;
    private int pos;
    private List<AddToCartModel> addToCartModelList;
    private Button btnNext;
    private String basic = "<data><ID>%s</ID><Type>A</Type><PlanID>%s</PlanID><Splan>%s</Splan><quantity>%s</quantity><CouponID>%s</CouponID><CouponDiscountAmount>%s</CouponDiscountAmount><Slotid>%s</Slotid><Prefdate>%s</Prefdate></data>";
    private String service = "";
    private String ServicePlan = "";
    private CouponResponse couponResponse;
    private CouponDialog couponDialog;
    private AddToCartModel addToCartModel;
    private String couponCode;
    TextView cartCount;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart);

        listView = (RecyclerView) findViewById(R.id.listView);
        addToCartAdapter = new AddToCartAdapter(this, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(linearLayoutManager);
        listView.setAdapter(addToCartAdapter);
        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);


        cartCount = (TextView) findViewById(R.id.cartCount);

        changeCart();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        addToCartModelList = MakenMakePrefernece.getInstance().getCart();

        if (MakenMakePrefernece.getInstance().UserPaymentInfo()) {
            if (MakenMakePrefernece.getInstance().getServicePlan().equals("Flexi") || MakenMakePrefernece.getInstance().getServicePlan().equals("")) {
                ServicePlan = "F";
            } else if (MakenMakePrefernece.getInstance().getServicePlan().equals("Basic")) {
                ServicePlan = "B";
            } else {
                ServicePlan = "F";
            }
        } else {
            ServicePlan = "F";
        }

        if (addToCartModelList == null) {
            addToCartModelList = new ArrayList<AddToCartModel>();
        }
        setAdapter(addToCartModelList);
    }

    private void setAdapter(List<AddToCartModel> addToCartModelList) {
        addToCartAdapter.setListData(addToCartModelList);
        addToCartAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtPlus:
                pos = (int) view.getTag();
                addToCartModelList.get(pos).setNoOfService(addToCartModelList.get(pos).getNoOfService() + 1);
                setAdapter(addToCartModelList);
                break;
            case R.id.txtMinus:
                pos = (int) view.getTag();
                if (addToCartModelList.get(pos).getNoOfService() > 1) {
                    addToCartModelList.get(pos).setNoOfService(addToCartModelList.get(pos).getNoOfService() - 1);
                    setAdapter(addToCartModelList);
                }
                break;
            case R.id.btnNext:
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    if (addToCartModelList.size() > 0) {

                        for (int b = 0; b < addToCartModelList.size(); b++) {
                            String services = String.format(basic,
                                    addToCartModelList.get(b).getServiceId(),
                                    addToCartModelList.get(b).getPlanId(),
                                    ServicePlan,
                                    addToCartModelList.get(b).getNoOfService(),
                                    addToCartModelList.get(b).getCouponId(),
                                    addToCartModelList.get(b).getDiscountedPrice(),
                                    addToCartModelList.get(b).getSlotId(),
                                    addToCartModelList.get(b).getPrefDate());
                            service = service.concat(services);
                        }
                        hitApiRequest(ApiConstants.REQUEST_SERVICE_SAVE);
                    } else {
                        ToastUtils.showToast(this, "MakePayment");
                    }
                } else {
                   /* Intent register  = new Intent(getActivity(),ContinueFragment.class);
                    startActivity(register);*/
                }
                break;
            case R.id.imgCoupons:
                addToCartModel = (AddToCartModel) view.getTag();
                couponDialog = new CouponDialog();
                couponDialog.setData(addToCartModel, this);
                couponDialog.show(this.getSupportFragmentManager(), couponDialog.getClass().getSimpleName());
                break;
            case R.id.btnCheck:
                addToCartModel = (AddToCartModel) view.getTag();
                couponCode = couponDialog.getCouponcode();
                if (StringUtils.isNullOrEmpty(couponCode)) {
                    ToastUtils.showToast(this, "Please enter coupon code");
                } else {
                    hitApiRequest(ApiConstants.REQUEST_CHECK_COUPON);
                }
                break;
            case R.id.imgCross:
                pos = (int) view.getTag();
                addToCartModelList.remove(pos);
                addToCartAdapter.notifyDataSetChanged();
                MakenMakePrefernece.getInstance().setCart(addToCartModelList);
                changeCart();
                break;
            default:
                super.onClick(view);
        }

    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_SERVICE_SAVE:
                String url1 = ApiConstants.URL_SERVICE_SAVE + MakenMakePrefernece.getInstance().getUserId() + "&plan=F&services=" + service;
                className = String.class;
                request = VolleyStringRequest.doGet(url1, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url1);
                break;
            case ApiConstants.REQUEST_CHECK_COUPON:
                String url3 = ApiConstants.URL_CHECK_COUPON;
                className = CouponResponse.class;
                request = VolleyStringRequest.doPost(url3, new UpdateListener(this, this, reqType, className) {
                }, getHashParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url3);
                break;
            default:
                url1 = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getHashParams(int reqType) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (reqType == ApiConstants.REQUEST_CHECK_COUPON) {
            hashMap.put(ApiConstants.PARAMS.COUPON_COUPON_CODE, couponCode);
            hashMap.put(ApiConstants.PARAMS.COUPON_USER_ID, MakenMakePrefernece.getInstance().getUserId());
            hashMap.put(ApiConstants.PARAMS.COUPON_SEVICE_ID, String.valueOf(addToCartModel.getServiceId()));
            hashMap.put(ApiConstants.PARAMS.COUPON_PLAN_TYPE, addToCartModel.getType());
            hashMap.put(ApiConstants.PARAMS.COUPON_QUANTITY, String.valueOf(addToCartModel.getNoOfService()));

        }
        return hashMap;
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {

                case ApiConstants.REQUEST_SERVICE_SAVE:
                    String response = (String) responseObject;
                    if (Integer.parseInt(response) > 0) {
                        addToCartModelList.clear();
                        removeAllItemToCart(addToCartModelList);
                        Intent i = new Intent(AddToCartActivity.this,ServiceConfirmationActivity.class);
                        i.putExtra("TAG", AddToCartActivity.class.getSimpleName());
                        i.putExtra("totalAmount", "100");
                        startActivity(i);
                    } else {
                        ToastUtils.showToast(this, "Services cannot be saved.");

                    }

                    break;
                case ApiConstants.REQUEST_CHECK_COUPON:
                    couponResponse = (CouponResponse) responseObject;
                    //  ToastUtils.showToast(getActivity(), couponResponse.getMessage());
                    if (couponResponse.getCouponID() != 0) {
                        if (addToCartModelList.contains(addToCartModel)) {
                            int pos = addToCartModelList.indexOf(addToCartModel);
                            addToCartModelList.get(pos).setCouponId(couponResponse.getCouponID() + "");
                            addToCartModelList.get(pos).setDiscountedPrice(couponResponse.getTotalDisountAmount() + "");
                            ToastUtils.showToast(this, "Coupon applied successfully.");
                            couponDialog.dismiss();
                        }
                    } else {
                        ToastUtils.showToast(this, "Coupon does not exist.");

                    }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void changeCart(){
        if (getCartItemSize() != 0) {
            cartCount.setText(getCartItemSize() + "");
        } else {
            cartCount.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
