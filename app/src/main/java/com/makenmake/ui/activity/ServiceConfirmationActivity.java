package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.GetPaymentResponse;
import com.makenmake.model.response.PaymentList;
import com.makenmake.ui.fragment.PackagePlanFragment;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.HashMap;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by monish on 17/09/16.
 */
public class ServiceConfirmationActivity extends BaseActivity {
    private EditText edtservices;
    private GetPaymentResponse getPaymentResponse;
    Button btnNext;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_confirmation);

        btnNext = (Button) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(this);
        edtservices = (EditText) findViewById(R.id.edtService);
        hitApiRequest(ApiConstants.REQUEST_GET_PAYMENT);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_PAYMENT:
                String url = ApiConstants.URL_GET_PAYMENT + MakenMakePrefernece.getInstance().getUserId();
                className = GetPaymentResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    private HashMap<String, String> getHashParams(int reqType) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (reqType == ApiConstants.REQUEST_GET_PAYMENT) {
        }
        return hashMap;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_PAYMENT:
                    getPaymentResponse = (GetPaymentResponse) responseObject;
                    Bundle bundle = getIntent().getExtras();
                    String total = bundle.getString("totalAmount");
                    String tag = bundle.getString("TAG");
                    if (tag.equals(PackagePlanFragment.class.getSimpleName())) {
                        //getPaymentResponse.getBasicServicesList().get(0).setTotalWithOverhead(total);
                        updateUiTwo(getPaymentResponse.getBasicServicesList());

                    } else if (getPaymentResponse != null && getPaymentResponse.getBasicServicesList() != null) {

                        updateUi(getPaymentResponse.getBasicServicesList());
                        findViewById(R.id.btnNext).setOnClickListener(this);
                    }

                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void updateUiTwo(List<PaymentList> paymentLists) {
        if (paymentLists.size() > 0) {
            ((EditText) findViewById(R.id.edtCategory)).setText(paymentLists.get(0).getCategory());
            ((EditText) findViewById(R.id.edtAmount)).setText("\u20B9 " + String.format("%.1f", Float.parseFloat(paymentLists.get(0).getTotalWithOverhead())) + "");
            ((EditText) findViewById(R.id.edtPlan)).setText(paymentLists.get(0).getPlan());
            ((EditText) findViewById(R.id.edtSaving)).setText("\u20B9 " + String.format("%.1f", Float.parseFloat(paymentLists.get(0).getSaving())) + "");
            ((EditText) findViewById(R.id.edtService)).setText(paymentLists.get(0).getServices());
            ((EditText) findViewById(R.id.edtType)).setText(paymentLists.get(0).getServiceType());
        }
    }

    private void updateUi(List<PaymentList> paymentLists) {
        if (paymentLists.size() > 0) {
            ((EditText) findViewById(R.id.edtCategory)).setText(paymentLists.get(0).getCategory());
            ((EditText) findViewById(R.id.edtAmount)).setText("\u20B9 " + String.format("%.1f", Float.parseFloat(paymentLists.get(0).getTotalWithOverhead())) + "");
            ((EditText) findViewById(R.id.edtPlan)).setText(paymentLists.get(0).getPlan());
            ((EditText) findViewById(R.id.edtSaving)).setText("\u20B9 " + String.format("%.1f", Float.parseFloat(paymentLists.get(0).getSaving())) + "");
            ((EditText) findViewById(R.id.edtService)).setText(paymentLists.get(0).getServices());
            ((EditText) findViewById(R.id.edtType)).setText(paymentLists.get(0).getServiceType());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnNext:
                Bundle bundle = new Bundle();
                if (getPaymentResponse != null) {
                    if (getPaymentResponse.getBasicServicesList() != null && getPaymentResponse.getBasicServicesList().size() > 0) {
                        bundle.putParcelable(AppConstants.EXTRA_PAYMENT_LIST, getPaymentResponse.getBasicServicesList().get(0));
                    }
                    if (getPaymentResponse.getAddOnServicesList() != null && getPaymentResponse.getAddOnServicesList().size() > 0) {
                        bundle.putParcelable(AppConstants.EXTRA_INVOICE_LIST, getPaymentResponse.getAddOnServicesList().get(0));
                    }
                }
                if (getPaymentResponse.getBasicServicesList().get(0).getCategory() != null) {
                    Intent i = new Intent(ServiceConfirmationActivity.this,DoPaymentActivity.class);
                    i.putExtras(bundle);
                    startActivity(i);
                } else {
                    ToastUtils.showToast(this, "Slow Internet");
                }
                break;
        }
        super.onClick(view);
    }
}