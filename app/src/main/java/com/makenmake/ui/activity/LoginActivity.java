package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.LoginRespone;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 18-09-2016.
 */

public class LoginActivity extends BaseActivity {
    private EditText edtMobileNo;
    private EditText edtPassword;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edtPassword = (EditText)findViewById(R.id.edtPassword);
        edtMobileNo = (EditText) findViewById(R.id.edtMobileNo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);

        findViewById(R.id.txtForgotPassword).setOnClickListener(this);
        findViewById(R.id.txtRegister).setOnClickListener(this);
        findViewById(R.id.txtTrouble).setOnClickListener(this);
        findViewById(R.id.txtLogin).setOnClickListener(this);
        //findViewById(R.id.txtCancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Bundle bundle;
        switch (view.getId()) {
            case R.id.txtForgotPassword:
                Intent forgot  = new Intent(this,EnterMobileActivity.class);
                forgot.putExtra(AppConstants.FROM_TROUBLE, false);
                startActivity(forgot);
                break;
            case R.id.txtRegister:
                Intent register  = new Intent(this,RegisterActivity.class);
                startActivity(register);
                break;
            case R.id.txtTrouble:
                Intent trouble  = new Intent(this,EnterMobileActivity.class);
                trouble.putExtra(AppConstants.FROM_TROUBLE, true);
                startActivity(trouble);
                break;
            case R.id.txtLogin:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_LOGIN);
                }
                break;
            default:
                super.onClick(view);
        }

    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_LOGIN:
                showProgressDialog();
                String url = ApiConstants.URL_LOGIN;
                className = LoginRespone.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                super.hitApiRequest(reqType);
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_LOGIN) {
            params.put(ApiConstants.PARAMS.USERNAME, edtMobileNo.getText().toString());
            params.put(ApiConstants.PARAMS.PASSWORD, StringUtils.getBase64String(edtPassword.getText().toString()));
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_LOGIN:
                    LoginRespone loginRespone = (LoginRespone) responseObject;
                    if (loginRespone.getStatus() > 0) {
                        MakenMakePrefernece.getInstance().setLoggedIn(true);
                        MakenMakePrefernece.getInstance().setUserId(loginRespone.getStatus() + "");
                        MakenMakePrefernece.getInstance().setUserName(loginRespone.getUserName());
                        Intent intent = new Intent(this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(intent);
                        hitApiRequest(ApiConstants.REQUEST_GET_SERVICE_PURCHASED);
                    } else {
                        ToastUtils.showToast(this, "Wrong username and password");
                    }
                    //getUserLooks(userLooksResponse.getResult().getIds());
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtMobileNo.getText().toString())) {
            ToastUtils.showToast(this, "Please enter mobile No.");
            return false;
        } else if (StringUtils.isNullOrEmpty(edtPassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter password");
            return false;
        } else if (!StringUtils.isValidMobileNumber(edtMobileNo.getText().toString(), false, 10)) {
            ToastUtils.showToast(this, "Please enter valid mobile No.");
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
