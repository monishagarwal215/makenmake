package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.FirstTimeAddressResponse;
import com.makenmake.model.response.UserInfoResponse;
import com.makenmake.ui.fragment.PlanFragment;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 23-09-2016.
 */

public class FirstTimeAddressActivity extends BaseActivity {

    private EditText txtHomeaddr, txtSecAddr;
    private Spinner spnrState, spnrDistrict, spnrCity;
    private String stateUrl, distUrl, cityUrl, updateUserUrl;
    private List<String> stateList, distList, cityList;
    private List<Integer> stateIdList, distIdList, cityIdList;
    private ArrayAdapter<String> stateAdapter, distAdapter, cityAdapter;
    private String stateName, distName, cityName, userAddress;
    private int stateID, distID, cityID;
    private String userID,name, DOB,emailID,gender,mNumber,modified, created, alternateNumbers, fName;
    private UserInfoResponse userInfoResponse;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_time_address);

        spnrState = (Spinner) findViewById(R.id.spnrState);
        spnrDistrict = (Spinner) findViewById(R.id.spnrDistrict);
        spnrCity = (Spinner) findViewById(R.id.spnrCity);

        txtHomeaddr = (EditText) findViewById(R.id.txtHomeaddr);
        txtSecAddr = (EditText) findViewById(R.id.txtSecAddr);
        findViewById(R.id.txtNext).setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userID = bundle.getString(AppConstants.EXTRA_USERID);
            name = bundle.getString(AppConstants.EXTRA_NAME);
            DOB = bundle.getString(AppConstants.EXTRA_DOB);
            emailID = bundle.getString(AppConstants.EXTRA_EMAILID);
            gender = bundle.getString(AppConstants.EXTRA_GENDER);
            mNumber = bundle.getString(AppConstants.EXTRA_MNUMBER);
            modified = bundle.getString(AppConstants.EXTRA_MODIFIED);
            created = bundle.getString(AppConstants.EXTRA_CREATED);
            alternateNumbers = bundle.getString(AppConstants.EXTRA_ALTERNATENUMBERS);
            fName = bundle.getString(AppConstants.EXTRA_FNAME);
        }

        initializeList();
        hitApiRequest(ApiConstants.REQUEST_STATE_LIST);


        spnrState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateID = stateIdList.get(i);
                stateName = stateList.get(i);
                distUrl = ApiConstants.URL_DISTRICT_LIST + String.valueOf(stateID);
                hitApiRequest(ApiConstants.REQUEST_DISTRICT_LIST);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spnrDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                distID = distIdList.get(i);
                distName = distList.get(i);
                cityUrl = ApiConstants.URL_CITY_LIST + String.valueOf(distID);
                hitApiRequest(ApiConstants.REQUEST_CITY_LIST);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spnrCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                cityID = cityIdList.get(i);
                cityName = cityList.get(i);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void initializeList() {
        stateList = new ArrayList<>();
        distList = new ArrayList<>();
        cityList = new ArrayList<>();
        stateIdList = new ArrayList<>();
        distIdList = new ArrayList<>();
        cityIdList = new ArrayList<>();

        stateAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, stateList);
        cityAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, cityList);
        distAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, distList);

        spnrState.setAdapter(stateAdapter);
        spnrCity.setAdapter(cityAdapter);
        spnrDistrict.setAdapter(distAdapter);

    }



    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_STATE_LIST:
                stateUrl = ApiConstants.URL_STATE_LIST + "4011";
                className = List.class;
                request = VolleyStringRequest.doGet(stateUrl, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, stateUrl);
                break;
            case ApiConstants.REQUEST_DISTRICT_LIST:
                className = List.class;
                request = VolleyStringRequest.doGet(distUrl, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, stateUrl);
                break;
            case ApiConstants.REQUEST_CITY_LIST:
                className = List.class;
                request = VolleyStringRequest.doGet(cityUrl, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, stateUrl);
                break;
            case ApiConstants.REQUEST_UPDATE_USER_INFO:
                updateUserUrl = ApiConstants.URL_UPDATE_USER_INFO;
                userAddress = txtHomeaddr.getText().toString() + txtSecAddr.getText().toString();
                className = String.class;
                request = VolleyStringRequest.doPost(updateUserUrl, new UpdateListener(this, this, reqType, className) {
                }, getHashParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, updateUserUrl);
                break;
            default:
                stateUrl = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getHashParams(int reqType) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (reqType == ApiConstants.REQUEST_UPDATE_USER_INFO) {
            hashMap.put(ApiConstants.PARAMS.ADDRESS_USERID, userID);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_NAME, name);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_DOB, DOB);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_EMAILID, emailID);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_UserAUSERADDRESS, userAddress);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_USERCITY, String.valueOf(cityID));
            hashMap.put(ApiConstants.PARAMS.ADDRESS_USERCOUNTRY, String.valueOf(4011));
            hashMap.put(ApiConstants.PARAMS.ADDRESS_USERSTATE, String.valueOf(stateID));
            hashMap.put(ApiConstants.PARAMS.ADDRESS_GENDER, gender);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_MNUMBER, mNumber);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_MODIFIED, modified);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_CREATED, created);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_USERDISTRICT, String.valueOf(distID));
            hashMap.put(ApiConstants.PARAMS.ADDRESS_CITYNAME, cityName);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_DISTRICTNAME, distName);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_STATENAME, stateName);
            hashMap.put(ApiConstants.PARAMS.ADDRESS_COUNTRYNAME, "India");
            hashMap.put(ApiConstants.PARAMS.ADDRESS_ALTERNATENUMBERS, alternateNumbers);
        }
        return hashMap;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_STATE_LIST:
                    Type listType = new TypeToken<List<FirstTimeAddressResponse>>() {
                    }.getType();
                    List<FirstTimeAddressResponse> firstTimeAddressResponses = new Gson().fromJson(new Gson().toJson(responseObject), listType);
                    stateList.clear();
                    stateIdList.clear();
                    for (int i = 0; i < firstTimeAddressResponses.size(); i++) {
                        stateList.add(firstTimeAddressResponses.get(i).getName());
                        stateIdList.add(firstTimeAddressResponses.get(i).getId());
                    }

                    stateAdapter.notifyDataSetChanged();
                    break;
                case ApiConstants.REQUEST_DISTRICT_LIST:

                    Type listType1 = new TypeToken<List<FirstTimeAddressResponse>>() {
                    }.getType();
                    List<FirstTimeAddressResponse> firstTimeAddressResponses1 = new Gson().fromJson(new Gson().toJson(responseObject), listType1);
                    distList.clear();
                    distIdList.clear();
                    for (int i = 0; i < firstTimeAddressResponses1.size(); i++) {
                        distList.add(firstTimeAddressResponses1.get(i).getName());
                        distIdList.add(firstTimeAddressResponses1.get(i).getId());
                    }
                    distAdapter.notifyDataSetChanged();
                    break;
                case ApiConstants.REQUEST_CITY_LIST:
                    Type listType2 = new TypeToken<List<FirstTimeAddressResponse>>() {
                    }.getType();
                    List<FirstTimeAddressResponse> firstTimeAddressResponses2 = new Gson().fromJson(new Gson().toJson(responseObject), listType2);
                    cityList.clear();
                    cityIdList.clear();
                    for (int i = 0; i < firstTimeAddressResponses2.size(); i++) {
                        cityList.add(firstTimeAddressResponses2.get(i).getName());
                        cityIdList.add(firstTimeAddressResponses2.get(i).getId());
                    }
                    cityAdapter.notifyDataSetChanged();
                    break;
                case ApiConstants.REQUEST_UPDATE_USER_INFO:
                    String Response = (String) responseObject;
                    if (Integer.parseInt(Response) == 1) {
                        if(fName.equals(PlanFragment.class.getSimpleName())) {
                            finish();
                        }else{
                            Intent i = new Intent(FirstTimeAddressActivity.this,AddToCartActivity.class);
                            startActivity(i);
                            finish();
                        }
                    }
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtNext:
                if (((CheckBox) findViewById(R.id.chkTnC)).isChecked()) {
                    hitApiRequest(ApiConstants.REQUEST_UPDATE_USER_INFO);
                }else{
                    ToastUtils.showToast(this, "Please check terms and condition");
                }
                break;
            default:
                super.onClick(view);
        }
    }
}