package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioGroup;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.InvoiceList;
import com.makenmake.model.response.PaymentList;
import com.makenmake.ui.fragment.HomeFragment;
import com.makenmake.utils.MakenMakePrefernece;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 22-09-2016.
 */

public class DoPaymentActivity extends BaseActivity {
    private PaymentList paymentList;
    private InvoiceList invoiceList;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_payment);

        //change plan 1 if new else 0
        //CustomerID is userId
        //Remaining amount cod will -500
        //IsMakeMakeClient 1
        //created by userid
        //invoice +userid + 09242016
        //amount total amount to be paid

        //String a = changeplan + ":" + IsMakenMakeClient + ":" + CustomerID + ":" + RemainingAmount + ":" + CreatedBy + ":" + invoiceNumber + ':' + amount + ':' + lblSevicePlan + ':' + serviceType + ':' + category + ':' + 2 + ':' + 0 + ':' + 0 + ':' + '' + ':' + '' + ':' + 'app'

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            paymentList = bundle.getParcelable(AppConstants.EXTRA_PAYMENT_LIST);
            invoiceList = bundle.getParcelable(AppConstants.EXTRA_INVOICE_LIST);
        }
        findViewById(R.id.txtPay).setOnClickListener(this);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_DO_COD_PAYMENT:
                url = ApiConstants.URL_DO_COD_PAYMENT;
                className = String.class;
                VolleyStringRequest request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;


            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = getBaseParams();

        if (reqType == ApiConstants.REQUEST_DO_COD_PAYMENT) {

            params.put(ApiConstants.PARAMS.USER_IDS, MakenMakePrefernece.getInstance().getUserId());

            if (invoiceList != null && !StringUtils.isNullOrEmpty(invoiceList.getInvoiceNumber())) {
                params.put(ApiConstants.PARAMS.INVOICE_NUMBER, invoiceList.getInvoiceNumber());
            } else {
                String invoiceNumber = MakenMakePrefernece.getInstance().getUserId() + new SimpleDateFormat("MMddyyyy").format(new Date());
                params.put(ApiConstants.PARAMS.INVOICE_NUMBER, invoiceNumber);
            }

            if (paymentList != null && !StringUtils.isNullOrEmpty(paymentList.getTotalWithOverhead() + "")) {
                params.put(ApiConstants.PARAMS.AMOUNT, paymentList.getTotalWithOverhead() + "");
                params.put(ApiConstants.PARAMS.WITHDRAW, paymentList.getTotalWithOverhead() + "");
            }

            if (invoiceList == null) {
                params.put(ApiConstants.PARAMS.CHANGE_PLAN, 1 + "");
            } else {
                params.put(ApiConstants.PARAMS.CHANGE_PLAN, 0 + "");
            }

            params.put(ApiConstants.PARAMS.BALANCE, paymentList.getWalletMoney() + "");

            params.put(ApiConstants.PARAMS.STATUS, 0 + "");

            if (invoiceList != null && !StringUtils.isNullOrEmpty(invoiceList.getPreviousPlan())) {
                params.put(ApiConstants.PARAMS.PLAN, invoiceList.getPreviousPlan());
            } else {
                params.put(ApiConstants.PARAMS.PLAN, "F");
            }
            params.put(ApiConstants.PARAMS.TYPE, paymentList.getServiceType());


            if (invoiceList == null) {
                params.put(ApiConstants.PARAMS.TYPE, "B");
            } else {
                params.put(ApiConstants.PARAMS.TYPE, "AddOn,");
            }
        }


        return params;
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        removeProgressDialog();
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }

            switch (reqType) {
                case ApiConstants.REQUEST_DO_COD_PAYMENT:
                    AlertDialogUtils.showAlertDialog(this, "Make n Make", "Hi " + MakenMakePrefernece.getInstance().getUserName() + ", Your COD request successfull !", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                           Intent intent = new Intent(DoPaymentActivity.this,HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    });
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtPay:
                if (((CheckBox) findViewById(R.id.checkboxAgree)).isChecked()) {
                    getPaymentMode();
                } else {
                    ToastUtils.showToast(this, "Please check terms and condition");
                }
                break;
        }
        super.onClick(view);
    }

    private void getPaymentMode() {
        RadioGroup rbPaymentMode = (RadioGroup) findViewById(R.id.rbPaymentMode);

        switch (rbPaymentMode.getCheckedRadioButtonId()) {
            case R.id.rdbtnCash:
                hitApiRequest(ApiConstants.REQUEST_DO_COD_PAYMENT);
                break;
            case R.id.rdbtnWallet:
                break;
            case R.id.rdbtnCitrus:
                getCitrusPaymentUrl();
                break;
            default:
        }
    }

    private void getCitrusPaymentUrl() {
        int remainingAmount = 0;
        String servicePlan = "";
        String serviceType = "";
        String category = "";
        String services = "";
        String plan = "";
        String walletMoney = "";
        String lblSevicePlan = "";
        String previousPlan = "";

        if (paymentList != null) {
            category = paymentList.getCategory();
            category = category.substring(0, 1);
            if (!StringUtils.isNullOrEmpty(paymentList.getServiceType())) {
                serviceType = paymentList.getServiceType().substring(0, paymentList.getServiceType().length() - 1);
            }
            if (!StringUtils.isNullOrEmpty(paymentList.getServices())) {
                services = paymentList.getServices().substring(0, paymentList.getServices().length() - 1);
            }
            plan = paymentList.getPlan();
            walletMoney = paymentList.getWalletMoney();

            if (plan.equalsIgnoreCase("u")) {
                lblSevicePlan = "Unlimited";
            } else if (plan.equalsIgnoreCase("m")) {
                lblSevicePlan = "Make your Plan";
            } else {
                lblSevicePlan = "Flexi";
            }


            int actualtotalPayment = new BigDecimal(paymentList.getTotal()).intValue();

            int saving = new BigDecimal(paymentList.getSaving()).intValue();
            int totalWithoutTax = new BigDecimal(paymentList.getTotalWithOverhead()).intValue();

            String isAlreadyActivePlan = paymentList.getAlreadyActivePlan();

            if (!isAlreadyActivePlan.equalsIgnoreCase("No") && invoiceList != null) {

                previousPlan = invoiceList.getPreviousPlan();
                if (paymentList.getAlreadyActivePlan().equals(plan)) {

                } else {
                    remainingAmount = new BigDecimal(invoiceList.getAmount()).intValue() - new BigDecimal(invoiceList.getTotalUsedAmount()).intValue();
                }
            } else {
                /*chat.config.Runtime.myAlert('Please try again');
                var MainNavView = Ext.getCmp('mnmServicesList');
                MainNavView.reset();*/
                //get Payment list agian
            }

            String changeplan = "0";
            if (previousPlan.equalsIgnoreCase(plan)) {
                changeplan = "0";
            } else {
                changeplan = "1";
            }


            String isMakenMakeClient = "1";
            String invoiceNumber = getInvoiceId();

            int amount = new BigDecimal(paymentList.getTotalWithOverhead()).intValue();

            String lblServiceType = paymentList.getServiceType();
            String lblcategory = paymentList.getCategory().substring(0, 1);

            StringBuilder builder = new StringBuilder();
            builder.append(changeplan)
                    .append(":")
                    .append(isMakenMakeClient)
                    .append(":")
                    .append(MakenMakePrefernece.getInstance().getUserId())
                    .append(":")
                    .append(remainingAmount)
                    .append(":")
                    .append(MakenMakePrefernece.getInstance().getUserId())
                    .append(":")
                    .append(invoiceNumber)
                    .append(":")
                    .append(amount)
                    .append(":")
                    .append(lblSevicePlan)
                    .append(":")
                    .append(serviceType)
                    .append(":")
                    .append(category)
                    .append(":")
                    .append("2:0:0:0:::app");

            Log.d("Make : ", builder.toString());


 /*           Bundle bundle = new Bundle();
            bundle.putString(AppConstants.EXTRA_URL, AppConstants.CITRUS_PAYMENT_URL + StringUtils.getBase64String(builder.toString()));
            replaceFragment(DoPaymentActivity.class.getSimpleName(), new WebviewFragment(), bundle, true);*/
            Intent intent = new Intent(this,WebviewActivity.class);
            intent.putExtra(AppConstants.EXTRA_URL, AppConstants.CITRUS_PAYMENT_URL + StringUtils.getBase64String(builder.toString()));
            startActivity(intent);
        }


    }

    private String getInvoiceId() {
        return MakenMakePrefernece.getInstance().getUserId() + "27102016";
    }
}