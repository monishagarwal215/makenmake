package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.DateUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.AddToCartModel;
import com.makenmake.model.response.DateModel;
import com.makenmake.model.response.UserInfoResponse;
import com.makenmake.ui.adapter.DateAdapter;
import com.makenmake.ui.fragment.ContinueFragment;
import com.makenmake.ui.fragment.HomeFragment;
import com.makenmake.utils.ActivityControl;
import com.makenmake.utils.MakenMakePrefernece;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 16-09-2016.
 */
public class SetDateTimeActivity extends BaseActivity {
    private List<DateModel> dateModels;
    private RecyclerView listView;
    private DateAdapter dateAdapter;
    private AddToCartModel addToCartModel;
    private ImageView cart;
    private int selectId = -1;
    private String dateSelected;
    private UserInfoResponse userInfoResponse;
    TextView txtSlot1, txtSlot2, txtSlot3;
    TextView cartCount;
    TextView txtBookNow;
    TextView txtAddToCart;
    TextView txtServiceName;
    Integer cartValue = -1;


    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_date_time);

        ActivityControl.setDateTimeActivity = this;

       // addToCartModel = new AddToCartModel();

        cartCount = (TextView) findViewById(R.id.cartCount);

        if (getCartItemSize() != 0) {
            cartCount.setText(getCartItemSize() + "");

        } else {
            cartCount.setVisibility(View.GONE);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cart = (ImageView) findViewById(R.id.imageCart);

        cart.setOnClickListener(this);




        TextView txtDetail = (TextView) findViewById(R.id.txtDetail);
        txtSlot1 = (TextView) findViewById(R.id.txtSlot1);
        txtSlot2 = (TextView) findViewById(R.id.txtSlot2);
        txtSlot3 = (TextView) findViewById(R.id.txtSlot3);
        txtAddToCart = (TextView) findViewById(R.id.txtAddToCart);
        txtBookNow = (TextView) findViewById(R.id.txtBookNow);


        txtAddToCart.setOnClickListener(this);
        txtBookNow.setOnClickListener(this);

        txtServiceName = (TextView) findViewById(R.id.txtServiceName);
        Bundle bundle = getIntent().getExtras();
       if (bundle != null) {
            addToCartModel = bundle.getParcelable(AppConstants.EXTRA_CART);
            txtDetail.setText(addToCartModel.getServiceContent());
            txtServiceName.setText(addToCartModel.getServiceName());
        }

        listView = (RecyclerView) findViewById(R.id.listView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        listView.setLayoutManager(linearLayoutManager);
        dateAdapter = new DateAdapter(this, this);
        listView.setAdapter(dateAdapter);
        getTenDays();

        txtSlot1.setOnClickListener(this);
        txtSlot2.setOnClickListener(this);
        txtSlot3.setOnClickListener(this);

    }

    private void getTenDays() {
        dateModels = new ArrayList<>();
        for (int i = 0; i <= 9; i++) {
            DateModel dateModel = new DateModel();
            DateTime dateTime = new DateTime();
            DateTime dateTimePlusOne = dateTime.plus(Period.days(i));
            dateModel.setDateTime(dateTimePlusOne);
            dateModels.add(dateModel);
        }
        setAdapter(dateModels);
    }

    private void setAdapter(List<DateModel> dateModels) {
        dateAdapter.setListData(dateModels);
        dateAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnrFullDate:
                int pos = (int) view.getTag();
                unselecteAll();
                checkSlots(dateModels.get(pos));
                dateModels.get(pos).setSelected(true);
                unslectRadio();
                TextView date = (TextView) view.findViewById(R.id.txtDate);
                dateSelected = date.getText().toString();
                addToCartModel.setPrefDate(DateUtils.getFormattedDateFromMillis("yyyy-MM-dd", dateModels.get(pos).getDateTime().getMillis()));
                setAdapter(dateModels);
                break;

            case R.id.txtSlot1:
                txtSlot1.setSelected(true);
                txtSlot2.setSelected(false);
                txtSlot3.setSelected(false);
                selectId = 1;
                addToCartModel.setSlotId(getString(R.string.slot_id_morning));
                break;

            case R.id.txtSlot2:
                txtSlot1.setSelected(false);
                txtSlot2.setSelected(true);
                txtSlot3.setSelected(false);
                addToCartModel.setSlotId(getString(R.string.slot_id_afternoon));
                selectId = 2;
                break;

            case R.id.txtSlot3:
                txtSlot1.setSelected(false);
                txtSlot2.setSelected(false);
                txtSlot3.setSelected(true);
                addToCartModel.setSlotId(getString(R.string.slot_id_evening));
                selectId = 3;
                break;

            case R.id.txtAddToCart:
                if (checkValid()) {
                    addItemToCart(addToCartModel);
                    ActivityControl.categoryDetailActivity.finish();
                    ActivityControl.setDateTimeActivity.finish();
                }
                break;

            case R.id.txtBookNow:
                cartValue = 0;
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    hitApiRequest(ApiConstants.REQUEST_USER_INFO);
                } else {
                    //TODO
                    AlertDialogUtils.showAlertDialogWithTwoButtons(this, "Service Manager", "Please login to proceed !", "Ok", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId){
                                case 1:
                                    Intent i = new Intent(SetDateTimeActivity.this, LoginActivity.class);
                                    startActivity(i);
                                    break;
                            }
                        }
                    });
        }
                break;

            case R.id.imageCart:
                cartValue = 1;
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    hitApiRequest(ApiConstants.REQUEST_USER_INFO);
                }else {
                    AlertDialogUtils.showAlertDialogWithTwoButtons(this, "Service Manager", "Please login to proceed !", "Ok", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                        @Override
                        public void onButtonClick(int buttonId) {
                            switch (buttonId) {
                                case 1:
                                    Intent i = new Intent(SetDateTimeActivity.this, LoginActivity.class);
                                    startActivity(i);
                                    break;
                            }
                        }
                    });
                }
                    break;

            default:
                super.onClick(view);
        }
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_USER_INFO:
                String url = ApiConstants.URL_USER_INFO + MakenMakePrefernece.getInstance().getUserId();
                className = UserInfoResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                super.hitApiRequest(reqType);
                break;
        }
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_USER_INFO:
                    userInfoResponse = (UserInfoResponse) responseObject;
                    if (StringUtils.isNullOrEmpty(userInfoResponse.getDistrictname())) {
                        Bundle bundle = new Bundle();
                        addItemToCart(addToCartModel);
                        bundle.putString(AppConstants.EXTRA_USERID, String.valueOf(userInfoResponse.getUserID()));
                        bundle.putString(AppConstants.EXTRA_NAME, userInfoResponse.getName());
                        bundle.putString(AppConstants.EXTRA_DOB, userInfoResponse.getDOB());
                        bundle.putString(AppConstants.EXTRA_EMAILID, userInfoResponse.getEmailid());
                        bundle.putString(AppConstants.EXTRA_GENDER, userInfoResponse.getGender());
                        bundle.putString(AppConstants.EXTRA_MNUMBER, userInfoResponse.getMNumber());
                        bundle.putString(AppConstants.EXTRA_MODIFIED, userInfoResponse.getModified());
                        bundle.putString(AppConstants.EXTRA_CREATED, userInfoResponse.getCreated());
                        bundle.putString(AppConstants.EXTRA_ALTERNATENUMBERS, userInfoResponse.getAlternateNumbers());
                        bundle.putString(AppConstants.EXTRA_FNAME, AddToCartActivity.class.getSimpleName());
                        Intent i = new Intent(SetDateTimeActivity.this, FirstTimeAddressActivity.class);
                        i.putExtras(bundle);
                        startActivity(i);
                    } else {
                       if (cartValue == 0){

                           if(checkValid()) {
                               addItemToCart(addToCartModel);
                               Intent add2cart = new Intent(this, AddToCartActivity.class);
                               startActivity(add2cart);
                           }
                       }else if (cartValue == 1){
                           Intent add2cart = new Intent(this, AddToCartActivity.class);
                           startActivity(add2cart);
                       }

                    }
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void checkSlots(DateModel dateModel) {
        DateTime today = new DateTime();
        if (dateModel.getDateTime().getDayOfYear() == today.getDayOfYear() && dateModel.getDateTime().getYear() == today.getYear()) {
            DateTime morningStart = new DateTime();
            morningStart = morningStart.hourOfDay().setCopy(9);
            morningStart = morningStart.minuteOfHour().setCopy(30);

            DateTime morningEnd = new DateTime();
            morningEnd = morningEnd.hourOfDay().setCopy(12);
            morningEnd = morningEnd.minuteOfHour().setCopy(30);

            DateTime afternoonStart = new DateTime();
            afternoonStart = afternoonStart.hourOfDay().setCopy(13);
            afternoonStart = afternoonStart.minuteOfHour().setCopy(00);

            DateTime afternoonEnd = new DateTime();
            afternoonEnd = afternoonEnd.hourOfDay().setCopy(16);
            afternoonEnd = afternoonEnd.minuteOfHour().setCopy(00);

            DateTime eveningStart = new DateTime();
            eveningStart = eveningStart.hourOfDay().setCopy(16);
            eveningStart = eveningStart.minuteOfHour().setCopy(00);

            DateTime eveningEnd = new DateTime();
            eveningEnd = eveningEnd.hourOfDay().setCopy(21);
            eveningEnd = eveningEnd.minuteOfHour().setCopy(00);

            if (today.isAfter(morningEnd)) {
               // findViewById(R.id.morning).setEnabled(false);
                txtSlot1.setEnabled(false);
                txtSlot1.setFocusable(false);

            } else {
                txtSlot1.setEnabled(true);
                // findViewById(R.id.morning).setEnabled(true);
            }

            if (today.isAfter(afternoonEnd)) {
                txtSlot2.setEnabled(false);
                // findViewById(R.id.afternoon).setEnabled(false);
            } else {
                txtSlot2.setEnabled(true);
               // findViewById(R.id.afternoon).setEnabled(true);
            }

            if (today.isAfter(eveningEnd)) {
               // findViewById(R.id.evening).setEnabled(false);
                txtSlot3.setEnabled(false);
            } else {
                txtSlot3.setEnabled(true);
              //  findViewById(R.id.evening).setEnabled(true);
            }
        } else {
            txtSlot1.setEnabled(true);
            txtSlot2.setEnabled(true);
            txtSlot3.setEnabled(true);
            //  findViewById(R.id.morning).setEnabled(true);
         //   findViewById(R.id.afternoon).setEnabled(true);
         //   findViewById(R.id.evening).setEnabled(true);
        }
    }

    private void unslectRadio() {
        selectId = -1;
        txtSlot1.setSelected(false);
        txtSlot2.setSelected(false);
        txtSlot3.setSelected(false);
    }

    private void unselecteAll() {
        for (DateModel temp : dateModels) {
            temp.setSelected(false);
        }
    }


    private boolean checkValid() {
        if (dateSelected == null) {
            ToastUtils.showToast(this, "Please select date");
            return false;
        } else if (!(selectId > 0)) {
            ToastUtils.showToast(this, "Please choose slots");
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityControl.setDateTimeActivity = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
