package com.makenmake.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.DateUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.DateModel;
import com.makenmake.model.response.OpenTicketDetailsModel;
import com.makenmake.ui.adapter.DateAdapter;
import com.makenmake.ui.fragment.HomeFragment;
import com.makenmake.utils.MakenMakePrefernece;

import org.joda.time.DateTime;
import org.joda.time.Period;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 21-09-2016.
 */
public class OpenTicketSetDateActivity extends BaseActivity {
    RecyclerView listView;
    DateAdapter dateAdapter;
    private List<DateModel> dateModels;
    private ArrayList<String> slots_id;
    private String slot = "";
    private String agreementId;
    private String serviceName;
    private String serviceId;
    private String category;
    TextView txtSlot1, txtSlot2, txtSlot3;
    private String fullDate;
    private EditText Description;
    String description;
    private int selectId = -1;
    String service = "<Root><child><SID>%s</SID><Plan>%s</Plan><cat>%s</cat><type>%s</type><Desc>%s</Desc></child></Root>";


    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_open_ticket_setdate);

        Bundle bundle = getIntent().getExtras();


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (bundle != null) {
            category = bundle.getString(AppConstants.BUNDLE_CATEGORY);
            slots_id = bundle.getStringArrayList(AppConstants.BUNDLE_SLOTID);
            //agreementId = bundle.getString(AppConstants.BUNDLE_AGREEMENTID);
            serviceName = bundle.getString(AppConstants.BUNDLE_SERVICE_NAME);
            serviceId = bundle.getString(AppConstants.BUNDLE_SERVICEID);
        }

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);


        txtSlot1 = (TextView) findViewById(R.id.txtSlot1);
        txtSlot2 = (TextView) findViewById(R.id.txtSlot2);
        txtSlot3 = (TextView) findViewById(R.id.txtSlot3);


        txtSlot1.setOnClickListener(this);
        txtSlot2.setOnClickListener(this);
        txtSlot3.setOnClickListener(this);

        Description = (EditText) findViewById(R.id.edtdesc);
        TextView txtServiceTitle = (TextView) findViewById(R.id.txtServiceTitle);
        txtServiceTitle.setText(serviceName);

        if (!StringUtils.isNullOrEmpty(category)) {
            if (category.equals("Domestic")) {
                this.category = "D";
            } else if (category.equals("Commercial")) {
                this.category = "C";
            }
        }


        listView = (RecyclerView) findViewById(R.id.listView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        listView.setLayoutManager(linearLayoutManager);
        dateAdapter = new DateAdapter(this, this);
        listView.setAdapter(dateAdapter);
        getTenDays();

        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
    }

    private void getTenDays() {
        dateModels = new ArrayList<>();
        for (int i = 0; i <= 9; i++) {
            DateModel dateModel = new DateModel();
            DateTime dateTime = new DateTime();
            DateTime dateTimePlusOne = dateTime.plus(Period.days(i));
            dateModel.setDateTime(dateTimePlusOne);
            dateModels.add(dateModel);
        }

        setAdapter(dateModels);
    }

    private void setAdapter(List<DateModel> dateModels) {
        dateAdapter.setListData(dateModels);
        dateAdapter.notifyDataSetChanged();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lnrFullDate:
                int pos = (int) view.getTag();
                unselecteAll();
                checkSlots(dateModels.get(pos));
                dateModels.get(pos).setSelected(true);
                unslectRadio();
                TextView date = (TextView) view.findViewById(R.id.txtDate);
                TextView month = (TextView) view.findViewById(R.id.txtMonth);
                String monthNumber = DateUtils.getMonthNumber(month.getText().toString());
                Calendar calender = Calendar.getInstance();
                int year = calender.get(Calendar.YEAR);
                fullDate = year + "-" + monthNumber + "-" + date.getText().toString();
                setAdapter(dateModels);
                break;
            case R.id.txtSlot1:
                txtSlot1.setSelected(true);
                txtSlot2.setSelected(false);
                txtSlot3.setSelected(false);
                selectId = 1;
                slot = slots_id.get(0);
                break;

            case R.id.txtSlot2:
                txtSlot1.setSelected(false);
                txtSlot2.setSelected(true);
                txtSlot3.setSelected(false);
                selectId = 2;
                slot = slots_id.get(1);

                break;

            case R.id.txtSlot3:
                txtSlot1.setSelected(false);
                txtSlot2.setSelected(false);
                txtSlot3.setSelected(true);
                selectId = 3;
                slot = slots_id.get(2);
                break;


            case R.id.btnSubmit:
                if (checkValid()) {
                    hitApiRequest(ApiConstants.REQUEST_TICKET_DETAILS);
                }

                break;

            default:
                super.onClick(view);
        }
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_TICKET_DETAILS:
                String url = ApiConstants.URL_TICKET_DETAILS;
                className = OpenTicketDetailsModel.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                super.hitApiRequest(reqType);

                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_TICKET_DETAILS) {

            String Services = String.format(service, serviceId, MakenMakePrefernece.getInstance().getServicePlan(), category, MakenMakePrefernece.getInstance().getType(), description);


            params.put(ApiConstants.PARAMS.USERS_ID, MakenMakePrefernece.getInstance().getUserId());
            params.put(ApiConstants.PARAMS.CREATEDFOR, MakenMakePrefernece.getInstance().getUserId());

            params.put(ApiConstants.PARAMS.AGREEMENTID, MakenMakePrefernece.getInstance().getAgreementId());

            params.put(ApiConstants.PARAMS.TYPE, "2");
            params.put(ApiConstants.PARAMS.DESC, description);
            params.put(ApiConstants.PARAMS.PREFERRED_DATE, fullDate);
            params.put(ApiConstants.PARAMS.SLOT, slot);
            params.put(ApiConstants.PARAMS.SERVICES, Services);

        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            ((BaseActivity) this).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_TICKET_DETAILS:
                    OpenTicketDetailsModel openTicketDetailsModel = (OpenTicketDetailsModel) responseObject;
                    Toast.makeText(this, "Hi,You! Your Service ticket Id is " + openTicketDetailsModel.getResult() + ". Our engineer will serve you shortly. Please log in with your account details on our website (www.makenmake.in) to see the status of the ticket.", Toast.LENGTH_LONG).show();
                  finish();
                    break;
                default:
                    super.updateView(responseObject, isSuccess, reqType);//responseObject, isSuccess,reqType
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    private boolean checkValid() {
        description = Description.getText().toString().trim();
        if (fullDate == null) {
            ToastUtils.showToast(this, "please Select The Date");
            return false;
        } else if (slot.equals("")) {
            ToastUtils.showToast(this, "please Select The Slot");
            return false;
        } else if (description.equals("")) {
            description = "w";
            return true;
        }

        return true;
    }

    private void unselecteAll() {
        for (DateModel temp : dateModels) {
            temp.setSelected(false);
        }
    }


    private void unslectRadio() {
        slot = "";
        txtSlot1.setSelected(false);
        txtSlot2.setSelected(false);
        txtSlot3.setSelected(false);
    }

/*
    private void checkSlots(DateModel dateModel) {
        DateTime today = new DateTime();
        if (dateModel.getDateTime().getDayOfYear() == today.getDayOfYear() && dateModel.getDateTime().getYear() == today.getYear()) {
            DateTime morningStart = new DateTime();
            morningStart = morningStart.minuteOfHour().setCopy(9);
            morningStart = morningStart.secondOfMinute().setCopy(30);

            DateTime morningEnd = new DateTime();
            morningEnd = morningEnd.minuteOfHour().setCopy(12);
            morningEnd = morningEnd.secondOfMinute().setCopy(30);

            DateTime afternoonStart = new DateTime();
            afternoonStart = afternoonStart.minuteOfHour().setCopy(13);
            afternoonStart = afternoonStart.secondOfMinute().setCopy(00);

            DateTime afternoonEnd = new DateTime();
            afternoonEnd = afternoonEnd.minuteOfHour().setCopy(16);
            afternoonEnd = afternoonEnd.secondOfMinute().setCopy(00);

            DateTime eveningStart = new DateTime();
            eveningStart = eveningStart.minuteOfHour().setCopy(16);
            eveningStart = eveningStart.secondOfMinute().setCopy(00);

            DateTime eveningEnd = new DateTime();
            eveningEnd = eveningEnd.minuteOfHour().setCopy(21);
            eveningEnd = eveningEnd.secondOfMinute().setCopy(00);

            if (today.isAfter(morningEnd)) {
                findViewById(R.id.morning).setEnabled(false);
            } else {
                findViewById(R.id.morning).setEnabled(true);
            }

            if (today.isAfter(afternoonEnd)) {
                findViewById(R.id.afternoon).setEnabled(false);
            } else {
                findViewById(R.id.afternoon).setEnabled(true);
            }

            if (today.isAfter(eveningEnd)) {
                findViewById(R.id.evening).setEnabled(false);
            } else {
                findViewById(R.id.evening).setEnabled(true);
            }
        } else {
            findViewById(R.id.morning).setEnabled(true);
            findViewById(R.id.afternoon).setEnabled(true);
            findViewById(R.id.evening).setEnabled(true);
        }
    }
*/
private void checkSlots(DateModel dateModel) {
    DateTime today = new DateTime();
    if (dateModel.getDateTime().getDayOfYear() == today.getDayOfYear() && dateModel.getDateTime().getYear() == today.getYear()) {
        DateTime morningStart = new DateTime();
        morningStart = morningStart.hourOfDay().setCopy(9);
        morningStart = morningStart.minuteOfHour().setCopy(30);

        DateTime morningEnd = new DateTime();
        morningEnd = morningEnd.hourOfDay().setCopy(12);
        morningEnd = morningEnd.minuteOfHour().setCopy(30);

        DateTime afternoonStart = new DateTime();
        afternoonStart = afternoonStart.hourOfDay().setCopy(13);
        afternoonStart = afternoonStart.minuteOfHour().setCopy(00);

        DateTime afternoonEnd = new DateTime();
        afternoonEnd = afternoonEnd.hourOfDay().setCopy(16);
        afternoonEnd = afternoonEnd.minuteOfHour().setCopy(00);

        DateTime eveningStart = new DateTime();
        eveningStart = eveningStart.hourOfDay().setCopy(16);
        eveningStart = eveningStart.minuteOfHour().setCopy(00);

        DateTime eveningEnd = new DateTime();
        eveningEnd = eveningEnd.hourOfDay().setCopy(21);
        eveningEnd = eveningEnd.minuteOfHour().setCopy(00);

        if (today.isAfter(morningEnd)) {
            // findViewById(R.id.morning).setEnabled(false);
            txtSlot1.setEnabled(false);
            txtSlot1.setFocusable(false);

        } else {
            txtSlot1.setEnabled(true);
        }

        if (today.isAfter(afternoonEnd)) {
            txtSlot2.setEnabled(false);
        } else {
            txtSlot2.setEnabled(true);
        }

        if (today.isAfter(eveningEnd)) {
            txtSlot3.setEnabled(false);
        } else {
            txtSlot3.setEnabled(true);
        }
    } else {
        txtSlot1.setEnabled(true);
        txtSlot2.setEnabled(true);
        txtSlot3.setEnabled(true);
    }
}



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
