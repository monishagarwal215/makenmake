package com.makenmake.ui.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.UserInfoResponse;
import com.makenmake.ui.fragment.ContinueFragment;
import com.makenmake.ui.fragment.FeedbackFragment;
import com.makenmake.ui.fragment.HomeFragment;
import com.makenmake.ui.fragment.MyAccountFragment;
import com.makenmake.ui.fragment.MyWalletFragment;
import com.makenmake.ui.fragment.OpenTicketFragment;
import com.makenmake.ui.fragment.PlanFragment;
import com.makenmake.ui.fragment.ReferAndEarnFragment;
import com.makenmake.ui.fragment.ShowLocalityFragment;
import com.makenmake.ui.fragment.TicketStatusFragment;
import com.makenmake.utils.MakenMakePrefernece;

import wiredsoft.com.makenmake.R;



public class HomeActivity extends BaseActivity{


    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ImageView cart;
    private TextView cartCount;
    private UserInfoResponse userInfoResponse;
    public PackageInfo pInfo;
    TextView[] textView;
    private int[] imgInactive;
    private int[] imgActive;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       /* pInfo = null;
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            if(verCode==1.0){
                AlertDialogUtils.showAlertDialog(this, "MakenMake", "Please update your app!", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        Intent intent = new Intent(Intent.ACTION_VIEW , Uri.parse("market://details?id=com.app.makenmake"));
                        startActivity(intent);
                    }
                });

            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }

        imgInactive = new int[]{
                R.drawable.address_white,
                R.drawable.home_white,
                R.drawable.user_white,
                R.drawable.ticket_white,
                R.drawable.wallet_white,
                R.drawable.ticket_history_white,
                R.drawable.refer_earn_white,
                R.drawable.plan_white,
                R.drawable.feedback_white
        };

        imgActive = new int[]{
                R.drawable.address_red,
                R.drawable.home_red,
                R.drawable.user_red,
                R.drawable.ticket_red,
                R.drawable.wallet_red,
                R.drawable.ticket_history_red,
                R.drawable.refer_earn_red,
                R.drawable.plan_red,
                R.drawable.feedback_red,
        };
        textView = new TextView[9];

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_img);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        cartCount = (TextView) findViewById(R.id.cartCount);
        updateCartValue(getCartItemSize());


        TextView txtname = (TextView) findViewById(R.id.txtname);
        TextView txtfirstdigit = (TextView) findViewById(R.id.txtfirstdigit);
        ImageView imgUserLogin = (ImageView) findViewById(R.id.imgUserLogin);


        cart = (ImageView) findViewById(R.id.imageCart);

        cart.setOnClickListener(this);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        setUpDrawer();


        textView[0]  = (TextView) findViewById(R.id.txtAddress);
        textView[1] = (TextView) findViewById(R.id.txtHome);
        textView[2] = (TextView) findViewById(R.id.txtMyAccount);
        textView[3] = (TextView) findViewById(R.id.txtOpenTicket);
        textView[4] = (TextView) findViewById(R.id.txtMyWallet);
        textView[5]  = (TextView) findViewById(R.id.txtHistory);
        textView[6] = (TextView) findViewById(R.id.txtRefer);
        textView[7] = (TextView) findViewById(R.id.txtPlans);
        textView[8] = (TextView) findViewById(R.id.txtFeedback);


        textView[0].setOnClickListener(this);
        textView[1].setOnClickListener(this);
        textView[2].setOnClickListener(this);
        textView[3].setOnClickListener(this);
        textView[4].setOnClickListener(this);
        textView[5].setOnClickListener(this);
        textView[6].setOnClickListener(this);
        textView[7].setOnClickListener(this);
        textView[8].setOnClickListener(this);
        textView[0].performClick();


        if (MakenMakePrefernece.getInstance().getLoggedIn()) {
            String name = MakenMakePrefernece.getInstance().getUserName();
            String OneWord = Character.toString(name.charAt(0));
            txtname.setText(name);
            txtfirstdigit.setText(OneWord);
            imgUserLogin.setImageResource(R.drawable.login_red);
        }else {
            txtname.setText("Hi User");
            txtfirstdigit.setText("M");
            imgUserLogin.setImageResource(R.drawable.logout_white);
        }

     }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        ((BaseActivity) this).showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {

            case ApiConstants.REQUEST_USER_INFO:
                String url = ApiConstants.URL_USER_INFO + MakenMakePrefernece.getInstance().getUserId();
                className = UserInfoResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        updateCartValue(getCartItemSize());
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            ((BaseActivity) this).removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_USER_INFO:
                    userInfoResponse = (UserInfoResponse) responseObject;

                    if (StringUtils.isNullOrEmpty(userInfoResponse.getDistrictname())) {
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.EXTRA_USERID, String.valueOf(userInfoResponse.getUserID()));
                        bundle.putString(AppConstants.EXTRA_NAME, userInfoResponse.getName());
                        bundle.putString(AppConstants.EXTRA_DOB, userInfoResponse.getDOB());
                        bundle.putString(AppConstants.EXTRA_EMAILID, userInfoResponse.getEmailid());
                        bundle.putString(AppConstants.EXTRA_GENDER, userInfoResponse.getGender());
                        bundle.putString(AppConstants.EXTRA_MNUMBER, userInfoResponse.getMNumber());
                        bundle.putString(AppConstants.EXTRA_MODIFIED, userInfoResponse.getModified());
                        bundle.putString(AppConstants.EXTRA_CREATED, userInfoResponse.getCreated());
                        bundle.putString(AppConstants.EXTRA_ALTERNATENUMBERS, userInfoResponse.getAlternateNumbers());
                        bundle.putString(AppConstants.EXTRA_FNAME, AddToCartActivity.class.getSimpleName());
                        Intent i = new Intent(HomeActivity.this,FirstTimeAddressActivity.class);
                        i.putExtras(bundle);
                        startActivity(i);

                    } else {
                        Intent add2cart = new Intent(this,AddToCartActivity.class);
                        startActivity(add2cart);
                    }

                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

  /*  private void changeCart(){

        if (getCartItemSize() != 0) {
            cartCount.setText(getCartItemSize() + "");
        } else {
            cartCount.setVisibility(View.GONE);
        }
    }*/



    private void setUpDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                ((Toolbar) findViewById(R.id.toolbar)), R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle("MakenMake");
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle("MakenMake");
                invalidateOptionsMenu();
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);

    }


    public void updateCartValue(int count) {
        ((TextView) findViewById(R.id.cartCount)).setText(count + "");
        if (count == 0) {
            findViewById(R.id.cartCount).setVisibility(View.GONE);
        } else {
            findViewById(R.id.cartCount).setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onClick(View view) {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
        switch (view.getId()) {
            case R.id.txtAddress:
                replaceFragment(null, new ShowLocalityFragment(), null, false);
                ChangeColor(0);
                break;
            case R.id.txtPlans:
                ChangeColor(7);
                replaceFragment(HomeFragment.class.getSimpleName(), new PlanFragment(), null, true);
                break;
            case R.id.txtHome:
                ChangeColor(1);
                replaceFragment(ShowLocalityFragment.class.getSimpleName(), new HomeFragment(), null, true);
                break;
            case R.id.txtMyAccount:
                ChangeColor(2);
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    replaceFragment(HomeFragment.class.getSimpleName(), new MyAccountFragment(), null, true);
                } else {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ContinueFragment(), null, true);
                }
                break;
            case R.id.txtMyWallet:
                ChangeColor(4);
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    replaceFragment(HomeFragment.class.getSimpleName(), new MyWalletFragment(), null, true);
                } else {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ContinueFragment(), null, true);
                }
                break;
            case R.id.txtOpenTicket:
                ChangeColor(3);
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    replaceFragment(HomeFragment.class.getSimpleName(), new OpenTicketFragment(), null, true);
                } else {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ContinueFragment(), null, true);
                }
                break;
            case R.id.txtHistory:
                ChangeColor(5);
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    replaceFragment(HomeFragment.class.getSimpleName(), new TicketStatusFragment(), null, true);
                } else {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ContinueFragment(), null, true);
                }
                break;
            case R.id.txtRefer:
                ChangeColor(6);
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ReferAndEarnFragment(), null, true);
                } else {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ContinueFragment(), null, true);
                }
                break;
            case R.id.txtFeedback:

                ChangeColor(8);
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    replaceFragment(HomeFragment.class.getSimpleName(), new FeedbackFragment(), null, true);
                }else {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ContinueFragment(), null, true);
                }
                    break;
            case R.id.imageCart:
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    hitApiRequest(ApiConstants.REQUEST_USER_INFO);
                } else {
                    replaceFragment(HomeFragment.class.getSimpleName(), new ContinueFragment(), null, true);

                }
                break;
            default:
                super.onClick(view);
        }

    }

    public void ChangeColor(int s) {

        for (int a = 0; a < textView.length; a++) {
            textView[a].setTextColor(getResources().getColor(R.color.colorSecondary));
            textView[a].setCompoundDrawablesWithIntrinsicBounds(imgInactive[a], 0, 0, 0);
            if (a == s) {
                textView[a].setTextColor(getResources().getColor(R.color.colorButton));
                textView[a].setCompoundDrawablesWithIntrinsicBounds(imgActive[a], 0, 0, 0);
            }
        }
    }

    public void onBackPressed() {
        if (MakenMakePrefernece.getInstance().getShowDilogorNot()) {
            this.showExitDialog();
        } else {
            replaceFragment(null, new HomeFragment(), null, false);
            ChangeColor(1);
        }
    }

    //show AlertDialog when device back button is Pressed
    private void showExitDialog() {
        AlertDialogUtils.showAlertDialogWithTwoButtons(this, "Service Manager", "Are you sure you want to exit ?","Ok","Cancel",new AlertDialogUtils.OnButtonClickListener() {
            @Override
            public void onButtonClick(int buttonId) {
                if (buttonId!=0){
                    moveTaskToBack(false);
                }
            }
        });
    }

}
