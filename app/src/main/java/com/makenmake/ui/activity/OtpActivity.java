package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.TroubleLoginResponse;

import wiredsoft.com.makenmake.R;

/**
 * Created by K.Agg on 18-09-2016.
 */
public class OtpActivity extends BaseActivity {

    private EditText entOtp;
    private String url;
    private String phoneNumber, userName;
    private EditText edtotp;
    Button submit;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);


        Bundle bundle   = getIntent().getExtras();
        if (bundle!=null){
            phoneNumber = bundle.getString(AppConstants.EXTRA_PHONE_NO);
            userName = bundle.getString(AppConstants.EXTRA_USERNAME);
        }

        edtotp = (EditText) findViewById(R.id.edtotp);
        submit = (Button) findViewById(R.id.btnSubmit);
        findViewById(R.id.btnSubmit).setOnClickListener(this);
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(edtotp.getText().toString())) {
            ToastUtils.showToast(this, "Please enter otp.");
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmit:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_VERIFY_OTP);
                }
                break;

            default:
                super.onClick(view);
        }
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_VERIFY_OTP:
                url = ApiConstants.URL_VERIFY_OTP + phoneNumber + "&otp=" + edtotp.getText().toString();
                className = TroubleLoginResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_VERIFY_OTP:
                    TroubleLoginResponse troubleLoginResponse = (TroubleLoginResponse) responseObject;
                    if (troubleLoginResponse.getStatus().equalsIgnoreCase("success")) {
                        AlertDialogUtils.showAlertDialog(this, "Service Manager", "Your login credentials are in your message inbox :)", new AlertDialogUtils.OnButtonClickListener() {
                            @Override
                            public void onButtonClick(int buttonId) {
                                Intent intent = new Intent(OtpActivity.this, HomeActivity.class);
                                startActivity(intent);
                            }
                        });

                    } else {
                        ToastUtils.showToast(this, "Wrong OTP");
                    }
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
