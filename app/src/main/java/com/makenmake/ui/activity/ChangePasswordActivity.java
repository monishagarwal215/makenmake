package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.HashMap;

import wiredsoft.com.makenmake.R;

/**
 * Created by monish on 10/09/16.
 */
public class ChangePasswordActivity extends BaseActivity {
    private EditText oldpassword, newpassword, newcnfpassword;
    private TextView txtsave;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        oldpassword = (EditText) findViewById(R.id.oldpassword);
        newpassword = (EditText) findViewById(R.id.newpassword);
        newcnfpassword = (EditText)findViewById(R.id.newcnfpassword);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);
        txtsave = (TextView) findViewById(R.id.txtSave);
        txtsave.setOnClickListener(this);
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_CHANGE_PASSWORD:
                String url = ApiConstants.URL_CHANGE_PASSWORD;
                className = String.class;
                request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
                }, getHashParams(reqType));
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    private HashMap<String, String> getHashParams(int reqType) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (reqType == ApiConstants.REQUEST_CHANGE_PASSWORD) {
            hashMap.put(ApiConstants.PARAMS.USER_ID, MakenMakePrefernece.getInstance().getUserId());
            hashMap.put(ApiConstants.PARAMS.OLDPASS, StringUtils.getBase64String(oldpassword.getText().toString()));
            hashMap.put(ApiConstants.PARAMS.NEWPASS, StringUtils.getBase64String(newpassword.getText().toString()));
        }
        return hashMap;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_CHANGE_PASSWORD:
                    String changePasswordResponse = (String) responseObject;
                    if (changePasswordResponse.equals("1")) {
                        //((BaseActivity) getActivity()).replaceFragment(null, new HomeFragment(), null, false);

                        MakenMakePrefernece.getInstance().setLoggedIn(false);
                        Intent i = new Intent(this, HomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                    else{
                        ToastUtils.showToast(this,"Please enter correct Old password");
                    }

                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.txtSave:
                if (validateData()) {
                    hitApiRequest(ApiConstants.REQUEST_CHANGE_PASSWORD);
                }
                break;
            default:
                super.onClick(view);
        }
    }

    private boolean validateData() {
        if (StringUtils.isNullOrEmpty(oldpassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter old password.");
            return false;
        } else if (StringUtils.isNullOrEmpty(newpassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter new password.");
            return false;
        } else if (StringUtils.isNullOrEmpty(newcnfpassword.getText().toString())) {
            ToastUtils.showToast(this, "Please enter confirm password.");
            return false;
        } else if (!(newpassword.getText().toString().equals(newcnfpassword.getText().toString()))) {
            ToastUtils.showToast(this, "Password does not match.");
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
