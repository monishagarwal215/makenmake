package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.AddToCartModel;
import com.makenmake.model.response.LevelFourResponse;
import com.makenmake.model.response.LevelThreeResponse;
import com.makenmake.model.response.LevelTwoResponse;
import com.makenmake.model.response.UserInfoResponse;
import com.makenmake.utils.ActivityControl;
import com.makenmake.utils.MakenMakePrefernece;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import wiredsoft.com.makenmake.R;


/**
 * Created by monish on 10/09/16.
 */
public class CategoryDetailActivity extends BaseActivity {


    private String url;
    private ImageView cart;

    private LayoutInflater mInflater;
    private String categoryId;
    private int category3;
    private int category2;
    private int clickedPos;
    private LinearLayout lnrCategorySerice;
    private AddToCartModel addToCartModel;
    private LevelTwoResponse levelTwoResponse;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mInflater = LayoutInflater.from(getApplicationContext());
        setContentView(R.layout.activity_category_detail);

        ActivityControl.categoryDetailActivity = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cart = (ImageView) findViewById(R.id.imageCart);

        cart.setOnClickListener(this);
        addToCartModel = new AddToCartModel();

        TextView cartCount = (TextView) findViewById(R.id.cartCount);
        if (getCartItemSize() != 0) {
            cartCount.setText(getCartItemSize() + "");
        } else {
            cartCount.setVisibility(View.GONE);
        }

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            categoryId = bundle.getString(AppConstants.BUNDLE_CATEGORY_ID);
        }


        hitApiRequest(ApiConstants.REQUEST_GET_MENU_LEVEL_2);
        findViewById(R.id.txtNext).setOnClickListener(this);

    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getApplicationContext())) {
            ToastUtils.showToast(getApplicationContext(), "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_MENU_LEVEL_2:
                url = ApiConstants.URL_GET_MENU_LEVEL_2 + categoryId;
                className = LevelTwoResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_GET_MENU_LEVEL_3:
                url = ApiConstants.URL_GET_MENU_LEVEL_3 + levelTwoResponse.getAddServicesubcategoryId();
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_GET_MENU_LEVEL_4:
                url = String.format(ApiConstants.URL_GET_MENU_LEVEL_4, categoryId, category2, category3);
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_USER_INFO:
                String url = ApiConstants.URL_USER_INFO + MakenMakePrefernece.getInstance().getUserId();
                className = UserInfoResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();
        if (reqType == ApiConstants.REQUEST_GET_MENU_LEVEL_2) {
        }
        return params;
    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getApplicationContext(), "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_GET_MENU_LEVEL_2:
                    levelTwoResponse = (LevelTwoResponse) responseObject;
                    updateUiOfLevelTwo(levelTwoResponse);
                    hitApiRequest(ApiConstants.REQUEST_GET_MENU_LEVEL_3);

                    break;
                case ApiConstants.REQUEST_GET_MENU_LEVEL_3:
                    Type listType = new TypeToken<List<LevelThreeResponse>>() {
                    }.getType();
                    List<LevelThreeResponse> levelThreeResponseList = new Gson().fromJson(new Gson().toJson(responseObject), listType);
                    updateUiOfLevelThree(levelThreeResponseList);
                    break;
                case ApiConstants.REQUEST_GET_MENU_LEVEL_4:
                    Type listType1 = new TypeToken<List<LevelFourResponse>>() {
                    }.getType();
                    List<LevelFourResponse> levelFourResponseList = new Gson().fromJson(new Gson().toJson(responseObject), listType1);
                    updateUiOfLevelFour(levelFourResponseList);
                    break;
                case ApiConstants.REQUEST_USER_INFO:
                    UserInfoResponse userInfoResponse = (UserInfoResponse) responseObject;

                    if (StringUtils.isNullOrEmpty(userInfoResponse.getDistrictname())) {
                        Bundle bundle = new Bundle();
                        bundle.putString(AppConstants.EXTRA_USERID, String.valueOf(userInfoResponse.getUserID()));
                        bundle.putString(AppConstants.EXTRA_NAME, userInfoResponse.getName());
                        bundle.putString(AppConstants.EXTRA_DOB, userInfoResponse.getDOB());
                        bundle.putString(AppConstants.EXTRA_EMAILID, userInfoResponse.getEmailid());
                        bundle.putString(AppConstants.EXTRA_GENDER, userInfoResponse.getGender());
                        bundle.putString(AppConstants.EXTRA_MNUMBER, userInfoResponse.getMNumber());
                        bundle.putString(AppConstants.EXTRA_MODIFIED, userInfoResponse.getModified());
                        bundle.putString(AppConstants.EXTRA_CREATED, userInfoResponse.getCreated());
                        bundle.putString(AppConstants.EXTRA_ALTERNATENUMBERS, userInfoResponse.getAlternateNumbers());
                        bundle.putString(AppConstants.EXTRA_FNAME, AddToCartActivity.class.getSimpleName());
                        Intent i = new Intent(this, FirstTimeAddressActivity.class);
                        i.putExtras(bundle);
                        startActivity(i);
                    } else {
                        Intent in = new Intent(this, AddToCartActivity.class);
                        startActivity(in);

                    }


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateUiOfLevelFour(final List<LevelFourResponse> levelFourResponseList) {
        final RelativeLayout relativeLayout = (RelativeLayout) lnrCategorySerice.getChildAt(clickedPos);
        final LinearLayout lntCategory4 = (LinearLayout) relativeLayout.findViewById(R.id.lntCategory4);

        for (LevelFourResponse temp : levelFourResponseList) {
            final View rowView = mInflater.inflate(R.layout.layout_level_four, null);
            ((TextView) rowView.findViewById(R.id.txtType)).setText(temp.getTbladdServiceRate().getServiceRateName());
            ((TextView) rowView.findViewById(R.id.txtAmount)).setText(temp.getTblServicePlan().getAmount() + "");
            lntCategory4.addView(rowView);

            rowView.setTag(temp);
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LevelFourResponse temp1 = (LevelFourResponse) v.getTag();

                    addToCartModel.setPlanId(temp1.getTblServicePlan().getServicePlanID());
                    addToCartModel.setServiceId(temp1.getTblServicePlan().getServiceID());
                    addToCartModel.setAmount(temp1.getTblServicePlan().getAmount());
                    addToCartModel.setNoOfService(temp1.getTblServicePlan().getNofCalls());
                    addToCartModel.setType(temp1.getTblServicePlan().getPlanType());

                    unSelectAllLevelFour();

                    ((ImageView) v.findViewById(R.id.imgCircle)).setImageResource(R.drawable.purple_circle_dots);
                    ((ImageView) ((RelativeLayout) v.getParent().getParent()).findViewById(R.id.imgCircle)).setImageResource(R.drawable.purple_circle_dots);

                    //((ImageView) relativeLayout.findViewById(R.id.imgCircle)).setImageResource(R.drawable.purple_circle_dots);
                    //((ImageView) lntCategory4.getChildAt(levelFourResponseList.indexOf(temp1)).findViewById(R.id.imgCircle)).setImageResource(R.drawable.purple_circle_dots);
                    /*for (int i = 0; i <= lntCategory4.getChildCount() - 1; i++) {
                        if (i == levelFourResponseList.indexOf(temp1)) {
                            ((ImageView) lntCategory4.getChildAt(i).findViewById(R.id.imgCircle)).setImageResource(R.drawable.purple_circle_dots);
                        } else {
                            ((ImageView) lntCategory4.getChildAt(i).findViewById(R.id.imgCircle)).setImageResource(R.drawable.white_circle_dots);
                        }
                    }*/
                }
            });
        }


    }

    private void unSelectAllLevelFour() {
        lnrCategorySerice = (LinearLayout) findViewById(R.id.lnrCategorySerice);

        for (int i = 0; i <= lnrCategorySerice.getChildCount() - 1; i++) {
            RelativeLayout rtlCategoryFour = (RelativeLayout) lnrCategorySerice.getChildAt(i);
            ((ImageView) rtlCategoryFour.findViewById(R.id.imgCircle)).setImageResource(R.drawable.white_circle_dots);

            LinearLayout lntCategory4 = (LinearLayout) rtlCategoryFour.findViewById(R.id.lntCategory4);
            for (int j = 0; j <= lntCategory4.getChildCount() - 1; j++) {
                ((ImageView) ((RelativeLayout) lntCategory4.getChildAt(j)).findViewById(R.id.imgCircle)).setImageResource(R.drawable.white_circle_dots);
                //((ImageView) lntCategory4.findViewById(R.id.imgCircle)).setImageResource(R.drawable.white_circle_dots);
            }
        }
    }

    private void updateUiOfLevelThree(final List<LevelThreeResponse> levelThreeResponseList) {
        lnrCategorySerice = (LinearLayout) findViewById(R.id.lnrCategorySerice);

        for (LevelThreeResponse temp : levelThreeResponseList) {
            final View rowView = mInflater.inflate(R.layout.layout_level_three, null);
            ((TextView) rowView.findViewById(R.id.txtLeveThree)).setText(temp.getServiceForName());
            lnrCategorySerice.addView(rowView);

            rowView.setTag(temp);
            rowView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LevelThreeResponse temp1 = (LevelThreeResponse) v.getTag();
                    category3 = temp1.getAddServiceForId();
                    category2 = temp1.getSubcategoryId();
                    clickedPos = levelThreeResponseList.indexOf(temp1);
                    if (((LinearLayout) rowView.findViewById(R.id.lntCategory4)).getVisibility() == View.VISIBLE) {
                        ((LinearLayout) rowView.findViewById(R.id.lntCategory4)).setVisibility(View.GONE);
                        ((ImageView) rowView.findViewById(R.id.imgCircle)).setImageResource(R.drawable.white_circle_dots);
                    } else {
                        ((LinearLayout) rowView.findViewById(R.id.lntCategory4)).setVisibility(View.VISIBLE);
                        ((ImageView) rowView.findViewById(R.id.imgCircle)).setImageResource(R.drawable.purple_circle_dots);
                    }

                    if (((LinearLayout) rowView.findViewById(R.id.lntCategory4)).getChildCount() == 0) {
                        hitApiRequest(ApiConstants.REQUEST_GET_MENU_LEVEL_4);
                        ((LinearLayout) rowView.findViewById(R.id.lntCategory4)).setVisibility(View.VISIBLE);
                        ((ImageView) rowView.findViewById(R.id.imgCircle)).setImageResource(R.drawable.purple_circle_dots);
                    }
                }
            });
        }

    }

    private void updateUiOfLevelTwo(LevelTwoResponse levelTwoResponse) {
        addToCartModel.setServiceContent(levelTwoResponse.getContent());
        addToCartModel.setServiceName(levelTwoResponse.getTitle());
        loadImage(ApiConstants.URL_IMAGE_BASE + levelTwoResponse.getImageName(), (ImageView) findViewById(R.id.imgCategory), R.drawable.category_img);
        ((TextView) findViewById(R.id.txtCategoryName)).setText(levelTwoResponse.getTitle());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtNext:
                if (addToCartModel != null && addToCartModel.getServiceId() != 0) {
                    Intent i = new Intent(this, SetDateTimeActivity.class);
                    i.putExtra(AppConstants.EXTRA_CART, addToCartModel);
                    startActivity(i);
                } else {
                    Toast.makeText(this, "Please select your requirement", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.imageCart:
                if (MakenMakePrefernece.getInstance().getLoggedIn()) {
                    hitApiRequest(ApiConstants.REQUEST_USER_INFO);
                } else {
                AlertDialogUtils.showAlertDialogWithTwoButtons(this, "Service Manager", "Please login to proceed !", "Ok", "Cancel", new AlertDialogUtils.OnButtonClickListener() {
                    @Override
                    public void onButtonClick(int buttonId) {
                        switch (buttonId) {
                            case 1:
                                Intent i = new Intent(CategoryDetailActivity.this, LoginActivity.class);
                                startActivity(i);
                                break;
                        }
                    }
                });
            }
            default:
                super.onClick(view);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityControl.categoryDetailActivity = null;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
