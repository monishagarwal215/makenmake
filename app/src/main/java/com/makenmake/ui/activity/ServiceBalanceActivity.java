package com.makenmake.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.ServiceBalanceResponse;
import com.makenmake.utils.MakenMakePrefernece;

import java.lang.reflect.Type;
import java.util.List;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 10/09/16.
 */
public class ServiceBalanceActivity extends BaseActivity {
    private TextView txtagreemnetid, txtagreementnum, txtexpirationdate, txtinvoicedate, txtplanamount, txtplanstatus;
    private String url;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_balance);

        txtagreemnetid = (TextView) findViewById(R.id.txtagreemnetid);
        txtagreementnum = (TextView) findViewById(R.id.txtagreementnum);
        txtexpirationdate = (TextView) findViewById(R.id.txtexpirationdate);
        txtinvoicedate = (TextView) findViewById(R.id.txtinvoicedate);
        txtplanamount = (TextView) findViewById(R.id.txtplanamount);
        txtplanstatus = (TextView) findViewById(R.id.txtplanstatus);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);

        hitApiRequest(ApiConstants.REQUEST_SERVICE_BALANCE);
    }

    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {

            case ApiConstants.REQUEST_SERVICE_BALANCE:
                url = ApiConstants.SERVICE_BALANCE + MakenMakePrefernece.getInstance().getUserId();
                className = List.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_SERVICE_BALANCE:
                    Type listType = new TypeToken<List<ServiceBalanceResponse>>() {
                    }.getType();
                    List<ServiceBalanceResponse> serviceBalanceResponse = new Gson().fromJson(new Gson().toJson(responseObject), listType);


                    updateServiceBalanceUI(serviceBalanceResponse);

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void updateServiceBalanceUI(List<ServiceBalanceResponse> serviceBalanceResponse) {
        if (serviceBalanceResponse != null && serviceBalanceResponse.size() > 0) {
            txtagreemnetid.setText(serviceBalanceResponse.get(0).getAgreementID());
            txtagreementnum.setText(serviceBalanceResponse.get(0).getAgreementNumber());
            txtexpirationdate.setText(serviceBalanceResponse.get(0).getExpirationdate());
            txtinvoicedate.setText(serviceBalanceResponse.get(0).getInvoicedate());
            txtplanamount.setText(serviceBalanceResponse.get(0).getPlanAmount());
            txtplanstatus.setText(serviceBalanceResponse.get(0).getPlanStatus());

        }
        else
        {
            AlertDialogUtils.showAlertDialog(this, "Service Manager", "Oops ! \n" + "You have not purchased any services yet.", new AlertDialogUtils.OnButtonClickListener() {
                @Override
                public void onButtonClick(int buttonId) {
                    getSupportFragmentManager().popBackStack();
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
