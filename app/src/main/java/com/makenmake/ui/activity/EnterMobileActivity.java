package com.makenmake.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.ui.BaseActivity;
import com.krapps.utils.AlertDialogUtils;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.constants.AppConstants;
import com.makenmake.model.response.ChangePasswordResponse;

import wiredsoft.com.makenmake.R;

/**
 * Created by kunal on 9/10/2016.
 */
public class EnterMobileActivity extends BaseActivity {
    private EditText entMobileNo;
    private String url;
    private boolean fromTrouble;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_mobile);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromTrouble = bundle.getBoolean(AppConstants.FROM_TROUBLE);
        }

        if (fromTrouble) {
            ((TextView) findViewById(R.id.txtHeader)).setText(getString(R.string.trouble_login));
        } else {
            ((TextView) findViewById(R.id.txtHeader)).setText(getString(R.string.forgot_password));
        }

        entMobileNo = (EditText) findViewById(R.id.entMobileNo);

        findViewById(R.id.txtSubmit).setOnClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar == null) {
            return;
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.img_back);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ((TextView) findViewById(R.id.cartCount)).setVisibility(View.GONE);
        ((ImageView) findViewById(R.id.imageCart)).setVisibility(View.GONE);

    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, "Device is out of network");
            return;
        }
        showProgressDialog();
        VolleyStringRequest request;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_FORGOT_PASSWORD:
                url = ApiConstants.URL_FORGOT_PASSWORD + entMobileNo.getText().toString();
                className = ChangePasswordResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

                break;
            case ApiConstants.REQUEST_TROUBLE_LOGIN:
                url = ApiConstants.URL_TROUBLE_LOGIN + entMobileNo.getText().toString();
                className = String.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);

                break;
            default:
                url = "";
                className = null;
                break;
        }
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this, "Some error occured.");
                return;
            }
            removeProgressDialog();
            switch (reqType) {
                case ApiConstants.REQUEST_FORGOT_PASSWORD:
                    ChangePasswordResponse changePasswordResponse = (ChangePasswordResponse) responseObject;
                    if (changePasswordResponse.getStatus() > 0) {
                        Intent fpOTP  = new Intent(this,ForgotPasswordOtpActivity.class);
                        fpOTP.putExtra(AppConstants.EXTRA_PHONE_NO, entMobileNo.getText().toString());
                        fpOTP.putExtra(AppConstants.FROM_TROUBLE, false);
                        startActivity(fpOTP);
                    } else {
                        ToastUtils.showToast(this, "Wrong number");
                    }
                case ApiConstants.REQUEST_TROUBLE_LOGIN:
                    String response = (String) responseObject;
                    if (!StringUtils.isNullOrEmpty(response) && TextUtils.isDigitsOnly(response)) {
                        int res = Integer.parseInt(response);
                        if (res == 1) {
                            Intent OTP  = new Intent(this,OtpActivity.class);
                            OTP.putExtra(AppConstants.EXTRA_PHONE_NO, entMobileNo.getText().toString());
                            OTP.putExtra(AppConstants.FROM_TROUBLE, true);
                            startActivity(OTP);
                        } else {
                            AlertDialogUtils.showAlertDialog(this, "Service Manager", "Invalid Number", new AlertDialogUtils.OnButtonClickListener() {
                                @Override
                                public void onButtonClick(int buttonId) {

                                }
                            });
                        }
                    }
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.txtSubmit:
                if (validateData()) {
                    if (fromTrouble) {
                        hitApiRequest(ApiConstants.REQUEST_TROUBLE_LOGIN);
                    } else {
                        hitApiRequest(ApiConstants.REQUEST_FORGOT_PASSWORD);
                    }
                }
                break;
            default:
                super.onClick(view);
        }

    }

    private boolean validateData() {
        if (StringUtils.isValidMobileNumber(entMobileNo.getText().toString(), false, 11)) {
            Toast.makeText(this, "Enter Mobile Number", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
