package com.makenmake.ui.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Toast;

import com.krapps.ui.BaseActivity;

import wiredsoft.com.makenmake.R;

/**
 * Created by saurabh vardani on 14-09-2016.
 */
public class ReferAndEarnActivity extends BaseActivity {
    private String referCode;
    private String referMessage;

    @Nullable
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refer_earn);

        referCode = getIntent().getExtras().getString("ReferCode");
        referMessage = "Hi your friend is enjoying Homecare services from Make n Make. You can also enjoy and get benefits by installing the APP at https://play.google.com/store/apps/details?id=foo.bar.MNM Use Refer code "+referCode+" while signup and your friend will get reward 50 in their MnM Wallet :). Do not break the chain to get rewards in your mnm wallet. Thanks.";
        findViewById(R.id.mess).setOnClickListener(this);
        findViewById(R.id.email).setOnClickListener(this);
        findViewById(R.id.fb).setOnClickListener(this);
        findViewById(R.id.wapp).setOnClickListener(this);
        findViewById(R.id.can).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.mess:
                Intent smsIntent = new Intent(Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", "");
                smsIntent.putExtra("sms_body", referMessage);
                startActivity(smsIntent);
                break;
            case R.id.email:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Refer and earn");
                emailIntent.putExtra(Intent.EXTRA_TEXT, referMessage);
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;
            case R.id.fb:
                Toast.makeText(this, "Coming soon...", Toast.LENGTH_LONG).show();
                break;
            case R.id.wapp:
                PackageManager pm = this.getPackageManager();
                try {
                    Intent waIntent = new Intent(Intent.ACTION_SEND);
                    waIntent.setType("text/plain");
                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
                    //Check if package exists or not. If not then code
                    //in catch block will be called
                    waIntent.setPackage("com.whatsapp");
                    waIntent.putExtra(Intent.EXTRA_TEXT, referMessage);
                    startActivity(Intent.createChooser(waIntent, "Share with"));
                } catch (PackageManager.NameNotFoundException e) {
                    Toast.makeText(this, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.can:
finish();
                break;
            default:
                super.onClick(view);
        }
    }

}
