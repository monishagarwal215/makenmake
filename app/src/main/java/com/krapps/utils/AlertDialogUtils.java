package com.krapps.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import wiredsoft.com.makenmake.R;

public class AlertDialogUtils {

	public static final int	BUTTON_CLICK_FAILURE	= 0;
	public static final int	BUTTON_CLICK_SUCCESS	= 1;

	public static void showAlertDialogWithTwoButtons(Context context, String headerMessage, String messageInfo, String btnPositiveText, String btnNegativeText, final OnButtonClickListener onButtonClick) {
		new AlertDialog.Builder(context).setTitle(headerMessage).setMessage(messageInfo).setPositiveButton(btnPositiveText, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onButtonClick.onButtonClick(BUTTON_CLICK_SUCCESS);
			}
		}).setNegativeButton(btnNegativeText, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				onButtonClick.onButtonClick(BUTTON_CLICK_FAILURE);
			}
		}).setIcon(R.drawable.logo).show();
	}

	public static void showAlertDialog(Context context, String headerMessage, String messageInfo, final OnButtonClickListener onButtonClick) {
		new AlertDialog.Builder(context).setTitle(headerMessage).setMessage(messageInfo).setNeutralButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				onButtonClick.onButtonClick(BUTTON_CLICK_SUCCESS);
			}
		}).setIcon(R.drawable.logo).show();
	}

	public interface OnButtonClickListener {

		void onButtonClick(int buttonId);

	}

}
