package com.krapps.listener;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.krapps.model.CommonJsonResponse;
import com.krapps.network.VolleyExceptionUtil;
import com.krapps.network.VolleyStringRequest;

import org.json.JSONObject;

import wiredsoft.com.makenmake.BuildConfig;

/**
 * @author kapil.vij
 */
public class UpdateJsonListener<T> implements ErrorListener, Listener<JSONObject> {

    private int reqType;
    private onUpdateViewListener onUpdateViewListener;
    private Activity mActivity;
    private Class<T> classObject;

    public interface onUpdateViewListener {
        public void updateView(Object responseString, boolean isSuccess, int reqType);
    }

    public UpdateJsonListener(Activity activity, onUpdateViewListener onUpdateView, int reqType, Class<T> classObject) {
        this.reqType = reqType;
        this.onUpdateViewListener = onUpdateView;
        mActivity = activity;
        this.classObject = classObject;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        onUpdateViewListener.updateView(VolleyExceptionUtil.getErrorMessage(error), false, reqType);
    }

    @Override
    public void onResponse(JSONObject jsonResponse) {
        final String responseStr = jsonResponse.toString();
        if (BuildConfig.DEBUG) {
            Log.i(VolleyStringRequest.mNetworkTag, responseStr);
        }
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Object responseObject = new Gson().fromJson(responseStr, classObject);
                    mActivity.runOnUiThread(new Runnable() {
                        @SuppressLint("InlinedApi")
                        @Override
                        public void run() {
                            if (responseObject instanceof CommonJsonResponse) {
                                CommonJsonResponse responseModel = (CommonJsonResponse) responseObject;
                            }
                            onUpdateViewListener.updateView(responseObject, true, reqType);
                        }
                    });
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
            onUpdateViewListener.updateView(responseStr, false, reqType);
        }
    }

}
