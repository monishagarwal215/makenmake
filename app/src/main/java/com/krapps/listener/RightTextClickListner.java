package com.krapps.listener;

public interface RightTextClickListner {
	public void onShareButtonClick();
	public void onDoneButtonClick();
}
