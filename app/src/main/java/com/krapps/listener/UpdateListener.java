package com.krapps.listener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.krapps.model.CommonJsonResponse;
import com.krapps.network.VolleyExceptionUtil;
import com.krapps.network.VolleyStringRequest;
import com.makenmake.model.response.MenuBarResponse;

import java.util.ArrayList;
import java.util.List;

import wiredsoft.com.makenmake.BuildConfig;

public class UpdateListener<T> implements Listener<String>, ErrorListener {
    private int reqType;
    private onUpdateViewListener onUpdateViewListener;
    private Activity mActivity;
    private Class<T> classObject;

    public interface onUpdateViewListener {
        public void updateView(Object responseObject, boolean isSuccess, int reqType);
    }

    public UpdateListener(Activity activity, onUpdateViewListener onUpdateView, int reqType, Class<T> classObject) {
        this.reqType = reqType;
        this.onUpdateViewListener = onUpdateView;
        mActivity = activity;
        this.classObject = classObject;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        String message = null;
        if (error instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (error instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (error instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (error instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (error instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (error instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        }
        onUpdateViewListener.updateView(message, false, reqType);
    }

    @Override
    public void onResponse(final String responseStr) {
        if (BuildConfig.DEBUG) {
            Log.i(VolleyStringRequest.mNetworkTag, responseStr);
        }
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Object responseObject = new Gson().fromJson(responseStr, classObject);
                    mActivity.runOnUiThread(new Runnable() {
                        @SuppressLint("InlinedApi")
                        @Override
                        public void run() {
                            if (responseObject instanceof CommonJsonResponse) {
                                CommonJsonResponse responseModel = (CommonJsonResponse) responseObject;
                            }
                            onUpdateViewListener.updateView(responseObject, true, reqType);
                        }
                    });
                }
            }).start();
        } catch (Exception ex) {
            ex.printStackTrace();
            onUpdateViewListener.updateView(responseStr, false, reqType);
        }
    }

}
