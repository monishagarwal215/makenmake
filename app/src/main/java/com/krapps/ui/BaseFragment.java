package com.krapps.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.listener.UpdateListener.onUpdateViewListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.AgreementDetailResponse;
import com.makenmake.model.response.AgreementDetail;
import com.makenmake.model.response.ServicePurchasedByClient;
import com.makenmake.model.response.UserAgreement;
import com.makenmake.model.response.AddToCartModel;
import com.makenmake.ui.activity.HomeActivity;
import com.makenmake.utils.MakenMakePrefernece;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import wiredsoft.com.makenmake.R;

public abstract class BaseFragment extends Fragment implements OnClickListener, onUpdateViewListener {
    public static String HOME_FRAGMENT = "frg_home";
    private String frgamentName;
    public BaseActivity mActivity;
    private float density;
    private String agreement_id;

    public BaseFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        density = mActivity.getResources().getDisplayMetrics().density;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHeader();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            setHeader();
        }
    }

    public abstract void setHeader();

    /*public void replaceChildFragment(Fragment fragment, Bundle bundle) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        Fragment fragmentLocal = getChildFragmentManager().findFragmentById(
                R.id.innerFrame);
        if (fragmentLocal != null
                && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        ft.replace(R.id.innerFrame, fragment, tag);

        // fragment.setRetainInstance(true);
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        hideKeyboard();
        ((BaseActivity) mActivity).removeProgressDialog();
    }

    public HashMap<String, String> getBaseParams() {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }

    public HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }

    public float getDensity() {
        return density;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = (BaseActivity) activity;
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = mActivity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) mActivity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void setFragmentName(String frgamentName) {
        this.frgamentName = frgamentName;
    }

    public String getFragmentName() {
        return frgamentName;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onStop() {
        super.onStop();
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(getActivity())) {
            ToastUtils.showToast(getActivity(),
                    getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_SERVICE_PURCHASED:
                url = ApiConstants.URL_GET_SERVICE_PURCHASED + MakenMakePrefernece.getInstance().getUserId();
                className = ServicePurchasedByClient.class;
                VolleyStringRequest request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_AGREEMENT_DETAILS:
                url = ApiConstants.URL_AGREEMENT_DETAILS + agreement_id;
                className = AgreementDetailResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(getActivity(), this, reqType, className) {
                });
                ((BaseApplication) getActivity().getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;


            default:
                url = "";
                className = null;
                break;
        }
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = getBaseParams();


        return params;
    }

    /**
     * Override this method when refresh page is required in your fragment
     *
     * @param bundle
     */
    protected void refreshFragment(Bundle bundle) {

    }

    public void updateView(Object object) {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            default:
                break;
        }

        Log.d("click", BaseFragment.class.getSimpleName().toString());

    }

    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(getActivity(),
                        getString(R.string.some_error_occured));
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_SERVICE_PURCHASED:
                    ServicePurchasedByClient servicepurchaedbyclient = (ServicePurchasedByClient) responseObject;
                    if (servicepurchaedbyclient.isAnyAgreement()) {
                        setUI(servicepurchaedbyclient.getUserAgreement());
                    } else {//TODO
                    }
                    break;
                case ApiConstants.REQUEST_AGREEMENT_DETAILS:
                    AgreementDetailResponse agreementdetails = (AgreementDetailResponse) responseObject;
                    if (agreementdetails.isAnyAgreement()) {
                        setUI2(agreementdetails.getAgreementDetail());
                    } else {
                    }

                    break;

                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setUI(List<UserAgreement> userAgreement) {
        if (userAgreement != null && userAgreement.size() > 0) {
            agreement_id = userAgreement.get(0).getAgreementID() + "";
            hitApiRequest(ApiConstants.REQUEST_AGREEMENT_DETAILS);
        }
    }

    private void setUI2(List<AgreementDetail> agreementDetails) {
        MakenMakePrefernece.getInstance().createPlanDetails(agreementDetails.get(0).getType(), agreementDetails.get(0).getServicePlan(), agreementDetails.get(0).getServiceName(), agreement_id, agreementDetails.get(0).getCategory());
    }

    public void addItemToCart(AddToCartModel addToCartModel) {
        List<AddToCartModel> addToCartModelList = MakenMakePrefernece.getInstance().getCart();
        if (addToCartModelList == null) {
            addToCartModelList = new ArrayList<>();
        }
        if (!addToCartModelList.contains(addToCartModel)) {
            addToCartModelList.add(addToCartModel);
        } else {
            ToastUtils.showToast(getActivity(), "Item hase already in the cart");
        }
        ((HomeActivity) getActivity()).updateCartValue(addToCartModelList.size());
        MakenMakePrefernece.getInstance().setCart(addToCartModelList);
    }

    public void removeAllItemToCart(List<AddToCartModel> addToCartModelslist) {
        ((HomeActivity) getActivity()).updateCartValue(addToCartModelslist.size());
        MakenMakePrefernece.getInstance().setCart(addToCartModelslist);
    }

    public void removeItemToCart(AddToCartModel addToCartModel) {
        List<AddToCartModel> addToCartModelList = MakenMakePrefernece.getInstance().getCart();
        if (addToCartModelList == null) {
            addToCartModelList = new ArrayList<>();
        }
        if (addToCartModelList.contains(addToCartModel)) {
            addToCartModelList.remove(addToCartModel);
        }
        ((HomeActivity) getActivity()).updateCartValue(addToCartModelList.size());
        MakenMakePrefernece.getInstance().setCart(addToCartModelList);
    }
}
