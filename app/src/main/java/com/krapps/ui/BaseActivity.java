package com.krapps.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.krapps.application.BaseApplication;
import com.krapps.listener.UpdateListener;
import com.krapps.listener.UpdateListener.onUpdateViewListener;
import com.krapps.network.VolleyStringRequest;
import com.krapps.picasso.BitmapBorderTransformation;
import com.krapps.picasso.BlurTransform;
import com.krapps.picasso.RoundedTransformation;
import com.krapps.utils.ConnectivityUtils;
import com.krapps.utils.StringUtils;
import com.krapps.utils.ToastUtils;
import com.makenmake.constants.ApiConstants;
import com.makenmake.model.response.AddToCartModel;
import com.makenmake.model.response.AgreementDetail;
import com.makenmake.model.response.AgreementDetailResponse;
import com.makenmake.model.response.ServicePurchasedByClient;
import com.makenmake.model.response.UserAgreement;
import com.makenmake.ui.activity.HomeActivity;
import com.makenmake.utils.MakenMakePrefernece;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

import wiredsoft.com.makenmake.R;

//import com.bugsense.trace.BugSenseHandler;

public class BaseActivity extends AppCompatActivity implements onUpdateViewListener, OnClickListener {

    private static AlertDialog alertDialog;
    private BaseApplication mApplication;
    private long delayTimeForExitToast = 2000;
    private long backPressedTime;
    private ProgressDialog mProgressDialog;
    private String mDeviceId;

    private boolean isWindowFocused;
    private boolean isAppWentToBg;
    private boolean isBackPressed;
    private static int runningActivities = 0;
    private int imgSize;
    private String agreement_id;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        Fabric.with(this, new Crashlytics());
        mApplication = (BaseApplication) getApplication();

    }

    public void clearAllFragment() {
        FragmentManager fm = this.getSupportFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public void replaceFragment(String currentFragmentTag, Fragment fragment, Bundle bundle, boolean isAddToBackStack) {
        findViewById(R.id.content_frame).setVisibility(View.VISIBLE);
        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragmentLocal = getSupportFragmentManager().findFragmentById(R.id.content_frame);
        if (fragmentLocal != null && fragmentLocal.getTag().equalsIgnoreCase(tag)) {
            ((BaseFragment) fragmentLocal).refreshFragment(bundle);
            return;
        }
        if (!StringUtils.isNullOrEmpty(currentFragmentTag)) {
            ft.add(R.id.content_frame, fragment, tag);
            Fragment fragmentToHide = getSupportFragmentManager().findFragmentByTag(currentFragmentTag);
            if (fragmentToHide != null) {
                ft.hide(fragmentToHide);
            }
        } else {
            ft.replace(R.id.content_frame, fragment, tag);
        }

        fragment.setRetainInstance(true);
        if (isAddToBackStack) {
            ft.addToBackStack(tag);
        }
        try {
            ft.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            ft.commitAllowingStateLoss();
        }
    }

    private void setUI(List<UserAgreement> userAgreement) {
        if (userAgreement != null && userAgreement.size() > 0) {
            agreement_id = userAgreement.get(0).getAgreementID() + "";
            hitApiRequest(ApiConstants.REQUEST_AGREEMENT_DETAILS);
        }else {
            MakenMakePrefernece.getInstance().createPlanDetails("NoType", "NoPlan", "NoName", "NoAgreementId", "NoCategory");
        }
    }

    private void setUI2(List<AgreementDetail> agreementDetails) {
        MakenMakePrefernece.getInstance().createPlanDetails(agreementDetails.get(0).getType(), agreementDetails.get(0).getServicePlan(), agreementDetails.get(0).getServiceName(), agreement_id, agreementDetails.get(0).getCategory());
    }

    public int getCartItemSize() {
        List<AddToCartModel> addToCartModelList = MakenMakePrefernece.getInstance().getCart();
        if (addToCartModelList == null) {
            addToCartModelList = new ArrayList<>();
        }
        return addToCartModelList.size();
    }

    public void addItemToCart(AddToCartModel addToCartModel) {
        List<AddToCartModel> addToCartModelList = MakenMakePrefernece.getInstance().getCart();
        if (addToCartModelList == null) {
            addToCartModelList = new ArrayList<>();
        }
        if (!addToCartModelList.contains(addToCartModel)) {
            addToCartModelList.add(addToCartModel);
        } else {
            ToastUtils.showToast(this, "Item hase already in the cart");
        }
        updateCartValue(addToCartModelList.size());
        MakenMakePrefernece.getInstance().setCart(addToCartModelList);
    }


    public void updateCartValue(int count) {
        ((TextView) findViewById(R.id.cartCount)).setText(count + "");
        if (count == 0) {
            findViewById(R.id.cartCount).setVisibility(View.GONE);
        } else {
            findViewById(R.id.cartCount).setVisibility(View.VISIBLE);
        }
    }

    public void removeAllItemToCart(List<AddToCartModel> addToCartModelslist) {
        updateCartValue(addToCartModelslist.size());
        MakenMakePrefernece.getInstance().setCart(addToCartModelslist);
    }

    public void removeItemToCart(AddToCartModel addToCartModel) {
        List<AddToCartModel> addToCartModelList = MakenMakePrefernece.getInstance().getCart();
        if (addToCartModelList == null) {
            addToCartModelList = new ArrayList<>();
        }
        if (addToCartModelList.contains(addToCartModel)) {
            addToCartModelList.remove(addToCartModel);
        }
        updateCartValue(addToCartModelList.size());
        MakenMakePrefernece.getInstance().setCart(addToCartModelList);
    }

    public void showProgressDialog() {
        if (isFinishing()) {
            return;
        }
        try {
            if (mProgressDialog == null) {
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                mProgressDialog.setCancelable(false);
                mProgressDialog.setMessage("Loading");
            }
            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Removes the progress dialog
     */
    public void removeProgressDialog() {
        try {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.hide();
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideSoftKeyBoard() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mApplication.activityResumed();
    }


    @Override
    protected void onPause() {
        super.onPause();
        mApplication.activityPaused();
    }

    /**
     * hides the soft key pad
     */
    public void hideSoftKeypad(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.e(BaseActivity.class.getSimpleName(), "hideSoftKeypad()", e);
        }
    }

    public void loadUrlOnBrowser(String url) {
        try {
            if (!url.startsWith("http")) {
                url = "http://" + url;
            }
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception ex) {
            ToastUtils.showToast(this, "Url is not proper!");
        }
    }

    @Override
    public void onClick(View view) {

    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }


    @Override
    public void updateView(Object responseObject, boolean isSuccess, int reqType) {
        try {
            if (!isSuccess) {
                ToastUtils.showToast(this,
                        getString(R.string.some_error_occured));
                return;
            }
            switch (reqType) {
                case ApiConstants.REQUEST_GET_SERVICE_PURCHASED:
                    ServicePurchasedByClient servicepurchaedbyclient = (ServicePurchasedByClient) responseObject;
                    if (servicepurchaedbyclient.isAnyAgreement()) {
                        setUI(servicepurchaedbyclient.getUserAgreement());
                    } else {//TODO
                    }
                    break;
                case ApiConstants.REQUEST_AGREEMENT_DETAILS:
                    AgreementDetailResponse agreementdetails = (AgreementDetailResponse) responseObject;
                    if (agreementdetails.isAnyAgreement()) {
                        setUI2(agreementdetails.getAgreementDetail());
                    } else {
                    }

                    break;

                default:

                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public void hitApiRequest(int reqType) {
        if (!ConnectivityUtils.isNetworkEnabled(this)) {
            ToastUtils.showToast(this, getString(R.string.device_is_out_of_network_coverage));
            return;
        }
        String url;
        Class className;
        switch (reqType) {
            case ApiConstants.REQUEST_GET_SERVICE_PURCHASED:
                url = ApiConstants.URL_GET_SERVICE_PURCHASED + MakenMakePrefernece.getInstance().getUserId();
                className = ServicePurchasedByClient.class;
                VolleyStringRequest request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;
            case ApiConstants.REQUEST_AGREEMENT_DETAILS:
                url = ApiConstants.URL_AGREEMENT_DETAILS + agreement_id;
                className = AgreementDetailResponse.class;
                request = VolleyStringRequest.doGet(url, new UpdateListener(this, this, reqType, className) {
                });
                ((BaseApplication) this.getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);
                break;

            default:
                url = "";
                className = null;
                break;
        }

      /*  VolleyStringRequest request = VolleyStringRequest.doPost(url, new UpdateListener(this, this, reqType, className) {
        }, getParams(reqType));
        ((BaseApplication) getApplicationContext()).getVolleyManagerInstance().addToRequestQueue(request, url);*/
    }

    private HashMap<String, String> getParams(int reqType) {
        HashMap<String, String> params = new HashMap<String, String>();


        return params;
    }


    public HashMap<String, String> getBaseParams() {
        HashMap<String, String> params = new HashMap<String, String>();

        return params;
    }

    public void onStop() {
        super.onStop();


    }

    public enum HEADER_MODE {
        MENU_ONLY, MENU_WITH_SEARCH, BACK_WITH_SEARCH, BACK_WITHOUT_SEARCH, BACK_WITH_FORWARD, ONLY_FORWARD, MENU_WITH_SETTING, BOTH_SIDE_TEXT, SEARCH;
    }

    public void loadImage(String url, final ImageView imgView, int placeholder) {
        Picasso picasso = Picasso.with(this);
        if (!StringUtils.isNullOrEmpty(url)) {
            picasso.load(url).placeholder(placeholder).centerCrop().fit().into(imgView);
        }

    }

    public void loadImageWithBlur(String url, final ImageView imgView) {
        Picasso picasso = Picasso.with(this);
        if (!StringUtils.isNullOrEmpty(url)) {
            picasso.load(url).placeholder(R.drawable.setting_placeholder).transform(new BlurTransform(this, 25)).centerCrop().fit().into(imgView);
        }

    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void loadRoundedImage(String url, ImageView imgView) {
        imgSize = 0;
        if (imgSize == 0) {
            float density = getResources().getDisplayMetrics().density;
            imgSize = (int) (50 * density);
        }
        if (url != null && imgView != null) {
            Picasso.with(this).load(url).placeholder(R.drawable.circlular_placeholder).resize(imgSize, imgSize).transform(new RoundedTransformation()).into(imgView);
        }
    }


    public void loadRoundedCornerImage(String url, final ImageView imgView) {
        Picasso picasso = Picasso.with(this);
        if (!StringUtils.isNullOrEmpty(url) && imgView != null) {
            picasso.load(url)
                    .transform(new BitmapBorderTransformation(0, 25, Color.TRANSPARENT))
                    .centerCrop()
                    .fit()
                    .into(imgView);
        }
    }
}
