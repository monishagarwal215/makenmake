package com.krapps.application;

import android.app.Application;
import android.content.Context;

import com.citrus.sdk.Environment;
import com.citrus.sdk.ui.utils.CitrusFlowManager;
import com.krapps.database.BaseSqliteOpenHelper;
import com.krapps.network.VolleyManager;

/**
 * @author kapil.vij
 */
public class BaseApplication extends Application {
    private static BaseSqliteOpenHelper dbHelper;
    public static Context mContext;

    private boolean activityVisible;
    private String mFriendChatUserName;
    private boolean activityChatVisible;

    @Override
    public void onCreate() {
        super.onCreate();

        mContext = getApplicationContext();
        initiateCitrus();
    }

    private void initiateCitrus() {
        CitrusFlowManager.initCitrusConfig("9hh5re3r5q-signup",
                "3be4d7bf59c109e76a3619a33c1da9a8", "9hh5re3r5q-signin",
                "ffcfaaf6e6e78c2f654791d9d6cb7f09",
                getResources().getColor(android.R.color.white), this,
                Environment.SANDBOX, "nativeSDK", "https://salty-plateau-1529.herokuapp.com/billGenerator.sandbox.php"
                , "https://salty-plateau-1529.herokuapp.com/redirectUrlLoadCash.php", true);

    }

    public boolean isActivityVisible() {
        return activityVisible;
    }

    public void activityResumed() {
        activityVisible = true;
    }

    public void activityPaused() {
        activityVisible = false;
    }

    public boolean isActivityChatVisible() {
        return activityChatVisible;
    }

    public void activityChatResumed(String pFriendChatUserName) {
        activityChatVisible = true;
        mFriendChatUserName = pFriendChatUserName;
    }

    public String getFocusedChatUser() {
        return mFriendChatUserName;
    }

    public void activityChatPaused() {
        activityChatVisible = false;
    }

    public VolleyManager getVolleyManagerInstance() {
        return VolleyManager.getInstance(getApplicationContext());
    }

    public static BaseSqliteOpenHelper getDbHelperInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new BaseSqliteOpenHelper(context);
        }
        return dbHelper;
    }

    @Override
    public void onTerminate() {
        dbHelper.close();
        super.onTerminate();
    }

}
