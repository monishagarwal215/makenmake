package com.krapps.network;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.krapps.listener.UpdateJsonListener;
import com.krapps.listener.UpdateListener;

import org.json.JSONException;
import org.json.JSONObject;

import wiredsoft.com.makenmake.BuildConfig;

/**
 * @author kapil.vij
 */
public class VolleyJsonRequest extends JsonObjectRequest {

    private VolleyJsonRequest(int method, String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {

        super(url, new JSONObject(requestJson), updateListener, updateListener);
    }

    public static VolleyJsonRequest doPost(String url, UpdateJsonListener updateListener, String requestJson) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
            Log.i("Request Jso-->", requestJson);
        }
        return new VolleyJsonRequest(Method.POST, url, updateListener, requestJson);
    }

    public static VolleyJsonRequest doget(String url, UpdateJsonListener updateListener) throws JSONException {
        if (BuildConfig.DEBUG) {
            Log.i("Request Url-->", url);
        }
        return new VolleyJsonRequest(Method.GET, url, updateListener, null);
    }
}